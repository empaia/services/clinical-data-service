FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.4.1@sha256:396f1a0318405d1eecc03c1a1200a68df75d40365b55b2a6daece82452e8bee1 AS builder
COPY . /cds
WORKDIR /cds
RUN poetry build && poetry export -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.3.0@sha256:3cd9b9e1643ddb2ef6b014e49f592af303ce646c4c38b24dbdf78ffa67faeac7
COPY --from=builder /cds/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /cds/dist /artifacts
RUN pip install /artifacts/*.whl
COPY ./run.sh /opt/app/bin/run.sh
