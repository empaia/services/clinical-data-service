import csv
import logging
from pathlib import Path

from clinical_data_service.utils.simple_cache import SimpleCache

from .settings import Settings

settings = Settings()

slide_address_cache = SimpleCache()
case_id_cache = SimpleCache()

logger = logging.getLogger("uvicorn")
logger.setLevel(logging.INFO)
if settings.debug:
    logger.setLevel(logging.DEBUG)

# read available Tags from Definitions
_tags_csv = Path(__file__).parent / "definitions/tags/tags.csv"
tags_dict = {}
with open(_tags_csv) as f:
    for tag in csv.DictReader(f, delimiter=";"):
        grp = tag.pop("TAG_GROUP")
        id_ = tag.pop("TAG_NAME_ID")
        tags_dict.setdefault(grp, {})[id_] = tag
