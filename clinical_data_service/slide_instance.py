import uuid
from typing import List

from fastapi import HTTPException

from clinical_data_service.models.v1.slide import SlideInfo as SlideInfoV1
from clinical_data_service.models.v3.slide import SlideInfo as SlideInfoV3
from clinical_data_service.zmq_connection import PluginConnection


class SlideInstance:
    def __init__(self, filepath, connection: PluginConnection):
        self._filepath = filepath
        self._connection = connection

        self._supported_data = None
        self._raw_slide_info = None

    async def get_slide_metadata(self):
        data = await self.__get_slide_supported_data()
        if not data:
            return None

        storage_addresses = data["storage_addresses"]

        main_storage_address = None
        secondary_storage_addresses = []
        for addr in storage_addresses:
            if bool(addr["main_address"]):
                main_storage_address = {"storage_address_id": str(uuid.uuid4()), "path": addr["address"]}
            else:
                secondary_storage_addresses.append({"storage_address_id": str(uuid.uuid4()), "path": addr["address"]})

        return {
            "main_storage_address": main_storage_address,
            "secondary_storage_addresses": secondary_storage_addresses,
        }

    async def is_supported(self):
        data = await self.__get_slide_supported_data()
        if data is None:
            return False
        return data["is_supported"]

    async def get_slide_info(self, slide_info_model):
        raw_slide_info_data = (await self.get_raw_slide_info()).copy()
        if issubclass(slide_info_model, SlideInfoV1):
            del raw_slide_info_data["format"]
            return SlideInfoV1.model_validate(raw_slide_info_data)
        if issubclass(slide_info_model, SlideInfoV3):
            raw_slide_info_data["raw_download"] = True
            return SlideInfoV3.model_validate(raw_slide_info_data)
        return raw_slide_info_data

    async def get_raw_slide_info(self):
        if not self._raw_slide_info:
            req_msg = {
                "req": "SLIDE_INFO",
                "filepath": self._filepath,
            }
            data = await self._connection.send_and_recv_json(req_msg)

            if data["rep"] == "error":
                raise HTTPException(status_code=data["status_code"], detail=data["detail"])

            self._raw_slide_info = data["content"]

        return self._raw_slide_info

    async def get_thumbnail(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        return await self.__get_associated_image(
            image_type="THUMBNAIL", image_format=image_format, image_quality=image_quality, max_x=max_x, max_y=max_y
        )

    async def get_label(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        return await self.__get_associated_image(
            image_type="LABEL", image_format=image_format, image_quality=image_quality, max_x=max_x, max_y=max_y
        )

    async def get_macro(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        return await self.__get_associated_image(
            image_type="MACRO", image_format=image_format, image_quality=image_quality, max_x=max_x, max_y=max_y
        )

    async def get_region(
        self,
        level: int,
        start_x: int,
        start_y: int,
        size_x: int,
        size_y: int,
        image_channels: List[int],
        image_format: str,
        image_quality: int,
        z=0,
        padding_color=None,
    ):
        req_msg = {
            "req": "REGION",
            "filepath": self._filepath,
            "min_workers": self._connection.min_workers,
            "level": level,
            "start_x": start_x,
            "start_y": start_y,
            "size_x": size_x,
            "size_y": size_y,
            "image_channels": image_channels,
            "image_format": image_format,
            "image_quality": image_quality,
            "z": z,
            "padding_color": padding_color,
        }

        data, image_array = await self._connection.send_and_recv_multi(req_msg)

        if data["rep"] == "error":
            raise HTTPException(status_code=data["status_code"], detail=data["detail"])

        return data["content"]["media_type"], image_array

    async def get_tile(
        self,
        level: int,
        tile_x: int,
        tile_y: int,
        image_channels: List[int],
        image_format: str,
        image_quality: int,
        z=0,
        padding_color=None,
    ):
        req_msg = {
            "req": "TILE",
            "filepath": self._filepath,
            "min_workers": self._connection.min_workers,
            "level": level,
            "tile_x": tile_x,
            "tile_y": tile_y,
            "image_channels": image_channels,
            "image_format": image_format,
            "image_quality": image_quality,
            "z": z,
            "padding_color": padding_color,
        }

        data, image_array = await self._connection.send_and_recv_multi(req_msg)

        if data["rep"] == "error":
            raise HTTPException(status_code=data["status_code"], detail=data["detail"])

        return data["content"]["media_type"], image_array

    async def __get_slide_supported_data(self):
        if not self._supported_data:
            req_msg = {
                "req": "CHECK_IS_SUPPORTED",
                "filepath": self._filepath,
            }
            data = await self._connection.send_and_recv_json(req_msg)
            if data["status_code"] == 200 and "content" in data:
                self._supported_data = data["content"]

        return self._supported_data

    async def __get_associated_image(
        self, image_type: str, image_format: str, image_quality: int, max_x: int = None, max_y: int = None
    ):
        req_msg = {
            "req": image_type,
            "filepath": self._filepath,
            "max_x": max_x,
            "max_y": max_y,
            "image_format": image_format,
            "image_quality": image_quality,
        }
        data, image_array = await self._connection.send_and_recv_multi(req_msg)

        if data["rep"] == "error":
            raise HTTPException(status_code=data["status_code"], detail=data["detail"])

        return data["content"]["media_type"], image_array
