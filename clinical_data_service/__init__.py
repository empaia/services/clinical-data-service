from importlib.metadata import version

__version__ = version("clinical-data-service")
