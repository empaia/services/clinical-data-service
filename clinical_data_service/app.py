"""
This modules defines the REST API for the Clinical Data Service. It can be used to add,
get, search or update clinical data such as Cases or Slides. About Slides: This service is
only concerned with the "meta-information" of those Slides, not with the actual image data.
Those are handled by the WSI Service.
"""

from contextlib import asynccontextmanager

import asyncpg
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from clinical_data_service.slide_manager import SlideManager

from . import __version__
from .api.root import add_routes_root
from .api.v1 import add_private_routes_v1, add_routes_v1
from .api.v3 import add_private_routes_v3, add_routes_v3
from .late_init import LateInit
from .singletons import settings

openapi_url = "/openapi.json"
if settings.disable_openapi:
    openapi_url = ""

late_init = LateInit()

slide_manager = SlideManager(data_dir=settings.data_dir, plugin_addresses=settings.plugin_addresses)


@asynccontextmanager
async def lifespan(fastapi_app: FastAPI):
    late_init.pool = await asyncpg.create_pool(
        user=settings.db_username,
        password=settings.db_password,
        database=settings.db,
        host=settings.db_host,
        port=settings.db_port,
    )
    await slide_manager.initialize_plugins()
    yield


app = FastAPI(
    title="Clinical Data Service",
    version=__version__,
    redoc_url=None,
    openapi_url=openapi_url,
    root_path=settings.root_path,
    lifespan=lifespan,
)


add_routes_root(app, late_init, settings)

app_v1 = FastAPI(openapi_url=openapi_url)
app_v3 = FastAPI(openapi_url=openapi_url)
app_v1_private = FastAPI(openapi_url=openapi_url)
app_v3_private = FastAPI(openapi_url=openapi_url)

if settings.cors_allow_origins:
    for app_obj in [app, app_v1, app_v3, app_v1_private, app_v3_private]:
        app_obj.add_middleware(
            CORSMiddleware,
            allow_origins=settings.cors_allow_origins,
            allow_credentials=settings.cors_allow_credentials,
            allow_methods=["*"],
            allow_headers=["*"],
        )


add_routes_v1(app_v1, late_init, slide_manager, settings)
add_routes_v3(app_v3, late_init, slide_manager, settings)
app.mount("/v1", app_v1)
app.mount("/v3", app_v3)

add_private_routes_v1(app_v1_private, late_init, slide_manager)
add_private_routes_v3(app_v3_private, late_init, slide_manager)
app.mount("/private/v1", app_v1_private)
app.mount("/private/v3", app_v3_private)
