from pathlib import Path

from fastapi import HTTPException


def validate_slide_storage_address(slide_file: str, data_dir: str) -> str:
    data_dir_p = Path(data_dir).resolve()
    slide_file_p = Path(slide_file)

    concat_p = data_dir_p / slide_file_p
    concat_p_resolve = concat_p.resolve()
    if data_dir_p not in concat_p_resolve.parents:
        raise HTTPException(
            status_code=422,
            detail=f"""
            Forbidden path sequence. WSI filepath must be a child path of {data_dir_p} or a relative path
            """,
        )

    relative_slide_file = concat_p_resolve.relative_to(data_dir_p)

    if not concat_p_resolve.exists():
        raise HTTPException(
            status_code=404,
            detail=f"File or directory '{relative_slide_file}' does not exist",
        )

    return str(relative_slide_file)
