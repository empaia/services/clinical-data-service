from typing import OrderedDict


class SimpleCache:
    def __init__(self, max_size=100):
        self._cache: OrderedDict = OrderedDict()
        self._max_size = max_size

    def get(self, key):
        return self._cache.get(key)

    def add(self, key, value):
        if key not in self._cache and len(self._cache) >= self._max_size:
            self._cache.popitem(False)

        self._cache[key] = value
