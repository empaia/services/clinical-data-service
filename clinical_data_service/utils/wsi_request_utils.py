import re

from fastapi import HTTPException

from clinical_data_service.singletons import settings

supported_image_formats = {
    "bmp": "image/bmp",
    "gif": "image/gif",
    "jpeg": "image/jpeg",
    "png": "image/png",
    "tiff": "image/tiff",
}

alternative_spellings = {"jpg": "jpeg", "tif": "tiff"}


def validate_image_request(image_format, image_quality):
    if image_format in alternative_spellings:
        image_format = alternative_spellings[image_format]
    if image_format not in supported_image_formats:
        raise HTTPException(
            status_code=400,
            detail=f"Provided image format parameter {image_format} not supported. \
                Supported formats: {supported_image_formats.keys()}",
        )
    if image_quality < 0 or image_quality > 100:
        raise HTTPException(status_code=400, detail="Provided image quality parameter not supported")
    return image_format, image_quality


def validate_hex_color_string(padding_color):
    if padding_color:
        match = re.search(r"^#(?:[0-9a-fA-F]{3}){1,2}$", padding_color)
        if match:
            stripped_padding_color = padding_color.lstrip("#")
            int_padding_color = tuple(int(stripped_padding_color[i : i + 2], 16) for i in (0, 2, 4))
            return int_padding_color
        else:
            raise HTTPException(status_code=400, detail="Invalid padding color string")
    return settings.padding_color


def validate_image_channels(raw_slide_info: dict, image_channels):
    if image_channels is None:
        return
    for i in image_channels:
        if i >= len(raw_slide_info["channels"]):
            raise HTTPException(
                status_code=400,
                detail="More image channels were requested than available",
            )
    if len(image_channels) != len(set(image_channels)):
        raise HTTPException(status_code=400, detail="No duplicates allowed in channels")


def validate_image_size(size_x, size_y):
    if size_x * size_y > settings.max_returned_region_size:
        raise HTTPException(
            status_code=422,
            detail=f"Requested region may not contain more than {settings.max_returned_region_size} pixels.",
        )


def validate_image_z(raw_slide_info: dict, z: int):
    if z > 0 and (raw_slide_info["extent"]["z"] == 1 or raw_slide_info["extent"]["z"] is None):
        raise HTTPException(
            status_code=422,
            detail=f"Invalid ZStackQuery z={z}. The image does not support multiple z-layers.",
        )
    if z > 0 and z >= raw_slide_info["extent"]["z"]:
        raise HTTPException(
            status_code=422,
            detail=f"Invalid ZStackQuery z={z}. The image has only {raw_slide_info['extent']['z']} z-layers.",
        )


def validate_image_level(raw_slide_info: dict, level):
    if level >= len(raw_slide_info["levels"]):
        raise HTTPException(
            status_code=422,
            detail="The requested pyramid level is not available. "
            + f"The coarsest available level is {len(raw_slide_info['levels']) - 1}.",
        )
