from asyncpg.exceptions import PostgresError

from .... import __version__ as version
from ....models.commons import ServiceStatus, ServiceStatusEnum


class GeneralClient:
    def __init__(self, conn):
        self.conn = conn

    async def check_server_health(self):
        # database connection is checked
        try:
            await self.conn.execute("SELECT datname FROM pg_database;")
            return ServiceStatus(status=ServiceStatusEnum.OK.value, version=version)
        except PostgresError:
            return ServiceStatus(
                status=ServiceStatusEnum.FAILURE.value, version=version, message="Cannot reach database"
            )
