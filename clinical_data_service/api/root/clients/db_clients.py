from asyncpg import Connection

from ....db import set_type_codecs
from .general_client import GeneralClient


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return GeneralClient(conn=conn)
