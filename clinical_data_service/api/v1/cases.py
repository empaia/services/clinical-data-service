from fastapi import Path, Query
from pydantic import UUID4, conint

from ...models.v1.clinical import Case, CaseList
from .clients.commons import get_case
from .clients.db_clients import db_clients


def add_routes_cases(app, late_init):
    @app.get(
        "/cases/{case_id}",
        response_model=Case,
        responses={
            200: {"description": "The full Case (except Slides) if it exists"},
            400: {"description": "Case not found"},
        },
        tags=["Clinical Data"],
        summary="Get case by ID",
    )
    async def _(
        case_id: UUID4 = Path(..., description="The ID of the case to retrieve"),
        with_slides: bool = False,
    ) -> Case:
        """
        Get case with the given ID, if it exists, or raise an HTTP Exception otherwise.
        The case does not include Slides; to get all Slides for a specific case, use the
        `/slides/query` route.
        * Input `case_id`: The ID of the case to retrieve
        * Output: The full Case (except Slides) if it exists
        """
        async with late_init.pool.acquire() as conn:
            case_client, _, _ = await db_clients(conn=conn)
            return await get_case(case_client=case_client, case_id=case_id, with_slides=with_slides)

    @app.get(
        "/cases",
        response_model=CaseList,
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
        tags=["Clinical Data"],
        summary="Get list of all cases",
    )
    async def _(
        with_slides: bool = False,
        skip: conint(ge=0) = Query(None, description="Number of cases to skip, for paging"),
        limit: conint(ge=0) = Query(None, description="Number of items to return, for paging"),
    ) -> CaseList:
        """
        Get list of all Cases, newest first, with paging.
        * Input `skip`:  Number of cases to skip, for paging
        * Input `limit`: Number of items to return, for paging
        * Output: List of at most limit items, including total number available
        """
        async with late_init.pool.acquire() as conn:
            case_client, _, _ = await db_clients(conn=conn)
            return await case_client.get_all(with_slides=with_slides, skip=skip, limit=limit)
