from typing import List

from fastapi import HTTPException, Path
from fastapi.responses import Response

from clinical_data_service.custom_models.queries import (
    ImageChannelQuery,
    ImageFormatsQuery,
    ImagePaddingColorQuery,
    ImageQualityQuery,
    PluginQuery,
    ZStackQuery,
)
from clinical_data_service.custom_models.responses import ImageRegionResponse, ImageResponses
from clinical_data_service.models.v1.slide import SlideInfo
from clinical_data_service.singletons import slide_address_cache
from clinical_data_service.utils.wsi_request_utils import (
    validate_hex_color_string,
    validate_image_channels,
    validate_image_level,
    validate_image_request,
    validate_image_size,
    validate_image_z,
)

from .clients.db_clients import db_clients


def add_routes_wsis(app, late_init, slide_manager, settings):
    @app.get("/slides/{slide_id}/info", response_model=SlideInfo, tags=["WSI Data"])
    async def _(slide_id: str, plugin: str = PluginQuery):
        """
        Get metadata information for a slide given its ID
        """
        main_slide_storage = await __get_storage_info(slide_id)
        if main_slide_storage:
            slide_handle = await slide_manager.get_slide_handle(main_slide_storage, plugin)
            slide_info = await slide_handle.get_slide_info(slide_info_model=SlideInfo)
            slide_info.id = slide_id
            return slide_info

        raise HTTPException(status_code=404, detail="Slide not found")

    @app.get(
        "/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=Response,
        tags=["WSI Data"],
    )
    async def _(
        slide_id: str,
        max_x: int = Path(example=100, ge=1, le=settings.max_thumbnail_size, description="Maximum width of thumbnail"),
        max_y: int = Path(example=100, ge=1, le=settings.max_thumbnail_size, description="Maximum height of thumbnail"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        plugin: str = PluginQuery,
    ):
        """
        Get slide thumbnail image  given its ID.
        You additionally need to set a maximum width and height for the thumbnail.
        Images will be scaled to match these requirements while keeping the aspect ratio.

        Optionally, the image format and its quality (e.g. for jpeg) can be selected.
        Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        """
        image_format, image_quality = validate_image_request(image_format, image_quality)

        main_slide_storage = await __get_storage_info(slide_id)
        if main_slide_storage:
            slide_handle = await slide_manager.get_slide_handle(main_slide_storage, plugin)
            media_type, thumbnail = await slide_handle.get_thumbnail(
                max_x=max_x, max_y=max_y, image_format=image_format, image_quality=image_quality
            )
            return Response(content=thumbnail, media_type=media_type)

        raise HTTPException(status_code=404, detail="Slide not found")

    @app.get(
        "/slides/{slide_id}/label/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=Response,
        tags=["WSI Data"],
    )
    async def _(
        slide_id: str,
        max_x: int = Path(example=100, description="Maximum width of label image"),
        max_y: int = Path(example=100, description="Maximum height of label image"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        plugin: str = PluginQuery,
    ):
        """
        Get the label image of a slide given its ID.
        You additionally need to set a maximum width and height for the label image.
        Images will be scaled to match these requirements while keeping the aspect ratio.

        Optionally, the image format and its quality (e.g. for jpeg) can be selected.
        Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        """
        image_format, image_quality = validate_image_request(image_format, image_quality)

        main_slide_storage = await __get_storage_info(slide_id)
        if main_slide_storage:
            slide_handle = await slide_manager.get_slide_handle(main_slide_storage, plugin)
            media_type, label = await slide_handle.get_label(
                max_x=max_x, max_y=max_y, image_format=image_format, image_quality=image_quality
            )
            return Response(content=label, media_type=media_type)

        raise HTTPException(status_code=404, detail="Slide not found")

    @app.get(
        "/slides/{slide_id}/macro/max_size/{max_x}/{max_y}",
        responses=ImageResponses,
        response_class=Response,
        tags=["WSI Data"],
    )
    async def _(
        slide_id: str,
        max_x: int = Path(example=100, description="Maximum width of macro image"),
        max_y: int = Path(example=100, description="Maximum height of macro image"),
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        plugin: str = PluginQuery,
    ):
        """
        Get the macro image of a slide given its ID.
        You additionally need to set a maximum width and height for the macro image.
        Images will be scaled to match these requirements while keeping the aspect ratio.

        Optionally, the image format and its quality (e.g. for jpeg) can be selected.
        Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        """
        image_format, image_quality = validate_image_request(image_format, image_quality)

        main_slide_storage = await __get_storage_info(slide_id)
        if main_slide_storage:
            slide_handle = await slide_manager.get_slide_handle(main_slide_storage, plugin)
            media_type, macro = await slide_handle.get_macro(
                max_x=max_x, max_y=max_y, image_format=image_format, image_quality=image_quality
            )
            return Response(content=macro, media_type=media_type)

        raise HTTPException(status_code=404, detail="Slide not found")

    @app.get(
        "/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        responses=ImageRegionResponse,
        response_class=Response,
        tags=["WSI Data"],
    )
    async def _(
        slide_id: str,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        start_x: int = Path(example=0, description="x component of start coordinate of requested region"),
        start_y: int = Path(example=0, description="y component of start coordinate of requested region"),
        size_x: int = Path(gt=0, example=1024, description="Width of requested region"),
        size_y: int = Path(gt=0, example=1024, description="Height of requested region"),
        image_channels: List[int] = ImageChannelQuery,
        z: int = ZStackQuery,
        padding_color: str = ImagePaddingColorQuery,
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        plugin: str = PluginQuery,
    ):
        """
        Get a region of a slide given its ID and by providing the following parameters:

        * `level` - Pyramid level of the region. Level 0 is highest (original) resolution.
        The available levels depend on the image.

        * `start_x`, `start_y` - Start coordinates of the requested region.
        Coordinates are given with respect to the requested level.
        Coordinates define the upper left corner of the region with respect to the image origin
        (0, 0) at the upper left corner of the image.

        * `size_x`, `size_y` - Width and height of requested region.
        Size needs to be given with respect to the requested level.

        There are a number of addtional query parameters:

        * `image_channels` - Single channels (or multiple channels) can be retrieved through the optional parameter
        image_channels as an integer array referencing the channel IDs.
        This is paricularly important for images with abitrary image channels and channels with a higher
        color depth than 8bit (e.g. fluorescence images).
        The channel composition of the image can be obtained through the slide info endpoint,
        where the dedicated channels are listed along with its color, name and bitness.
        By default all channels are returned.

        * `z` - The region endpoint also offers the selection of a layer in a Z-Stack by setting the index z.
        Default is z=0.

        * `padding_color` - Background color as 24bit-hex-string with leading #,
        that is used when image region contains whitespace when out of image extent. Default is white.
        Only works for 8-bit RGB slides, otherwise the background color is black.

        * `image_format` - The image format can be selected. Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        Multi-channel images can also be represented as RGB-images (mostly for displaying reasons in the viewer).
        Note that the mapping of all color channels to RGB values is currently restricted to the first three channels.
        Default is jpeg.

        * `image_quality` - The image quality can be set for specific formats,
        e.g. for the jpeg format a value between 0 and 100 can be selected. Default is 90.
        """
        vp_color = validate_hex_color_string(padding_color)
        image_format, image_quality = validate_image_request(image_format, image_quality)
        validate_image_size(size_x, size_y)

        main_slide_storage = await __get_storage_info(slide_id)
        if main_slide_storage:
            slide_handle = await slide_manager.get_slide_handle(main_slide_storage, plugin)
            slide_info = await slide_handle.get_raw_slide_info()

            validate_image_level(slide_info, level)
            validate_image_z(slide_info, z)
            validate_image_channels(slide_info, image_channels)

            media_type, region = await slide_handle.get_region(
                level=level,
                start_x=start_x,
                start_y=start_y,
                size_x=size_x,
                size_y=size_y,
                image_channels=image_channels,
                image_format=image_format,
                image_quality=image_quality,
                z=z,
                padding_color=vp_color,
            )

            return Response(content=region, media_type=media_type)

        raise HTTPException(status_code=404, detail="Slide not found")

    @app.get(
        "/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}",
        responses=ImageResponses,
        response_class=Response,
        tags=["WSI Data"],
    )
    async def _(
        slide_id: str,
        level: int = Path(ge=0, example=0, description="Pyramid level of region"),
        tile_x: int = Path(example=0, description="Request the tile_x-th tile in x dimension"),
        tile_y: int = Path(example=0, description="Request the tile_y-th tile in y dimension"),
        image_channels: List[int] = ImageChannelQuery,
        z: int = ZStackQuery,
        padding_color: str = ImagePaddingColorQuery,
        image_format: str = ImageFormatsQuery,
        image_quality: int = ImageQualityQuery,
        plugin: str = PluginQuery,
    ):
        """
        Get a tile of a slide given its ID and by providing the following parameters:

        * `level` - Pyramid level of the tile. Level 0 is highest (original) resolution.
        The available levels depend on the image.

        * `tile_x`, `tile_y` - Coordinates are given with respect to tiles,
        i.e. tile coordinate n is the n-th tile in the respective dimension.
        Coordinates are also given with respect to the requested level.
        Coordinates (0,0) select the tile at the upper left corner of the image.

        There are a number of addtional query parameters:

        * `image_channels` - Single channels (or multiple channels) can be retrieved through
        the optional parameter image_channels as an integer array referencing the channel IDs.
        This is paricularly important for images with abitrary image channels and channels
        with a higher color depth than 8bit (e.g. fluorescence images).
        The channel composition of the image can be obtained through the slide info endpoint,
        where the dedicated channels are listed along with its color, name and bitness.
        By default all channels are returned.

        * `z` - The region endpoint also offers the selection of a layer in a Z-Stack by setting the index z.
        Default is z=0.

        * `padding_color` - Background color as 24bit-hex-string with leading #,
        that is used when image tile contains whitespace when out of image extent. Default is white.
        Only works for 8-bit RGB slides, otherwise the background color is black.

        * `image_format` - The image format can be selected. Formats include jpeg, png, tiff, bmp, gif.
        When tiff is specified as output format the raw data of the image is returned.
        Multi-channel images can also be represented as RGB-images (mostly for displaying reasons in the viewer).
        Note that the mapping of all color channels to RGB values is currently restricted to the first three channels.
        Default is jpeg.

        * `image_quality` - The image quality can be set for specific formats,
        e.g. for the jpeg format a value between 0 and 100 can be selected. Default is 90.
        It is ignored if raw jpeg tiles are available through a WSI service plugin.
        """
        vp_color = validate_hex_color_string(padding_color)
        image_format, image_quality = validate_image_request(image_format, image_quality)

        main_slide_storage = await __get_storage_info(slide_id)
        if main_slide_storage:
            slide_handle = await slide_manager.get_slide_handle(main_slide_storage, plugin)
            raw_slide_info = await slide_handle.get_raw_slide_info()

            validate_image_level(raw_slide_info, level)
            validate_image_z(raw_slide_info, z)
            validate_image_channels(raw_slide_info, image_channels)

            media_type, tile = await slide_handle.get_tile(
                level=level,
                tile_x=tile_x,
                tile_y=tile_y,
                image_channels=image_channels,
                image_format=image_format,
                image_quality=image_quality,
                z=z,
                padding_color=vp_color,
            )

            return Response(content=tile, media_type=media_type)

        raise HTTPException(status_code=404, detail="Slide not found")

    async def __get_storage_info(slide_id: str):
        main_storage_address = slide_address_cache.get(slide_id)
        if main_storage_address:
            return main_storage_address

        async with late_init.pool.acquire() as conn:
            _, _, storage_client = await db_clients(conn=conn)
            async with storage_client.conn.transaction():
                main_storage_address = await storage_client.get_main_storage_address(slide_id)
                slide_address_cache.add(slide_id, main_storage_address)
                return main_storage_address
