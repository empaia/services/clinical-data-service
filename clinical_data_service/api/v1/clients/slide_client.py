from typing import Optional

from asyncpg import Connection
from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ....models.v1.clinical import (
    ClinicalSlide,
    ClinicalSlideList,
    ClinicalSlideQuery,
    PostClinicalSlide,
    PutClinicalSlide,
)
from ....singletons import logger, settings, tags_dict


class SlideClient:
    def __init__(self, conn: Connection):
        self.conn = conn

    @staticmethod
    def _sql_slide_as_json():
        return """json_build_object(
            'id', id,
            'case_id', case_id,
            'tissue', tissue,
            'stain', stain,
            'block', block,
            'deleted', deleted,
            'created_at', EXTRACT(EPOCH FROM created_at)::int,
            'updated_at', EXTRACT(EPOCH FROM updated_at)::int
        )"""

    async def add(self, post_slide: PostClinicalSlide, external_ids: bool):
        await check_slide_tissue_stain(post_slide)

        if settings.allow_external_ids and external_ids:
            value_template = "$1, $2, $3, $4, $5"
            post_data = [
                post_slide.id,
                post_slide.case_id,
                post_slide.tissue if post_slide.tissue is not None else None,
                post_slide.stain if post_slide.stain is not None else None,
                post_slide.block,
            ]
        else:
            value_template = "uuid_generate_v4(), $1, $2, $3, $4"
            post_data = [
                post_slide.case_id,
                post_slide.tissue if post_slide.tissue is not None else None,
                post_slide.stain if post_slide.stain is not None else None,
                post_slide.block,
            ]

        sql = f"""
        INSERT INTO slides (id, case_id, tissue, stain, block)
        VALUES ({value_template})
        RETURNING {SlideClient._sql_slide_as_json()};
        """
        logger.debug(sql)

        try:
            async with self.conn.transaction():
                row = await self.conn.fetchrow(sql, *post_data)
                return ClinicalSlide.model_validate(row["json_build_object"])
        except UniqueViolationError as e:
            raise HTTPException(422, "Slide with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Slide could not be created") from e

    async def get(self, slide_id: UUID4) -> Optional[ClinicalSlide]:
        sql = f"""
        SELECT {SlideClient._sql_slide_as_json()}
        FROM slides
        WHERE id=$1;
        """
        logger.debug(sql)
        row = await self.conn.fetchrow(sql, slide_id)
        if row is None:
            return
        return ClinicalSlide.model_validate(row["json_build_object"])

    @staticmethod
    def _query_transform(query: ClinicalSlideQuery, start_index=1):
        transformed_query = ""
        query_terms = []
        query_data = []

        if query is None:
            return transformed_query, query_data

        query_mapping = {
            "slides.case_id": query.cases,
        }

        i = start_index
        for key, val in query_mapping.items():
            if val is None:
                continue

            query_terms.append(f"{key} = ANY(${i})")
            query_data.append(val)
            i += 1

        transformed_query = SlideClient._sql_concat(terms=query_terms, prefix="WHERE ", sep=" AND ")
        return transformed_query, query_data

    @staticmethod
    def _sql_concat(terms, prefix, sep):
        if len(terms) == 0:
            return ""
        return prefix + sep.join(terms)

    async def get_all(self, query: ClinicalSlideQuery = None, skip=None, limit=None) -> ClinicalSlideList:
        transformed_query, query_data = self._query_transform(query)

        sql = f"""
        SELECT json_agg({SlideClient._sql_slide_as_json()} ORDER BY sub.created_at DESC)
        FROM (
            SELECT *
            FROM slides
            {transformed_query}
            ORDER BY created_at DESC
        """

        if limit is not None:
            sql += f"""    LIMIT {limit}
            """

        if skip is not None:
            sql += f"""    OFFSET {skip}
            """

        sql += """) AS sub;
        """
        logger.debug(sql)

        items = []
        if limit is None or limit > 0:
            row = await self.conn.fetchrow(sql, *query_data)
            if row["json_agg"] is not None:
                items = row["json_agg"]

        sql = f"""
        SELECT COUNT(id)
        FROM slides
        {transformed_query};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, *query_data)
        item_count = row["count"]

        return ClinicalSlideList(items=items, item_count=item_count)

    @staticmethod
    def _update_transform(put_slide: PutClinicalSlide, start_index=2):
        update_terms = []
        update_data = []

        update_mapping = {
            "tissue": put_slide.tissue if put_slide.tissue is not None else None,
            "stain": put_slide.stain if put_slide.stain is not None else None,
            "block": put_slide.block,
            "deleted": put_slide.deleted,
        }

        i = start_index
        for key, val in update_mapping.items():
            if val is None:
                continue

            update_terms.append(f"{key}=${i}")
            update_data.append(val)
            i += 1

        if not update_data:
            return "", None

        transformed_update = SlideClient._sql_concat(terms=update_terms, prefix="SET ", sep=", ")
        return transformed_update, update_data

    async def update(self, slide_id: UUID4, put_slide: PutClinicalSlide):
        await check_slide_tissue_stain(put_slide)

        transformed_update, update_data = self._update_transform(put_slide)
        if update_data is None:
            return await self.get(slide_id=slide_id)

        sql = f"""
        UPDATE slides
        {transformed_update},
        updated_at=CURRENT_TIMESTAMP
        WHERE id=$1
        RETURNING {SlideClient._sql_slide_as_json()};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, slide_id, *update_data)
        return ClinicalSlide.model_validate(row["json_build_object"])


async def check_slide_tissue_stain(slide: PutClinicalSlide):
    """Check that the Tissue and Stain in the slide is found in the defined tags"""
    if slide.stain is not None and slide.stain not in tags_dict["STAIN"]:
        raise HTTPException(422, f"{slide.stain} is not a valid Stain")
    if slide.tissue is not None and slide.tissue not in tags_dict["TISSUE"]:
        raise HTTPException(422, f"{slide.tissue} is not a valid Tissue")
