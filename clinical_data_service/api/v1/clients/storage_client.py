from asyncpg import Connection, PostgresError, UniqueViolationError
from fastapi import HTTPException

from clinical_data_service import __version__ as version
from clinical_data_service.models.commons import ServiceStatus, ServiceStatusEnum
from clinical_data_service.models.v1.storage import PutSlideStorage, SlideStorage
from clinical_data_service.utils.slide_storage_utils import validate_slide_storage_address

from ....singletons import logger, settings


class StorageClient:
    def __init__(self, conn: Connection):
        self.conn = conn

    async def check_server_health(self):
        try:
            await self.conn.execute("SELECT datname FROM pg_database;")
            return ServiceStatus(status=ServiceStatusEnum.OK.value, version=version)
        except PostgresError:
            return ServiceStatus(
                status=ServiceStatusEnum.FAILURE.value, version=version, message="Cannot reach database"
            )

    async def update_slide_storage(self, slide_id: str, put_slide_storage: PutSlideStorage) -> SlideStorage:
        slide = await self.get_raw_slide_storage(slide_id=slide_id)
        if slide:
            await self.remove_slide_storage(slide_id=slide_id)

        # make sure storage addresses are relative and exist in mounted directory
        main_path = validate_slide_storage_address(put_slide_storage.main_storage_address.path, settings.data_dir)
        secondary_storage_addresses = []
        if put_slide_storage.secondary_storage_addresses:
            for storage_addr in put_slide_storage.secondary_storage_addresses:
                secondary_storage_addresses.append(
                    {
                        "storage_address_id": storage_addr.storage_address_id,
                        "path": validate_slide_storage_address(storage_addr.path, settings.data_dir),
                    }
                )

        insert_slide_storage_sql = (
            "INSERT INTO slides_storage ( slide_id, storage_type ) VALUES( $1, $2 ) RETURNING * ;"
        )
        insert_storage_address_sql = """
            INSERT INTO storage_addresses ( storage_address_id, slide_id, address, main_address, original_address )
            (SELECT
                st.storage_address_id, st.slide_id, st.address, st.main_address, st.original_address
            FROM
                unnest( $1::storage_addresses[] ) as st
            )
            RETURNING * ;
        """

        try:
            raw_slide_storage = await self.conn.fetchrow(insert_slide_storage_sql, slide_id, "fs")
            # secondary files
            storage_addresses_to_persist = [
                (addr["storage_address_id"], slide_id, addr["path"], False, None)
                for addr in secondary_storage_addresses
            ]
            # main storage file/path
            storage_addresses_to_persist.append(
                (put_slide_storage.main_storage_address.storage_address_id, slide_id, main_path, True, None)
            )
            # persist storage addresses
            raw_storage_addresses = await self.conn.fetch(insert_storage_address_sql, storage_addresses_to_persist)
            # aggregate result
            return self.validate_slide_storage(
                slide_id=raw_slide_storage["slide_id"], raw_storage_addresses=raw_storage_addresses
            )
        except UniqueViolationError as e1:
            logger.error("Storage address already defined: %s", e1)
            raise HTTPException(
                status_code=400,
                detail="UniqueViolationError: No unique storage_address_id",
            ) from e1
        except PostgresError as e2:
            logger.error("Failed to persist slide storage: %s", e2)
            raise HTTPException(status_code=500, detail="Failed to persist slide storage") from e2

    async def remove_slide_storage(self, slide_id: str):
        slide = await self.get_raw_slide_storage(slide_id)
        if not slide:
            raise HTTPException(
                status_code=404,
                detail=f"No slide storage defined for slide {slide_id}",
            )

        delete_storage_addresses_sql = """
            DELETE FROM storage_addresses
            WHERE slide_id = $1;
        """
        delete_slide_storage_sql = """
            DELETE FROM slides_storage
            WHERE slide_id = $1
            RETURNING slide_id ;
        """

        try:
            _ = await self.conn.fetchrow(delete_storage_addresses_sql, slide_id)
            _ = await self.conn.fetch(delete_slide_storage_sql, slide_id)
        except PostgresError as e2:
            logger.error("Failed to delete slide storage: %s", e2)
            raise HTTPException(status_code=500, detail="Failed to delete slide storage") from e2

    async def get_main_storage_address(self, slide_id: str) -> str:
        raw_storage_addresses = await self.get_raw_slide_storage(slide_id)
        if raw_storage_addresses:
            for raw_torage_address in raw_storage_addresses:
                if bool(raw_torage_address["main_address"]):
                    return raw_torage_address["address"]
        raise HTTPException(status_code=404, detail=f"No main storage for slide_id {slide_id} found")

    async def get_slide_storage(self, slide_id: str) -> SlideStorage:
        raw_slide_storage = await self.get_raw_slide_storage(slide_id=slide_id)
        return self.validate_slide_storage(slide_id=slide_id, raw_storage_addresses=raw_slide_storage)

    async def get_raw_slide_storage(self, slide_id: str) -> SlideStorage:
        sql = """
            SELECT slides_storage.*, storage_addresses.*
            FROM slides_storage
            LEFT JOIN storage_addresses ON slides_storage.slide_id = storage_addresses.slide_id
            WHERE slides_storage.slide_id = $1;
        """

        try:
            return await self.conn.fetch(sql, slide_id)
        except PostgresError as e1:
            logger.error("Failed to query slide storage: %s", e1)
            raise HTTPException(status_code=500, detail="Failed to query slide storage") from e1

    def validate_slide_storage(self, slide_id, raw_storage_addresses) -> str:
        if not raw_storage_addresses:
            raise HTTPException(status_code=404, detail=f"No slide storage defined for slide {slide_id}")

        result_secondary_storage_addresses = []
        result_main_storage_address = None
        for raw_storage_address in raw_storage_addresses:
            address = {
                "storage_address_id": raw_storage_address["storage_address_id"],
                "path": raw_storage_address["address"],
            }
            if bool(raw_storage_address["main_address"]):
                result_main_storage_address = address
            else:
                result_secondary_storage_addresses.append(address)

        if not result_main_storage_address:
            raise HTTPException(status_code=404, detail=f"No valid main storage address found for slide {slide_id}")

        return SlideStorage(
            slide_id=slide_id,
            main_storage_address=result_main_storage_address,
            secondary_storage_addresses=result_secondary_storage_addresses,
        )
