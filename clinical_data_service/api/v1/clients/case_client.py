from typing import Optional

from asyncpg import Connection
from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ....models.v1.clinical import Case, CaseList, PostCase, PutCase
from ....singletons import logger, settings


class CaseClient:
    def __init__(self, conn: Connection):
        self.conn = conn

    @staticmethod
    def _sql_case_as_json_post():
        return """json_build_object(
            'id', id,
            'creator_id', creator_id,
            'creator_type', creator_type,
            'description', description,
            'deleted', deleted,
            'created_at', EXTRACT(EPOCH FROM created_at)::int,
            'updated_at', EXTRACT(EPOCH FROM updated_at)::int,
            'slides', ARRAY[]::text[]
        )"""

    async def _sql_slides_left_join(self, with_slides: bool):
        if with_slides:
            return "LEFT JOIN slides as s ON c.id = s.case_id"
        else:
            return ""

    async def _sql_slide_as_json(self, with_slides: bool):
        if with_slides:
            return """
            ,COALESCE(
                json_agg(
                    json_build_object(
                        'id', s.id,
                        'case_id', s.case_id,
                        'tissue', s.tissue,
                        'stain', s.stain,
                        'block', s.block,
                        'deleted', s.deleted,
                        'created_at', EXTRACT(EPOCH FROM s.created_at)::int,
                        'updated_at', EXTRACT(EPOCH FROM s.updated_at)::int
                    )
                ) FILTER (WHERE s.id IS NOT NULL), '[]'
            ) AS slides
            """
        else:
            return ",ARRAY[]::text[] AS slides"

    async def add(self, post_case: PostCase, external_ids: bool):
        if settings.allow_external_ids and external_ids:
            value_template = "$1, $2, $3, $4"
            post_data = [post_case.id, post_case.creator_id, post_case.creator_type, post_case.description]
        else:
            value_template = "uuid_generate_v4(), $1, $2, $3"
            post_data = [post_case.creator_id, post_case.creator_type, post_case.description]

        sql = f"""
        INSERT INTO cases (id, creator_id, creator_type, description)
        VALUES ({value_template})
        RETURNING {CaseClient._sql_case_as_json_post()};
        """
        logger.debug(sql)

        try:
            async with self.conn.transaction():
                row = await self.conn.fetchrow(sql, *post_data)
                return Case.model_validate(row["json_build_object"])
        except UniqueViolationError as e:
            raise HTTPException(422, "Case with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Case could not be created") from e

    async def get(self, case_id: UUID4, with_slides: bool) -> Optional[Case]:
        sql = f"""
        SELECT row_to_json(row)
        FROM (
            SELECT
            c.id,
            c.creator_id,
            c.creator_type,
            c.description,
            c.deleted,
            EXTRACT(EPOCH FROM c.created_at)::int AS created_at,
            EXTRACT(EPOCH FROM c.updated_at)::int AS updated_at
            {await self._sql_slide_as_json(with_slides)}
            FROM cases as c
            {await self._sql_slides_left_join(with_slides)}
            WHERE c.id = $1
            GROUP BY c.id
        ) AS row;
        """
        logger.debug(sql)
        row = await self.conn.fetchrow(sql, case_id)
        if row is None:
            return
        return Case.model_validate(row[0])

    async def get_all(self, with_slides: bool, skip=None, limit=None) -> CaseList:
        sql = f"""
        SELECT row_to_json(row)
        FROM (
            SELECT
            c.id,
            c.creator_id,
            c.creator_type,
            c.description,
            c.deleted,
            EXTRACT(EPOCH FROM c.created_at)::int AS created_at,
            EXTRACT(EPOCH FROM c.updated_at)::int AS updated_at
            {await self._sql_slide_as_json(with_slides)}
            FROM cases as c
            {await self._sql_slides_left_join(with_slides)}
            GROUP BY c.id
            ORDER BY c.created_at DESC
        """

        if limit is not None:
            sql += f" LIMIT {limit} "

        if skip is not None:
            sql += f" OFFSET {skip} "

        sql += " ) AS row; "

        logger.debug(sql)

        items = []
        if limit is None or limit > 0:
            row = await self.conn.fetch(sql)
            if row is not None:
                items = [r[0] for r in row]

        sql = """
        SELECT COUNT(id)
        FROM cases;
        """
        logger.debug(sql)

        item_count = await self.conn.fetchval(sql)

        return CaseList(items=items, item_count=item_count)

    async def update(self, case_id: UUID4, put_case: PutCase):
        if put_case.description is None and put_case.deleted is None:
            return await self.get(case_id=case_id, with_slides=False)

        values_to_update = []
        values_to_update_str = ""
        str_index = 2
        if put_case.description is not None:
            values_to_update.append(put_case.description)
            values_to_update_str += f"description=${str_index},"
            str_index += 1
        if put_case.deleted is not None:
            values_to_update.append(put_case.deleted)
            values_to_update_str += f"deleted=${str_index},"

        sql = f"""
        UPDATE cases
        SET {values_to_update_str}
            updated_at=CURRENT_TIMESTAMP
        WHERE id=$1
        RETURNING {CaseClient._sql_case_as_json_post()};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, case_id, *values_to_update)
        return Case.model_validate(row["json_build_object"])
