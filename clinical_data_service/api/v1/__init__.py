from .cases import add_routes_cases
from .private import add_private_routes
from .slides import add_routes_slides
from .wsis import add_routes_wsis


def add_private_routes_v1(app, late_init, slide_manager):
    add_private_routes(app, late_init, slide_manager)


def add_routes_v1(app, late_init, slide_manager, settings):
    add_routes_cases(app, late_init)
    add_routes_slides(app, late_init)
    add_routes_wsis(app, late_init, slide_manager, settings)
