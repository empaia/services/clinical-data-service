from fastapi import Body, Path, Query, status
from pydantic import UUID4, conint

from ...models.v1.clinical import ClinicalSlide, ClinicalSlideList, ClinicalSlideQuery
from ...singletons import tags_dict
from .clients.commons import get_slide
from .clients.db_clients import db_clients


def add_routes_slides(app, late_init):
    @app.get(
        "/slides/{slide_id}",
        response_model=ClinicalSlide,
        responses={
            200: {"description": "The full Slide, if it exists"},
            400: {"description": "Slide not found"},
        },
        tags=["Clinical Data"],
        summary="Get slide by ID",
    )
    async def _(
        slide_id: UUID4 = Path(..., description="The ID of the Slide to retrieve"),
    ) -> ClinicalSlide:
        """
        Get the Slide with the given ID, or raise an HTTP Error if it does not exist.
        * Input `slide_id`: The ID of the Slide to retrieve
        * Output: The full Slide, if it exists
        """
        async with late_init.pool.acquire() as conn:
            _, slide_client, _ = await db_clients(conn=conn)
            return await get_slide(slide_client=slide_client, slide_id=slide_id)

    @app.get(
        "/slides",
        response_model=ClinicalSlideList,
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
        tags=["Clinical Data"],
        summary="Get list of all slides",
    )
    async def _(
        skip: conint(ge=0) = Query(None, description="Number of slides to skip, for paging"),
        limit: conint(ge=0) = Query(None, description="Number of items to return, for paging"),
    ) -> ClinicalSlideList:
        """
        Get list of all Slides, newest first, with paging.
        * Input skip: Number of cases to skip, for paging
        * Input limit: Number of items to return, for paging
        * Output: List of at most limit items, including total number available
        """
        async with late_init.pool.acquire() as conn:
            _, slide_client, _ = await db_clients(conn=conn)
            return await slide_client.get_all(skip=skip, limit=limit)

    @app.put(
        "/slides/query",
        response_model=ClinicalSlideList,
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
        tags=["Clinical Data"],
        summary="Query slides by case IDs",
    )
    async def _(
        slide_query: ClinicalSlideQuery = Body(..., description="Query for filtering the returned items"),
        skip: conint(ge=0) = Query(None, description="Number of cases to skip, for paging"),
        limit: conint(ge=0) = Query(None, description="Number of items to return, for paging"),
    ) -> ClinicalSlideList:
        """
        Get list of filtered Slides, newest first, with paging.
        * Input query: Query for filtering the returned items
        * Input skip: Number of cases to skip, for paging
        * Input limit: Number of items to return, for paging
        * Output: List of at most limit items, including total number available
        """
        async with late_init.pool.acquire() as conn:
            _, slide_client, _ = await db_clients(conn=conn)
            return await slide_client.get_all(query=slide_query, skip=skip, limit=limit)

    @app.get(
        "/tags",
        response_model=dict,
        status_code=status.HTTP_200_OK,
        tags=["Misc"],
        summary="Get a dictionary of all available tissue and stain tags",
    )
    async def _() -> dict:
        """
        Get a dictionary with the different allowed tags for Organs (`TISSUE`), Stains (`STAIN`), and others.
        Values are returned as a dictionary in the format:
        `{"tissue": {"SKIN": {"DE": "Haut", "EN": "Skin"}, ...}, ...}`.
        The values to be used for the Slides' `organ` and `stain` are the keys on the 2nd
        level, e.g. `SKIN` in the above example.
        """
        return tags_dict
