from fastapi import Body, Path, status
from pydantic import UUID4

from clinical_data_service.models.v3.storage import PutSlideStorage, SlideStorage

from ...models.v3.clinical import Case, ClinicalSlide, PostCase, PostClinicalSlide, PutCase, PutClinicalSlide
from .clients.commons import get_case, get_slide
from .clients.db_clients import db_clients


def add_private_routes(app, late_init, slide_manager):
    @app.post(
        "/cases",
        response_model=Case,
        responses={
            200: {"description": "The newly created Case"},
        },
        tags=["Clinical Data"],
        summary="Create new case",
    )
    async def _(
        post_case: PostCase = Body(..., description="The Case to be added to the DB"), external_ids: bool = False
    ) -> Case:
        """
        Create a new Case and add it to the Data Base.
        * Input `case`: The Case to be added to the DB
        * Output: The full newly created `Case`
        """
        async with late_init.pool.acquire() as conn:
            case_client, _, _ = await db_clients(conn=conn)
            return await case_client.add(post_case=post_case, external_ids=external_ids)

    @app.put(
        "/cases/{case_id}",
        response_model=Case,
        responses={
            200: {"description": "Updated Case"},
            400: {"description": "Illegal or read-only attribute"},
        },
        tags=["Clinical Data"],
        summary="Update case by ID",
    )
    async def _(
        case_id: UUID4 = Path(..., description="The ID of the case to update"),
        put_case: PutCase = Body(..., description="Dictionary with attributes to be updated"),
    ) -> Case:
        """
        Update the Case with that ID with the given template, or raise Error if Case does not exist.
        The template should only contain the attributes to be updated. If the template contains a
        `null` value, then the corresponding attribute will _not_ be left unchanged but set to `null`.
        * Input `case_id`: The ID of the Case to update
        * Input `template`: Dictionary with attributes to be updated
        """
        async with late_init.pool.acquire() as conn:
            case_client, _, _ = await db_clients(conn=conn)
            await get_case(case_client=case_client, case_id=case_id)
            return await case_client.update(case_id=case_id, put_case=put_case)

    @app.post(
        "/slides",
        response_model=ClinicalSlide,
        responses={
            200: {"description": "The newly created Slide"},
            400: {"description": "Case not found"},
        },
        tags=["Clinical Data"],
        summary="Create new slide",
    )
    async def _(
        post_slide: PostClinicalSlide = Body(..., description="The Slide to be added to the Data Base"),
        external_ids: bool = False,
    ) -> ClinicalSlide:
        """
        Create a new Slide description and add it to the Data Base. May raise Exception,
        e.g., if the case ID or the given enum values for tissue or stain are invalid.
        * Input `slide`: The Slide to be added to the Data Base
        * Output: The full newly created `Slide`
        """
        slide_storage_addresses = None
        if post_slide.main_path:
            slide_handle = await slide_manager.get_slide_handle(post_slide.main_path)
            slide_storage_addresses = await slide_handle.get_slide_metadata()

        async with late_init.pool.acquire() as conn:
            case_client, slide_client, storage_client = await db_clients(conn=conn)
            await get_case(case_client=case_client, case_id=post_slide.case_id)

            async with storage_client.conn.transaction() and slide_client.conn.transaction():
                slide = await slide_client.add(post_slide=post_slide, external_ids=external_ids)
                if slide_storage_addresses:
                    await storage_client.update_slide_storage(
                        slide_id=str(slide.id),
                        put_slide_storage=PutSlideStorage(
                            main_storage_address=slide_storage_addresses["main_storage_address"],
                            secondary_storage_addresses=slide_storage_addresses["secondary_storage_addresses"],
                        ),
                    )

                return slide

    @app.put(
        "/slides/{slide_id}",
        response_model=ClinicalSlide,
        responses={
            200: {"description": "Updated Slide"},
            400: {"description": "Illegal or read-only attribute"},
        },
        tags=["Clinical Data"],
        summary="Update slide by ID",
    )
    async def _(
        slide_id: UUID4 = Path(..., description="The ID of the slide to update"),
        put_slide: PutClinicalSlide = Body(..., description="Dictionary with attributes to be updated"),
    ) -> ClinicalSlide:
        """
        Update the Slide with that ID with the given template, or raise Error if Slide does not exist.
        The template should only contain the attributes to be updated. If the template contains a
        `null` value, then the corresponding attribute will _not_ be left unchanged but set to `null`.
        * Input `slide_id`: The ID of the Slide to update
        * Input `template`: Dictionary with attributes to be updated
        """
        async with late_init.pool.acquire() as conn:
            _, slide_client, _ = await db_clients(conn=conn)
            await get_slide(slide_client=slide_client, slide_id=slide_id)
            return await slide_client.update(slide_id=slide_id, put_slide=put_slide)

    @app.put(
        "/slides/{slide_id}/storage",
        status_code=status.HTTP_200_OK,
        response_model=SlideStorage,
        tags=["Storage Data"],
        summary="Create Slide Storage",
    )
    async def _(
        slide_id: UUID4 = Path(..., description="ID of the slide the storage is created for"),
        slide_storage: PutSlideStorage = Body(..., description="Dictionary with slide storage attributes"),
    ):
        async with late_init.pool.acquire() as conn:
            _, slide_client, storage_client = await db_clients(conn=conn)
            await get_slide(slide_client=slide_client, slide_id=slide_id)
            async with storage_client.conn.transaction():
                return await storage_client.update_slide_storage(
                    slide_id=str(slide_id), put_slide_storage=slide_storage
                )

    @app.get(
        "/slides/{slide_id}/storage",
        status_code=status.HTTP_200_OK,
        response_model=SlideStorage,
        tags=["Storage Data"],
        summary="Read Slide Storage",
    )
    async def _(slide_id: str):
        async with late_init.pool.acquire() as conn:
            _, slide_client, storage_client = await db_clients(conn=conn)
            await get_slide(slide_client=slide_client, slide_id=slide_id)
            async with storage_client.conn.transaction():
                return await storage_client.get_slide_storage(slide_id)

    @app.delete(
        "/slides/{slide_id}/storage",
        status_code=status.HTTP_200_OK,
        tags=["Storage Data"],
        summary="Remove Slide Storage",
    )
    async def _(slide_id: str):
        async with late_init.pool.acquire() as conn:
            _, slide_client, storage_client = await db_clients(conn=conn)
            await get_slide(slide_client=slide_client, slide_id=slide_id)
            async with storage_client.conn.transaction():
                await storage_client.remove_slide_storage(slide_id)
