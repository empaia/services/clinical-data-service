from asyncpg.exceptions import DataError
from fastapi import HTTPException


async def get_case(case_client, case_id, with_slides: bool = False):
    try:
        case = await case_client.get(case_id, with_slides)
        if case is None:
            raise HTTPException(status_code=400, detail={"cause": f"Case not found: {case_id} does not exist"})
        return case
    except DataError as e:
        raise HTTPException(status_code=400, detail={"cause": "Case not found: badly formed"}) from e


async def get_slide(slide_client, slide_id):
    try:
        slide = await slide_client.get(slide_id)
        if slide is None:
            raise HTTPException(status_code=400, detail={"cause": f"Slide not found: {slide_id} does not exist"})
        return slide
    except DataError as e:
        raise HTTPException(status_code=400, detail={"cause": "Slide not found: badly formed"}) from e
