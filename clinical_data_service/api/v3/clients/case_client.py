from typing import Optional

from asyncpg import Connection
from asyncpg.exceptions import DatatypeMismatchError, NotNullViolationError, PostgresError, UniqueViolationError
from fastapi.exceptions import HTTPException
from pydantic import UUID4

from ....models.v3.clinical import Case, CaseList, CaseQuery, PostCase, PutCase
from ....singletons import logger, settings, tags_dict


class CaseClient:
    def __init__(self, conn: Connection):
        self.conn = conn

    @staticmethod
    def _sql_case_as_json_post():
        return """json_build_object(
            'id', id,
            'creator_id', creator_id,
            'creator_type', creator_type,
            'description', description,
            'indication', indication,
            'procedure', procedure,
            'deleted', deleted,
            'created_at', EXTRACT(EPOCH FROM created_at)::int,
            'updated_at', EXTRACT(EPOCH FROM updated_at)::int,
            'slides', null
        )"""

    async def _sql_slides_left_join(self, with_slides: bool):
        if with_slides:
            return "LEFT JOIN slides as s ON c.id = s.case_id"
        else:
            return ""

    async def _sql_slide_as_json(self, with_slides: bool):
        if with_slides:
            return """
            ,COALESCE(
                json_agg(
                    json_build_object(
                        'id', s.id,
                        'case_id', s.case_id,
                        'tissue', s.tissue,
                        'stain', s.stain,
                        'block', s.block,
                        'deleted', s.deleted,
                        'created_at', EXTRACT(EPOCH FROM s.created_at)::int,
                        'updated_at', EXTRACT(EPOCH FROM s.updated_at)::int
                    )
                ) FILTER (WHERE s.id IS NOT NULL), '[]'
            ) AS slides
            """
        else:
            return ", NULL AS slides"

    async def _query_select_case_ids(self, case_query: CaseQuery):
        if case_query.slides:
            return """
            SELECT
                DISTINCT(case_id)
            FROM
                slides
            WHERE
                (
                    $1::uuid[] IS NULL
                    OR case_id = ANY($1)
                )
                AND
                id = ANY($2)
            """

        return """
        SELECT
            id AS case_id
        FROM
            cases
        WHERE
            id = ANY($1)
        """

    async def add(self, post_case: PostCase, external_ids: bool):
        await check_indication_procedure_stain(post_case)

        if settings.allow_external_ids and external_ids:
            value_template = "$1, $2, $3, $4, $5, $6"
            post_data = [
                post_case.id,
                post_case.creator_id,
                post_case.creator_type,
                post_case.description,
                post_case.indication,
                post_case.procedure,
            ]
        else:
            value_template = "uuid_generate_v4(), $1, $2, $3, $4, $5"
            post_data = [
                post_case.creator_id,
                post_case.creator_type,
                post_case.description,
                post_case.indication,
                post_case.procedure,
            ]

        sql = f"""
        INSERT INTO cases (id, creator_id, creator_type, description, indication, procedure)
        VALUES ({value_template})
        RETURNING {CaseClient._sql_case_as_json_post()};
        """
        logger.debug(sql)

        try:
            async with self.conn.transaction():
                row = await self.conn.fetchrow(sql, *post_data)
                return Case.model_validate(row["json_build_object"])
        except UniqueViolationError as e:
            raise HTTPException(422, "Case with given id already exists") from e
        except (DatatypeMismatchError, NotNullViolationError) as e:
            raise HTTPException(422, "ERROR: No valid value for id. Value must be of type UUID4.") from e
        except PostgresError as e:
            logger.debug(e)
            raise HTTPException(500, "Case could not be created") from e

    async def get(self, case_id: UUID4, with_slides: bool) -> Optional[Case]:
        sql = f"""
        SELECT row_to_json(row)
        FROM (
            SELECT
            c.id,
            c.creator_id,
            c.creator_type,
            c.description,
            c.indication,
            c.procedure,
            c.deleted,
            EXTRACT(EPOCH FROM c.created_at)::int AS created_at,
            EXTRACT(EPOCH FROM c.updated_at)::int AS updated_at
            {await self._sql_slide_as_json(with_slides)}
            FROM cases as c
            {await self._sql_slides_left_join(with_slides)}
            WHERE c.id = $1
            GROUP BY c.id
        ) AS row;
        """
        logger.debug(sql)
        row = await self.conn.fetchrow(sql, case_id)
        if row is None:
            return
        return Case.model_validate(row[0])

    async def get_all(self, with_slides: bool, skip=None, limit=None) -> CaseList:
        sql = f"""
        SELECT row_to_json(row)
        FROM (
            SELECT
            c.id,
            c.creator_id,
            c.creator_type,
            c.description,
            c.indication,
            c.procedure,
            c.deleted,
            EXTRACT(EPOCH FROM c.created_at)::int AS created_at,
            EXTRACT(EPOCH FROM c.updated_at)::int AS updated_at
            {await self._sql_slide_as_json(with_slides)}
            FROM cases as c
            {await self._sql_slides_left_join(with_slides)}
            GROUP BY c.id
            ORDER BY c.created_at DESC
        """

        if limit is not None:
            sql += f" LIMIT {limit} "

        if skip is not None:
            sql += f" OFFSET {skip} "

        sql += " ) AS row; "

        logger.debug(sql)

        items = []
        if limit is None or limit > 0:
            row = await self.conn.fetch(sql)
            if row is not None:
                items = [r[0] for r in row]

        sql = """
        SELECT COUNT(id)
        FROM cases;
        """
        logger.debug(sql)

        item_count = await self.conn.fetchval(sql)

        return CaseList(items=items, item_count=item_count)

    async def update(self, case_id: UUID4, put_case: PutCase):
        if (
            put_case.description is None
            and put_case.deleted is None
            and put_case.indication is None
            and put_case.procedure is None
        ):
            return await self.get(case_id=case_id, with_slides=False)

        await check_indication_procedure_stain(put_case)

        values_to_update = []
        values_to_update_str = ""
        str_index = 2
        if put_case.description is not None:
            values_to_update.append(put_case.description)
            values_to_update_str += f"description=${str_index},"
            str_index += 1
        if put_case.deleted is not None:
            values_to_update.append(put_case.deleted)
            values_to_update_str += f"deleted=${str_index},"
            str_index += 1
        if put_case.indication is not None:
            values_to_update.append(put_case.indication)
            values_to_update_str += f"indication=${str_index},"
            str_index += 1
        if put_case.procedure is not None:
            values_to_update.append(put_case.procedure)
            values_to_update_str += f"procedure=${str_index},"
            str_index += 1

        sql = f"""
        UPDATE cases
        SET {values_to_update_str}
            updated_at=CURRENT_TIMESTAMP
        WHERE id=$1
        RETURNING {CaseClient._sql_case_as_json_post()};
        """
        logger.debug(sql)

        row = await self.conn.fetchrow(sql, case_id, *values_to_update)
        return Case.model_validate(row["json_build_object"])

    async def query(self, case_query: CaseQuery, with_slides: bool, skip=None, limit=None) -> CaseList:
        if not case_query.cases and not case_query.slides:
            return await self.get_all(with_slides, skip, limit)

        limit_sql = f" LIMIT {limit} " if limit is not None else ""
        offset_sql = f" OFFSET {skip} " if skip is not None else ""

        sql = f"""
        WITH case_ids AS (
            {await self._query_select_case_ids(case_query)}
        ),
        case_count AS (
            SELECT
                count(case_id) AS c_count
            FROM
                case_ids
        )
        SELECT
            case_count.c_count,
            (
                SELECT
                    array_agg(row_to_json(ROW))
                FROM
                (
                    SELECT
                        c.id,
                        c.creator_id,
                        c.creator_type,
                        c.description,
                        c.indication,
                        c.procedure,
                        c.deleted,
                        EXTRACT(EPOCH FROM c.created_at)::int AS created_at,
                        EXTRACT(EPOCH FROM c.updated_at)::int AS updated_at
                        {await self._sql_slide_as_json(with_slides)}
                    FROM cases as c
                    {await self._sql_slides_left_join(with_slides)}
                    WHERE
                        c.id IN (
                            SELECT
                                *
                            FROM
                                case_ids
                        )
                    GROUP BY c.id
                    ORDER BY c.created_at DESC
                    {limit_sql}
                    {offset_sql}
                ) AS ROW
            )
        FROM
            case_count
        GROUP BY case_count.c_count;
        """

        logger.debug(sql)

        if case_query.slides:
            result = await self.conn.fetchrow(sql, case_query.cases, case_query.slides)
        else:
            result = await self.conn.fetchrow(sql, case_query.cases)

        item_count = 0
        items = []

        if result is not None:
            item_count = result[0]
            items = result[1] if result[1] else []

        return CaseList(items=items, item_count=item_count)


async def check_indication_procedure_stain(case: PutCase):
    """Check that the Tissue and Stain in the slide is found in the defined tags"""
    if case.indication is not None and case.indication not in tags_dict["INDICATION"]:
        raise HTTPException(422, f"{case.indication} is not a valid Stain")
    if case.procedure is not None and case.procedure not in tags_dict["PROCEDURE"]:
        raise HTTPException(422, f"{case.procedure} is not a valid Tissue")
