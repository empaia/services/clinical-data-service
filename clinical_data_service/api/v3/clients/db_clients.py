from asyncpg import Connection

from ....db import set_type_codecs
from .case_client import CaseClient
from .slide_client import SlideClient
from .storage_client import StorageClient


async def db_clients(conn: Connection):
    await set_type_codecs(conn=conn)
    return CaseClient(conn=conn), SlideClient(conn=conn), StorageClient(conn=conn)
