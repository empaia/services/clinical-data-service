from typing import Annotated

from fastapi import Body, Header, HTTPException, Path, Query
from pydantic import UUID4, conint

from ...models.v3.clinical import ClinicalSlide, ClinicalSlideList, ClinicalSlideQuery
from .clients.commons import get_slide
from .clients.db_clients import db_clients


def add_routes_slides(app, late_init):
    @app.get(
        "/slides/{slide_id}",
        response_model=ClinicalSlide,
        responses={
            200: {"description": "The full Slide, if it exists"},
            400: {"description": "Slide not found"},
        },
        tags=["Clinical Data"],
        summary="Get slide by ID",
    )
    async def _(
        slide_id: UUID4 = Path(..., description="The ID of the Slide to retrieve"),
        case_id: Annotated[UUID4 | None, Header()] = None,
    ) -> ClinicalSlide:
        """
        Get the Slide with the given ID, or raise an HTTP Error if it does not exist.
        * Input `slide_id`: The ID of the Slide to retrieve
        * Output: The full Slide, if it exists
        """

        async with late_init.pool.acquire() as conn:
            _, slide_client, _ = await db_clients(conn=conn)
            slide: ClinicalSlide = await get_slide(slide_client=slide_client, slide_id=slide_id)

            if case_id is not None and slide.case_id != case_id:
                raise HTTPException(status_code=412, detail="Slide case_id attribute not matching case-id HTTP header.")

            return slide

    @app.get(
        "/slides",
        response_model=ClinicalSlideList,
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
        tags=["Clinical Data"],
        summary="Get list of all slides",
    )
    async def _(
        skip: conint(ge=0) = Query(None, description="Number of slides to skip, for paging"),
        limit: conint(ge=0) = Query(None, description="Number of items to return, for paging"),
    ) -> ClinicalSlideList:
        """
        Get list of all Slides, newest first, with paging.
        * Input skip: Number of cases to skip, for paging
        * Input limit: Number of items to return, for paging
        * Output: List of at most limit items, including total number available
        """
        async with late_init.pool.acquire() as conn:
            _, slide_client, _ = await db_clients(conn=conn)
            return await slide_client.get_all(skip=skip, limit=limit)

    @app.put(
        "/slides/query",
        response_model=ClinicalSlideList,
        responses={
            200: {"description": "List of at most limit items, including total number available"},
        },
        tags=["Clinical Data"],
        summary="Query slides by case IDs",
    )
    async def _(
        slide_query: ClinicalSlideQuery = Body(..., description="Query for filtering the returned items"),
        skip: conint(ge=0) = Query(None, description="Number of cases to skip, for paging"),
        limit: conint(ge=0) = Query(None, description="Number of items to return, for paging"),
    ) -> ClinicalSlideList:
        """
        Get list of filtered Slides, newest first, with paging.
        * Input query: Query for filtering the returned items
        * Input skip: Number of cases to skip, for paging
        * Input limit: Number of items to return, for paging
        * Output: List of at most limit items, including total number available
        """
        async with late_init.pool.acquire() as conn:
            _, slide_client, _ = await db_clients(conn=conn)
            return await slide_client.get_all(query=slide_query, skip=skip, limit=limit)
