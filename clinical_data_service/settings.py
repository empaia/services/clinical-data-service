from typing import Set

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    disable_openapi: bool = False
    root_path: str = ""
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = []
    debug: bool = False

    db_host: str = "localhost"
    db_port: str = "5432"
    db_username: str = "admin"
    db_password: str = "secret"
    db: str = "js"
    allow_external_ids: bool = False

    # wsi related

    data_dir: str = "/data"
    max_returned_region_size: int = 25_000_000  # e.g. 5000 x 5000
    max_thumbnail_size: int = 500
    # padding color for areas outside scanned regions
    padding_color: tuple = (255, 255, 255)
    enable_viewer_routes: bool = False

    # base plugins:
    # * cds-plugin-openslide
    # * cds-plugin-pil
    # * cds-plugin-tifffile
    # * cds-plugin-tiffslide
    # * cds-plugin-wsidicom
    # * cds-plugin-isyntax
    # * cds-plugin-mirax
    plugin_addresses: str = ""

    # migration related

    # will migrate the storage addresses in the database
    # usually "/data" was used for previous deployments
    migration_storage_address_legacy_data_dir: str = ""

    model_config = SettingsConfigDict(env_prefix="CDS_", env_file=".env", extra="ignore")
