async def step_02(conn):
    sql = """
    ALTER TABLE slides
    RENAME COLUMN organ TO tissue;
    """
    await conn.execute(sql)
