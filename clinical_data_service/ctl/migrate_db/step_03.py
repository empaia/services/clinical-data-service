async def step_03(conn):
    sql = """
    ALTER TABLE cases
    ADD COLUMN IF NOT EXISTS deleted boolean;
    """
    await conn.execute(sql)

    sql = """
    UPDATE cases
    SET deleted = false
    WHERE deleted IS NULL;
    """
    await conn.execute(sql)

    sql = """
    ALTER TABLE slides
    ADD COLUMN IF NOT EXISTS deleted boolean;
    """
    await conn.execute(sql)

    sql = """
    UPDATE slides
    SET deleted = false
    WHERE deleted IS NULL;
    """
    await conn.execute(sql)
