async def step_04(conn):
    sql = """
        UPDATE slides
        SET stain = REPLACE (
            stain,
            'HE',
            'H_AND_E'
            )
        WHERE stain = 'HE';
    """
    await conn.execute(sql)
