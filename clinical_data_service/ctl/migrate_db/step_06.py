from ...singletons import settings


async def step_06(conn):
    if settings.migration_storage_address_legacy_data_dir == "":
        raise KeyError(
            """
                Environment variable `CDS_MIGRATION_STORAGE_ADDRESS_LEGACY_DATA_DIR` not set.
                Aborting migration and service startup.
            """
        )

    regex_str: str = f"/{settings.migration_storage_address_legacy_data_dir.lstrip('/').rstrip('/')}/"

    sql = f"""
        UPDATE public.storage_addresses
        SET address = regexp_replace(address, '{regex_str}', '')
        WHERE address LIKE '{regex_str}%';
    """
    await conn.execute(sql)
