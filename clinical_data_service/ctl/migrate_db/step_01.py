from asyncpg.exceptions import UniqueViolationError


async def step_01(conn):
    try:
        async with conn.transaction():
            sql = """
            CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
            """
            await conn.execute(sql)
    except UniqueViolationError as e:
        print("WARNING:", e)

    sql = """
    CREATE TABLE cases (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        creator_id text NOT NULL,
        creator_type text NOT NULL,
        description text,
        created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE slides (
        id uuid DEFAULT uuid_generate_v4() PRIMARY KEY,
        case_id uuid NOT NULL,
        organ text,
        stain text,
        block text,
        created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
        updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
    );
    """
    await conn.execute(sql)

    sql = """
    CREATE INDEX cases_created_at ON cases (created_at DESC);

    CREATE INDEX slides_case_id ON slides USING btree (case_id);
    CREATE INDEX slides_created_at ON slides (created_at DESC);
    """
    await conn.execute(sql)
