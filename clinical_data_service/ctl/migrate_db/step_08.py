async def step_08(conn):
    sql = """
    ALTER TABLE public.storage_addresses
    ADD COLUMN IF NOT EXISTS original_address varchar NULL;
    """
    await conn.execute(sql)
