async def step_05(conn):
    sql = """
    CREATE TABLE IF NOT EXISTS public.slides_storage (
        slide_id varchar NOT NULL,
        storage_type varchar NULL,
        CONSTRAINT slides_storage_pkey PRIMARY KEY (slide_id)
    );
    CREATE UNIQUE INDEX IF NOT EXISTS ix_slides_storage_slide_id ON public.slides_storage USING btree (slide_id);
    """
    await conn.execute(sql)

    sql = """
    CREATE TABLE IF NOT EXISTS public.storage_addresses (
        storage_address_id varchar NOT NULL,
        slide_id varchar NOT NULL,
        address varchar NULL,
        main_address bool NULL,
        CONSTRAINT storage_addresses_pkey PRIMARY KEY (storage_address_id),
        CONSTRAINT storage_addresses_slide_id_fkey FOREIGN KEY (slide_id) REFERENCES public.slides_storage(slide_id)
    );
    CREATE INDEX IF NOT EXISTS ix_storage_addresses_slide_id
        ON public.storage_addresses USING btree (slide_id);
    CREATE UNIQUE INDEX IF NOT EXISTS ix_storage_addresses_storage_address_id
        ON public.storage_addresses USING btree (storage_address_id);
    """
    await conn.execute(sql)
