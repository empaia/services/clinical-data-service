async def step_07(conn):
    sql = """
    ALTER TABLE cases
    ADD COLUMN IF NOT EXISTS indication text;
    """
    await conn.execute(sql)

    sql = """
    ALTER TABLE cases
    ADD COLUMN IF NOT EXISTS procedure text;
    """
    await conn.execute(sql)
