include:
  - project: "empaia/integration/ci-templates"
    ref: "main"
    file:
      - "pipeline-templates/pipeline-poetry-standard.yml"
      - "job-templates/docker-init.yml"
  - local: "scripts/build-and-push-plugin-docker.yml"
  - local: "scripts/check-plugin-changes-and-version.yml"

variables:
  TITLE: "CDS"
  DESCRIPTION: "Clinical Data Service (CDS)"
  DIRECTORY: "clinical_data_service"
  TEST_DIRECTORY: "tests"
  RUNNER_TAG: "empaia-parallel"
  RUNNER_DOCKER_TAG: "empaia-docker"
  HOST_PORT: 8000
  CONTAINER_PORT: 8000
  GIT_DEPTH: 0

# Check if plugins have changed and version was raised

check-plugin-version-openslide-feature-branch:
  extends: .check-plugin-version-feature-branch
  variables:
    PLUGIN_NAME: "openslide"

check-plugin-version-tiffslide-feature-branch:
  extends: .check-plugin-version-feature-branch
  variables:
    PLUGIN_NAME: "tiffslide"

check-plugin-version-tifffile-feature-branch:
  extends: .check-plugin-version-feature-branch
  variables:
    PLUGIN_NAME: "tifffile"

check-plugin-version-pil-feature-branch:
  extends: .check-plugin-version-feature-branch
  variables:
    PLUGIN_NAME: "pil"

check-plugin-version-wsidicom-feature-branch:
  extends: .check-plugin-version-feature-branch
  variables:
    PLUGIN_NAME: "wsidicom"

# Tests

docker-based-pytest:
  extends: .docker-init
  tags:
    - empaia-docker
    - testdata
  before_script:
    - !reference [.docker-init, before_script]
    - apt-get update && apt-get install -y wget
    # download philips sdk
    - export DECODED_SDK_URL=$(echo -n $ISX_PHILIPS_SDK_URL | base64 --decode)
    - wget -q $DECODED_SDK_URL -O wsi_format_plugins/isyntax/philips-pathologysdk-2.0-ubuntu18_04_py36_research.zip
    # download mirax backend
    - export DECODED_MRXS_BACKEND_URL=$(echo -n $MIRAX_BACKEND_URL | base64 --decode)
    - wget -q $DECODED_MRXS_BACKEND_URL -O wsi_format_plugins/mirax/mirax_backend.zip
    # build plugins and cds
    - cp sample_test.env .env
    - docker compose -f docker-compose.test.yml down -v
    - docker compose -f docker-compose.test.yml up --build -d
    - poetry install
    - sleep 10
  script:
    - poetry run pytest tests -vv --maxfail=1
  after_script:
    - docker compose -f docker-compose.test.yml down -v
    - !reference [.docker-init, after_script]

docker-based-pytest-external:
  extends: .docker-init
  tags:
    - empaia-docker
    - testdata
  before_script:
    - !reference [.docker-init, before_script]
    - apt-get update && apt-get install -y wget
    # download philips sdk
    - export DECODED_SDK_URL=$(echo -n $ISX_PHILIPS_SDK_URL | base64 --decode)
    - wget -q $DECODED_SDK_URL -O wsi_format_plugins/isyntax/philips-pathologysdk-2.0-ubuntu18_04_py36_research.zip
    # download mirax backend
    - export DECODED_MRXS_BACKEND_URL=$(echo -n $MIRAX_BACKEND_URL | base64 --decode)
    - wget -q $DECODED_MRXS_BACKEND_URL -O wsi_format_plugins/mirax/mirax_backend.zip
    # build plugins and cds
    - cp sample_test_ext_ids.env .env
    - docker compose -f docker-compose.test.yml down -v
    - docker compose -f docker-compose.test.yml up --build -d
    - poetry install
    - sleep 10
  script:
    - poetry run pytest tests -vv --maxfail=1
  after_script:
    - docker compose -f docker-compose.test.yml down -v
    - !reference [.docker-init, after_script]


# Check plugin versions and build (master only)

docker-build-and-push-plugin-openslide:
  extends: .build-and-push-plugin-docker
  variables:
    PLUGIN_NAME: "openslide"
  needs:
   - !reference [.docker-build-and-push-default-needs, needs]
   - job: docker-based-pytest
   - job: docker-based-pytest-external

docker-build-and-push-plugin-pil:
  extends: .build-and-push-plugin-docker
  variables:
    PLUGIN_NAME: "pil"
  needs:
    - !reference [.docker-build-and-push-default-needs, needs]
    - job: docker-based-pytest
    - job: docker-based-pytest-external

docker-build-and-push-plugin-tiffslide:
  extends: .build-and-push-plugin-docker
  variables:
    PLUGIN_NAME: "tiffslide"
  needs:
    - !reference [.docker-build-and-push-default-needs, needs]
    - job: docker-based-pytest
    - job: docker-based-pytest-external

docker-build-and-push-plugin-tifffile:
  extends: .build-and-push-plugin-docker
  variables:
    PLUGIN_NAME: "tifffile"
  needs:
    - !reference [.docker-build-and-push-default-needs, needs]
    - job: docker-based-pytest
    - job: docker-based-pytest-external

docker-build-and-push-plugin-wsidicom:
  extends: .build-and-push-plugin-docker
  variables:
    PLUGIN_NAME: "wsidicom"
  needs:
    - !reference [.docker-build-and-push-default-needs, needs]
    - job: docker-based-pytest
    - job: docker-based-pytest-external


# Build Clinical data service


docker-build-and-push:
  needs:
    - !reference [.docker-build-and-push-default-needs, needs]
    - job: docker-build-and-push-plugin-openslide
    - job: docker-build-and-push-plugin-pil
    - job: docker-build-and-push-plugin-tiffslide
    - job: docker-build-and-push-plugin-tifffile
    - job: docker-build-and-push-plugin-wsidicom