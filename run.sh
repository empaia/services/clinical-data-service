#!/usr/bin/env bash

cdsctl migrate-db && uvicorn clinical_data_service.app:app $@
