# Clinical Data Service

This Clinical Data Service (CDS) enables storing of all the clinical information related to diagnostic cases and slides including

* Case and slide organization
* Slide storage information
* Actual WSI meta- and image data retrieval

The service enables users to stream Whole Slide Images (WSI) tile-based via HTTP. It is based on a FastAPI webserver and a number of plugins to access whole slide image data.
Regarding the slide's metadata, it provides the extent of the base level (original image, level=0), its pixel size in nm (level=0), general tile extent, the total count of levels and a list of levels with information about its extent, downsampling factor in relation to the base level. Furthermore, the channel depth is given along with a list of all available channels.

Regions of the WSI can be requested on any of the available levels. There is also a way to access tiles of a predefined size of each level (e.g. useful for a [viewer](clinical_data_service/api/root/viewer.html)). Furthermore, it is possible to get a thumbnail, label and macro image.

## Build, Environment Variables & Startup

Build the image with `docker build -t clinical-data-service .`.

Environment Variables:

* `CDS_ROOT_PATH` set if server runs behind a proxy, else openapi `/docs` will not work.
* `CDS_DISABLE_OPENAPI` set if you want to disable  openapi `/docs` in productive mode.
* `CDS_ENABLE_VIEWER_ROUTES`: set to enable the validation viewer `/validation_viewer`
* `CDS_MAX_RETURNED_REGION_SIZE`: set maximum image region size for service (channels x width x height; default is 4 x 5000 x 5000)
* `CDS_PLUGIN_ADDRESSES`: set availibility and priority of plugins (see section `Plugin Priority`)


```bash
cd clinical-data-service
python -m venv .venv
source .venv/bin/activate
poetry install
```

Setup environment:

```bash
cp sample.env .env  # adapt as needed
```

Start service:

```bash
docker-compose up --build -d
```

### Automated Checks

Checks are enforced in CI, so check your code before committing!

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`
* run tests with `pytest`

```bash
black .
isort .
pycodestyle clinical_data_service/ wsi_format_plugins/ tests/
pylint clinical_data_service/ wsi_format_plugins/ tests/
pytest tests
```

## Validation Viewer

A minimal example viewer can be found [here](http://localhost:8000/validation_viewer) when the clinical data service is up and running.

## API Documentation (Swagger)

```
http://localhost:8000/docs
http://localhost:8000/private/v1/docs
http://localhost:8000/v1/docs
http://localhost:8000/private/v3/docs
http://localhost:8000/v3/docs
```

## Available Plugins

Different formats are supported by plugins for accessing image data. Five base plugins are included and support the following formats:

- [openslide](./wsi_format_plugins/openslide/)
  - 3DHISTECH (\*.mrxs)
  - HAMAMATSU (\*.ndpi)
  - LEICA (\*.scn)
  - VENTANA (\*.bif)
  - VSF (\*.vsf)

- [pil](./wsi_format_plugins/pil/)
  - JPEG (\*.jpeg, \*.jpg)
  - PNG (\*.png)

- [tifffile](./wsi_format_plugins/tifffile/)
  - OME-TIFF (\*.ome.tif, \*.ome.tif, \*.ome.tiff, \*.ome.tf2, \*.ome.tf8, \*.ome.btf)

- [tiffslide](./wsi_format_plugins/tiffslide/)
  - APERIO (\*.svs)
  - HAMAMATSU (\*.ndpi)
  - GENERIC TIF (\*.tif / \*.tiff)

- [wsidicom](./wsi_format_plugins/wsidicom/)
  - DICOM FOLDER

- [isyntax](./wsi_format_plugins/isyntax/) - CONFIDENTIAL (see [Mirax Backend](https://gitlab.com/empaia/integration/mirax-backend))
  - PHILIPS (\*.isyntax / \*.tif)

- [mirax](./wsi_format_plugins/mirax/) - CONFIDENTIAL (requires [Philips SDK](https://www.usa.philips.com/healthcare/sites/pathology/about/sdk))
  - 3DHISTECH (\*.mrxs)


## Plugin Priority

Each plugin will do a quick test if the given slide is supported. The first plugin that is able to open the file, is then cached in the clinical data service and used for every further request of the slide. The priority of plugins can be set via the environment variable `CDS_PLUGIN_ADDRESSES`. The variable states, which plugins can be used by the CDS by passing a pipe-separated list of plugin hosts and an optional semi-colon-separated minimum worker count per plugin (e.g. `cds-plugin-openslide;4|cds-plugin-tiffslide|cds-plugin-pil;1`). The order of the plugins determines the priority of the plugins. For instance, plugin `openslide` and `tiffslide` both support `.svs`-files, but `tiffslide` supports native tile retrieval. To make sure, `tiffslide` is always used for `.svs`-files, one has to set the following priority and the settings: `CDS_PLUGIN_ADDRESSES=cds-plugin-tiffslide|cds-plugin-openslide`

## Plugin Development

New plugins should follow the implementation of the base plugins. Each plugin needs to implement the [`BaseSlideInstance`](wsi_format_plugins/utils/cds_plugin_utils/base_slide_instance.py).

## Populate Testdata

WSIs need to be mounted into the CDS service via environment variable `COMPOSE_DATA_DIR=/path/to/WSIs`. The containing slides are **not** automatically registered during service startup, but need to be registered manually. This can be done by executing the following script:

```
python3 tests/data_generators/register_wsis_in_compose_data_dir.py 
```

The script will recursively walk through the mounted directory and register all contained WSIs. If you would like to register WSIs of a certain file format(s) use the `--only-filetypes` flag and specify the desired formats (e.g. `--only-filetypes mrxs;isyntax` to register only 3DHistech and Philips slides) 