# Changelog

## 0.6.3 - 7

* renovate

## 0.6.2

* introduced optional original_path to storage addresses

## 0.6.1

* fixed bug and added tests for case tags check not working

## 0.6.0

* added indication and procedure tags to case model to support EMP-0105

## 0.5.23

* return null for cases.slides instead of misleading empty array if with_slides=False

## 0.5.22

* fix tiffslide plugin id

## 0.5.21

* revert and pin tifffile version

## 0.5.19

* added case-id header checks for case data separation (EMP-0100)

## 0.5.17 - 0.5.18

* renovate

## 0.5.16

* updated plugins

## 0.5.15

* reworked ci pipeline

## 0.5.14

* renovate

## 0.5.12 & 0.5.13

* updated tifffile plugin


## 0.5.11

* added /cases/query endpoint in v3 API
* using FastAPI lifespan instead of startup event
* updated pydantic settings definition

## 0.5.10

* updated tifffile plugin

## 0.5.9

* updated plugins

## 0.5.8

* fix for generic tiff files without a value for mpp

## 0.5.7

* remove streaming response
* multi worker support in plugins
* some minor improvements and cleanup

## 0.5.6

* updated zmq backend server logic for most plugins

## 0.5.4 & 0.5.5

* updated tiffslide and openslide plugin

## 0.5.3

* reworked slide storage models again (re-added storage id)

## 0.5.2

* reworked slide/storage routes
* reworked slide storage models
* refactored data dir treatment in cds and plugins
* adapted all tests

## 0.5.1

* adapted readme
* updated models and deps

## 0.5.0

- integrate storage mapper and WSI service into clinical data service

## 0.4.10

- renovate: pydantic, fastapi, uvicorn, postgres, ...

## 0.4.9

- upgraded submodules
- remove deprecated `HE` tag and replace with `H_AND_E`

## 0.4.8

- upgrade black, postgres and pytest

## 0.4.7

- update base images

## 0.4.6

- update fastapi and pytest

## 0.4.5

- update fastapi, postgres and pydantic

## 0.4.4

- update fastapi, postgres, pydantic and pydantic-settings

## 0.4.3

- update postgres

## 0.4.2

- update fastapi, uvicorn, docker images

## 0.4.1

- update fastapi, postgres, pycodestyle, pydantic, pylint

## 0.3.9 & 0.4.0

- update asyncpg, black, fastapi, postgres, pydantic, uvicorn and base images
- adapt pydantic intergration to version 2
- set meaningful default values for settings

## 0.3.8

- update fastapi, pytest and base images

## 0.3.7

- update fastapi, postgres, pydantic, pytes and base images


## 0.3.6

- update fastapi

## 0.3.5

- update postgres, pydantic, requests

## 0.3.4

- update fastapi

## 0.3.3

- updated base images, asyncpg, black, fastapi, isort, postgres, pycodestyle, pydantic,  pylint, pytest, typer, uvicorn

## 0.3.2

- renamed v2 to v3 api

## 0.3.1

- alive route now checks DB and uses ServiceStatus mdoel


## 0.3.0

- refactor for API versioning
- updated definitions repo

## 0.2.38

- updated base image, postgres version

## 0.2.37

- updated base image

## 0.2.36

- updated base image, fastapi and postgres version

## 0.2.35

- updated base image, fastapi and postgres version

## 0.2.34

- updated base image and postgres version

## 0.2.33

- updated base image and postgres version

## 0.2.32

- updated base image version

## 0.2.31

- updated dependencies

## 0.2.30

- updated dependencies

## 0.2.29

- updated dependencies

## 0.2.28

- updated dependencies

## 0.2.27

- updated dependencies

## 0.2.26

- updated ci

## 0.2.25

- added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

## 0.2.24

- allow all origins, which is now possible because frontends do no more use client credentials (instead they explicitly
use an authorization header)

## 0.2.23

- updated models

## 0.2.22

- added ID validation

## 0.2.21

- fixed error in DB migration

## 0.2.20

- removed timestamps from loggin

## 0.2.19

- extended update routes to label cases and slides as deleted

## 0.2.18

- updated Definitions to fix encoding problem of tags

## 0.2.17

- renamed all occurrences of organ to tissue

## 0.2.16

- read allowed tags (organs, stains) from definitions
- added route `v1/tags` to show those allowed tags

## 0.2.15

- added external-id mode
- added query parameter `with_slides` to `GET /v1/cases` and `GET /v1/cases/{case_id}` to include slides in response

## 0.2.14

- catch UniqueViolationError when creating PSQL extensions, due to race conditions when sharing a DB

## 0.2.13

- added prefix for migration_steps table

## 0.2.12

- updated db-migration

## 0.2.11

- fixed internal server error when query does not match any items

## 0.2.10

- replaced MongoDB with PSQL to match other micro-services in MDS
  - allows for consistent DB backups

## 0.2.9

- changed test structure

## 0.2.8

- added current version number to result of `GET /alive` route

## 0.2.7

- return full objects instead of just IDs on Case and Slide creation

## 0.2.6

- return lists of cases or slides in sorted order, newest first

## 0.2.5

- added /alive endpoint

## 0.1.5

- updated to new consolidated model

## 0.1.4

- fix in documentation concerning CORS_ALLOW_ORIGINS variable

## 0.1.3

- CORS_ALLOW_ORIGINS setting

## 0.1.2

- updated Model (non-optional attributes and dedicated update models)

## 0.1.1

- updated Model (added creator_type Enum and attribute)

## 0.1.0

- routes for storing and retrieving Cases and WSIs
