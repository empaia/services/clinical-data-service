import os
from typing import List

from cds_plugin_utils import slide_utils
from cds_plugin_utils.base_slide_instance import BaseSlideInstance
from PIL import Image, UnidentifiedImageError


class PillowSlideInstance(BaseSlideInstance):
    supported_vendor_formats = {
        "jpeg": [".jpg", ".jpeg"],
        "png": [".png"],
        "bmp": [".bmp"],
    }

    def __init__(self, filepath: str, padding_color: tuple[int, int] = (255, 255, 255)):
        data_dir = os.getenv("PLUGIN_PIL_DATA_DIR") if os.getenv("PLUGIN_PIL_DATA_DIR") else "/data"
        self.slide = None
        self.slide_info = None

        self.padding_color = padding_color
        self.abs_filepath = f"{data_dir.rstrip('/')}/{filepath.lstrip('/')}"

        supported_formats_ext = []
        for key in self.supported_vendor_formats.keys():
            supported_formats_ext.extend(self.supported_vendor_formats[key])
        is_supported = self.__check_file_extension_supported(self.abs_filepath, None, set(supported_formats_ext))
        if is_supported:
            try:
                self.slide = Image.open(self.abs_filepath)
                self.slide_info = self.__get_slide_info_pil()
                if self.slide_info["rep"] == "error":
                    self.result = self.slide_info
                else:
                    self.result = slide_utils.get_success_response(
                        content={
                            "is_supported": True,
                            "format": str(self.slide.format).lower(),
                            "plugin": "pil",
                            "storage_addresses": [{"main_address": True, "address": filepath}],
                        }
                    )
            except UnidentifiedImageError:
                self.result = slide_utils.get_error_response(status_code=500, detail="PIL Unidentified Image Error")
        else:
            self.result = slide_utils.get_success_response(
                content={
                    "is_supported": False,
                    "format": None,
                    "plugin": "pil",
                    "storage_addresses": [],
                }
            )

    def get_info(self):
        return self.slide_info

    def close(self):
        if self.slide:
            self.slide.close()

    def get_region(
        self,
        level: int,
        start_x: int,
        start_y: int,
        size_x: int,
        size_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        if padding_color is None:
            padding_color = self.padding_color

        if not slide_utils.check_region_overlaps_image(
            start_x, start_y, size_x, start_y, self.slide.width, self.slide.height
        ):
            # simply crop image as requested region is a complete subset of the whole image region
            result_region = self.slide.crop((start_x, start_y, start_x + size_x, start_y + size_y))
        else:
            # calculate region to copy and paste
            crop_region_coord = slide_utils.get_crop_region_coordinates(
                start_x, start_y, size_x, size_y, self.slide.width, self.slide.height
            )
            paste_region_coord = slide_utils.get_paste_region_coordinates(start_x, start_y, size_x, size_y)

            result_region = Image.new("RGB", (size_x, size_y), padding_color)
            if crop_region_coord[2] > crop_region_coord[0] and crop_region_coord[3] > crop_region_coord[1]:
                crop_region = self.slide.crop(crop_region_coord)
                result_region.paste(crop_region, paste_region_coord)

        if image_channels:
            result_region = slide_utils.process_image_channels(result_region, image_channels)

        rgb_image = slide_utils.rgba_to_rgb_with_background_color(
            image_rgba=result_region, padding_color=self.padding_color
        )

        image_data = slide_utils.image_to_byte_array(rgb_image, image_format, image_quality)
        return (
            slide_utils.get_success_response(content={"media_type": f"image/{image_format}"}),
            image_data,
        )

    def get_thumbnail(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        thumbnail = self.slide.copy()
        thumbnail.thumbnail((max_x, max_y))
        rgb_thumbnail = slide_utils.rgba_to_rgb_with_background_color(
            image_rgba=thumbnail, padding_color=self.padding_color
        )
        image_data = slide_utils.image_to_byte_array(rgb_thumbnail, image_format, image_quality)
        return (
            slide_utils.get_success_response(content={"media_type": f"image/{image_format}"}),
            image_data,
        )

    def get_tile(
        self,
        level: int,
        tile_x: int,
        tile_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        tile_extent_x = int(self.slide_info["content"]["tile_extent"]["x"])
        tile_extent_y = int(self.slide_info["content"]["tile_extent"]["y"])
        return self.get_region(
            level,
            tile_x * tile_extent_x,
            tile_y * tile_extent_y,
            tile_extent_x,
            tile_extent_y,
            image_format=image_format,
            image_quality=image_quality,
            image_channels=image_channels,
            padding_color=padding_color,
            z=z,
        )

    # private

    def __check_file_extension_supported(self, filepath, unsupported_extensions, supported_extensions):
        if not filepath:
            return False

        unsupported_extensions = unsupported_extensions if unsupported_extensions else []
        supported_extensions = supported_extensions if supported_extensions else []

        for ext in unsupported_extensions:
            if str(filepath).endswith(ext):
                return False
        for ext in supported_extensions:
            if str(filepath).endswith(ext):
                return True

        return False

    def __get_slide_info_pil(
        self,
    ):
        base_level = {
            "x": self.slide.width,
            "y": self.slide.height,
            "z": 1,
        }
        slide_info = {
            "id": "",
            "channels": slide_utils.get_rgb_channel_dict(),
            "channel_depth": 8,
            "extent": base_level,
            "pixel_size_nm": {"x": -1, "y": -1},
            "tile_extent": {"x": 256, "y": 256, "z": 1},
            "num_levels": 1,
            "levels": [{"extent": base_level, "downsample_factor": 1.0}],
            "format": f"pil-{str(self.slide.format).lower()}",
        }
        return slide_utils.get_success_response(content=slide_info)

    def _get_associated_image(self, associated_image_name):
        return (
            slide_utils.get_error_response(
                status_code=500, detail=f"Associated image {associated_image_name} does not exist"
            ),
            None,
        )

    def get_label(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        return self._get_associated_image("label")

    def get_macro(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        return self._get_associated_image("macro")
