# Changelog

## 0.1.6 - 13

* renovate

## 0.1.5

* removed timed_cache from utils

## 0.1.4

* renovate

## 0.1.3

* Rework slide utils import
* Refactored dockerfile

## 0.1.2

* updated zmq backend server logic

## 0.1.1

* updated plugin file handling and zmq port

## 0.1.0

* Init