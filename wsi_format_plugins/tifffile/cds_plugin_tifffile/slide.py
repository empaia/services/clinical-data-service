import os
import re
import xml.etree.ElementTree as xml
from typing import List

import numpy as np
import tifffile
from cds_plugin_utils import log_utils, slide_utils
from cds_plugin_utils.base_slide_instance import BaseSlideInstance
from skimage import transform, util


class FilehandleNotDefinedError(Exception):
    pass


class FileOffsetError(Exception):
    pass


class OmeTifffileSlideInstance(BaseSlideInstance):
    supported_vendor_formats = {"ome": [".ome.tif", ".ome.tiff"]}

    def __init__(self, filepath: str, padding_color: tuple[int, int] = (255, 255, 255)):
        data_dir = os.getenv("PLUGIN_TIFFFILE_DATA_DIR") if os.getenv("PLUGIN_TIFFFILE_DATA_DIR") else "/data"
        self.slide = None
        self.abs_filepath = f"{data_dir.rstrip('/')}/{filepath.lstrip('/')}"
        self.padding_color = padding_color

        supported_formats_ext = []
        for key in self.supported_vendor_formats.keys():
            supported_formats_ext.extend(self.supported_vendor_formats[key])

        is_supported = self.__check_file_extension_supported(self.abs_filepath, None, set(supported_formats_ext))
        if is_supported:
            try:
                self.slide = tifffile.TiffFile(self.abs_filepath)
            except tifffile.TiffFileError as e:
                self.result = slide_utils.get_error_response(status_code=500, detail=f"TiffFileError: {e}")

            format = str(self.slide.series[0].kind).lower()
            if format not in self.supported_vendor_formats.keys():
                self.result = slide_utils.get_error_response(
                    status_code=500, detail=f"Unsupported file format ({format})"
                )
            else:
                try:
                    parsed_metadata = xml.fromstring(self.slide.ome_metadata)
                    self.slide_info = self.__get_slide_info(parsed_metadata)
                    if self.slide_info["rep"] == "error":
                        self.result = self.slide_info
                    else:
                        self.result = slide_utils.get_success_response(
                            content={
                                "is_supported": True,
                                "format": format,
                                "plugin": "tiffslide",
                                "storage_addresses": [{"main_address": True, "address": filepath}],
                            }
                        )
                except Exception as ex:
                    self.result = slide_utils.get_error_response(
                        status_code=500, detail=f"Could not obtain ome metadata ({ex})"
                    )
        else:
            self.result = slide_utils.get_success_response(
                content={
                    "is_supported": False,
                    "format": None,
                    "plugin": "tiffslide",
                    "storage_addresses": [],
                }
            )

    def close(self):
        if self.slide:
            self.slide.close()

    def get_info(self):
        return self.slide_info

    def get_region(
        self,
        level: int,
        start_x: int,
        start_y: int,
        size_x: int,
        size_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        data, result_array = self.__get_region(level, start_y, start_x, size_y, size_x, padding_color)

        if data["rep"] == "error":
            return data, result_array

        try:
            raw_binary_img = slide_utils.process_region_binary(
                result_array, image_format, image_quality, image_channels
            )
            data["content"]["media_type"] = f"image/{image_format}"
            return data, raw_binary_img
        except slide_utils.UnsupportedOutputFormatError as e1:
            return (
                slide_utils.get_error_response(status_code=422, detail=f"{e1}"),
                None,
            )
        except Exception as e2:
            return (
                slide_utils.get_error_response(status_code=500, detail=f"{e2}"),
                None,
            )

    def __get_region(
        self,
        level: int,
        start_y: int,
        start_x: int,
        size_y: int,
        size_x: int,
        padding_color: tuple[int, int],
    ) -> np.ndarray:
        level_slide = self.slide_info["content"]["levels"][level]
        tif_level = self.__get_tif_level_for_slide_level(level_slide)

        if not padding_color:
            padding_color = self.padding_color

        result_array = []
        for i, page in enumerate(tif_level.pages):
            try:
                temp_channel = self.__read_region_of_page(page, i, start_y, start_x, size_y, size_x, padding_color)
                result_array.append(temp_channel)
            except Exception as e:
                print(e)
                log_utils.tprint(
                    log_utils.LogIssuer.WORKER,
                    f"ERROR: Unknown error occured while reading region: {e}",
                )
                return (
                    slide_utils.get_error_response(
                        status_code=500,
                        detail=f"Internal error occured while reading tiff page",
                    ),
                    None,
                )

        return slide_utils.get_success_response_with_payload(
            content={}, raw_payload=np.concatenate(result_array, axis=0)[:, :, :, 0]
        )

    def get_thumbnail(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        thumb_level = self.__get_best_level_for_thumbnail(max_x=max_x, max_y=max_y)
        level_extent = self.slide_info["content"]["levels"][thumb_level]["extent"]

        data, result_array = self.__get_region(
            level=thumb_level,
            start_x=0,
            start_y=0,
            size_x=level_extent["x"],
            size_y=level_extent["y"],
            padding_color=self.padding_color,
        )

        if data["rep"] == "error":
            return data, result_array

        new_max_x, new_max_y = slide_utils.calculate_thumbnail_size(level_extent["x"], level_extent["y"], max_x, max_y)
        resized_array = util.img_as_uint(transform.resize(result_array, (result_array.shape[0], new_max_y, new_max_x)))

        if image_format == "tiff":
            return slide_utils.get_success_response_with_payload(
                content={"media_type": f"image/{image_format}"},
                raw_payload=slide_utils.narray_to_tiff_byte_array(narray=resized_array),
            )
        else:
            rgb_array = slide_utils.get_requested_channels_as_rgb_array(narray=resized_array, image_channels=None)
            pil_image = slide_utils.convert_narray_to_pil_image(
                narray=rgb_array  # , lower=np.min(rgb_array), upper=np.max(rgb_array)
            )
            return slide_utils.get_success_response_with_payload(
                content={"media_type": f"image/{image_format}"},
                raw_payload=slide_utils.image_to_byte_array(pil_image, image_format, image_quality),
            )

    def get_label(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        return self.__get_associated_image("label")

    def get_macro(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        return self.__get_associated_image("macro")

    def get_tile(
        self,
        level: int,
        tile_x: int,
        tile_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        tile_extent_x = int(self.slide_info["content"]["tile_extent"]["x"])
        tile_extent_y = int(self.slide_info["content"]["tile_extent"]["y"])
        return self.get_region(
            level=level,
            start_x=tile_x * tile_extent_x,
            start_y=tile_y * tile_extent_y,
            size_x=tile_extent_x,
            size_y=tile_extent_y,
            image_format=image_format,
            image_quality=image_quality,
            image_channels=image_channels,
            padding_color=padding_color,
            z=z,
        )

    # private

    def __get_best_level_for_thumbnail(self, max_x, max_y):
        slide_levels = self.slide_info["content"]["levels"]
        thumb_level = len(slide_levels) - 1
        for i, level in enumerate(slide_levels):
            if level["extent"]["x"] < max_x or level["extent"]["y"] < max_y:
                thumb_level = i
                break
        return thumb_level

    def __check_file_extension_supported(self, filepath, unsupported_extensions, supported_extensions):
        if not filepath:
            return False

        unsupported_extensions = unsupported_extensions if unsupported_extensions else []
        supported_extensions = supported_extensions if supported_extensions else []

        for ext in unsupported_extensions:
            if str(filepath).endswith(ext):
                return False
        for ext in supported_extensions:
            if str(filepath).endswith(ext):
                return True

        return False

    def __get_associated_image(self, associated_image_name):
        return (
            slide_utils.get_error_response(
                status_code=404,
                detail=f"Associated image '{associated_image_name}' does not exist",
            ),
            None,
        )

    def __get_color_for_channel(self, channel_index, channel_depth, padding_color):
        rgb_channel_color = 0
        if padding_color:
            rgb_channel_color = padding_color[channel_index if channel_index < 2 else 2]

        if channel_depth != 8:
            rgb_channel_color = rgb_channel_color * int(pow(2, channel_depth) / pow(2, 8))
        return rgb_channel_color

    def __get_tif_level_for_slide_level(self, slide_level):
        for level in self.slide.series[0].levels:
            if level.shape[1] == slide_level["extent"]["y"]:
                return level

    def __read_region_of_page(self, page, channel_index, start_x, start_y, size_x, size_y, padding_color):
        page_frame = page.keyframe

        channel_depth = self.slide_info["content"]["channel_depth"]
        if not page_frame.is_tiled:
            result = self.__read_region_of_page_untiled(
                page, channel_index, start_x, start_y, size_x, size_y, padding_color
            )
            if result.size == 0:
                result = np.full(
                    (size_x, size_y),
                    self.__get_color_for_channel(channel_index, channel_depth, padding_color),
                    dtype=page_frame.dtype,
                )
        else:
            result = self.__read_region_of_page_tiled(
                page, channel_index, start_x, start_y, size_x, size_y, padding_color
            )
            if result.size == 0:
                result = np.full(
                    (page_frame.imagedepth, size_x, size_y, page_frame.samplesperpixel),
                    self.__get_color_for_channel(channel_index, channel_depth, padding_color),
                    dtype=page_frame.dtype,
                )

        return result

    def __read_region_of_page_untiled(self, page, channel_index, start_x, start_y, size_x, size_y, padding_color):
        page_frame = page.keyframe
        page_width, page_height = page_frame.imagewidth, page_frame.imagelength
        page_array = page.asarray()

        if not (start_x <= page_height and start_y <= page_width and start_x + size_x >= 0 and start_y + size_y >= 0):
            return np.empty(shape=(0, 0))

        out = np.full(
            (size_x, size_y),
            self.__get_color_for_channel(
                channel_index,
                self.slide_info["content"]["channel_depth"],
                padding_color,
            ),
            dtype=page_frame.dtype,
        )

        # NOTE: page width and height are oredered reverse to our x/y coordinates
        # 1) we calculate the source region that we need to obtain
        src_coord = slide_utils.get_crop_region_coordinates(start_x, start_y, size_x, size_y, page_height, page_width)

        # 2) we calculate the x/y offset where we want to paste the cropped region in our destination array
        dest_coord = slide_utils.get_paste_region_coordinates(start_x, start_y, size_y, size_x)

        # 3) width and height need to be adapted to only obtain the actual image data from the page image array
        rel_height = page_height - src_coord[0] if src_coord[2] > page_height else src_coord[2] - src_coord[0]
        rel_width = page_width - src_coord[1] if src_coord[3] > page_width else src_coord[3] - src_coord[1]

        # 4) crop the region from our source page
        src_array = page_array[
            src_coord[0] : src_coord[0] + rel_height,
            src_coord[1] : src_coord[1] + rel_width,
        ]

        # 5) and paste it to the destination array
        out[
            dest_coord[0] : dest_coord[0] + src_array.shape[0],
            dest_coord[1] : dest_coord[1] + src_array.shape[1],
        ] = src_array

        return np.expand_dims(np.expand_dims(out, axis=0), axis=3)

    def __read_region_of_page_tiled(self, page, channel_index, start_x, start_y, size_x, size_y, padding_color):
        page_frame = page.keyframe
        image_width, image_height = page_frame.imagewidth, page_frame.imagelength

        if not (start_x <= image_height and start_y <= image_width and start_x + size_x >= 0 and start_y + size_y >= 0):
            return np.empty(shape=(0, 0))

        tile_width, tile_height = page_frame.tilewidth, page_frame.tilelength
        end_x = (start_x + size_x) if (start_x + size_x) < image_height else image_height
        end_y = (start_y + size_y) if (start_y + size_y) < image_width else image_width

        start_tile_x0, start_tile_y0 = start_x // tile_height, start_y // tile_width
        end_tile_xn, end_tile_yn = np.ceil([end_x / tile_height, end_y / tile_width]).astype(int)
        tile_per_line = int(np.ceil(image_width / tile_width))

        # initialize array with size of all relevant tiles
        out = np.full(
            (
                page_frame.imagedepth,
                size_x,
                size_y,
                page_frame.samplesperpixel,
            ),
            self.__get_color_for_channel(
                channel_index,
                self.slide_info["content"]["channel_depth"],
                padding_color,
            ),
            dtype=page_frame.dtype,
        )
        fh = page.parent.filehandle

        if fh is None:
            raise FilehandleNotDefinedError("Could not read from tiff file. File handle is not defined")

        jpegtables = page.jpegtables
        if jpegtables is not None:
            jpegtables = jpegtables.value

        used_offsets = set()
        # iterate through tiles starting at the top left to the bottom right
        for i in range(start_tile_x0, end_tile_xn):
            for j in range(start_tile_y0, end_tile_yn):
                index = int(i * tile_per_line + j)
                if len(page.dataoffsets) <= index:
                    continue

                offset = page.dataoffsets[index]
                bytecount = page.databytecounts[index]
                if offset in used_offsets:
                    continue

                used_offsets.add(offset)

                # search to tile offset and read image tile
                fh.seek(offset)
                if fh.tell() != offset:
                    raise FileOffsetError("Failed reading to tile offset")
                data = fh.read(bytecount)
                tile, tile_content_dim, _ = page.decode(data, index, jpegtables=jpegtables)

                # insert tile in temporary output array
                tile_paste_start_x = (i - start_tile_x0) * tile_height
                tile_paste_start_y = (j - start_tile_y0) * tile_width

                adjusted_tile_height = min(
                    (image_height - tile_content_dim[2]), tile_height, (size_x - tile_paste_start_x)
                )
                adjusted_tile_width = min(
                    (image_width - tile_content_dim[3]), tile_width, (size_y - tile_paste_start_y)
                )

                tile_paste_end_x = tile_paste_start_x + adjusted_tile_height
                tile_paste_end_y = tile_paste_start_y + adjusted_tile_width

                out[
                    :,
                    tile_paste_start_x:tile_paste_end_x,
                    tile_paste_start_y:tile_paste_end_y:,
                ] = tile[:, 0:adjusted_tile_height, 0:adjusted_tile_width:]

        # determine the new start positions and size of our region
        new_start_x = start_x - start_tile_x0 * tile_height
        new_start_y = start_y - start_tile_y0 * tile_width

        # restrict the output array to the requested region
        return out[:, new_start_x : new_start_x + size_x, new_start_y : new_start_y + size_y :]

    def __get_levels(self):
        levels = self.slide.series[0].levels
        level_count = len(levels)
        level_dimensions = []
        level_downsamples = []

        for i, item in enumerate(levels):
            level_dimensions.append([item.keyframe.imagewidth, item.keyframe.imagelength])
            if i > 0:
                level_downsamples.append(level_dimensions[0][0] / item.keyframe.imagewidth)
            else:
                level_downsamples.append(1)

        original_levels = slide_utils.get_original_levels(level_count, level_dimensions, level_downsamples)
        return original_levels

    def __get_xml_namespace(self, parsed_metadata):
        m = re.match(r"\{.*\}", parsed_metadata.tag)
        return m.group(0) if m else ""

    def __convert_int_to_rgba_array(self, i: int):
        return [(i >> 24) & 0xFF, (i >> 16) & 0xFF, (i >> 8) & 0xFF, i & 0xFF]

    def __get_channels(self, parsed_metadata):
        namespace = self.__get_xml_namespace(parsed_metadata)
        xml_channels = (
            parsed_metadata.find(f"{namespace}Image").find(f"{namespace}Pixels").findall(f"{namespace}Channel")
        )
        channels = []
        for i, xmlc in enumerate(xml_channels):
            color_int = self.__convert_int_to_rgba_array(int(xmlc.get("Color")))
            temp_channel = {
                "id": i,
                "name": xmlc.get("Name"),
                "color": {
                    "r": color_int[0],
                    "g": color_int[1],
                    "b": color_int[2],
                    "a": color_int[3],
                },
            }
            channels.append(temp_channel)
        return channels

    def __get_pixel_size(self, parsed_metadata):
        namespace = self.__get_xml_namespace(parsed_metadata)
        pixel_unit_x = parsed_metadata.find(f"{namespace}Image").find(f"{namespace}Pixels").get("PhysicalSizeXUnit")
        pixel_unit_y = parsed_metadata.find(f"{namespace}Image").find(f"{namespace}Pixels").get("PhysicalSizeYUnit")
        if pixel_unit_x != pixel_unit_y:
            raise ValueError("Different pixel size unit in x- and y-direction not supported.")

        pixel_size_x = parsed_metadata.find(f"{namespace}Image").find(f"{namespace}Pixels").get("PhysicalSizeX")
        pixel_size_y = parsed_metadata.find(f"{namespace}Image").find(f"{namespace}Pixels").get("PhysicalSizeY")
        if pixel_unit_x == "nm":
            x = float(pixel_size_x)
            y = float(pixel_size_y)
        elif pixel_unit_x == "µm":
            x = float(pixel_size_x) * 1000
            y = float(pixel_size_y) * 1000
        elif pixel_unit_x == "cm":
            x = float(pixel_size_x) * 1e6
            y = float(pixel_size_y) * 1e6
        else:
            raise ValueError(f"Invalid pixel size unit ({pixel_unit_x})")

        return {"x": x, "y": y}

    def __get_slide_info(self, parsed_metadata):
        serie = self.slide.series[0]
        channels = self.__get_channels(parsed_metadata)
        pixel_size = self.__get_pixel_size(parsed_metadata)
        levels = self.__get_levels()
        try:
            slide_info = {
                "id": "",
                "channels": channels,
                "channel_depth": serie.keyframe.bitspersample,
                "extent": {
                    "x": serie.keyframe.imagewidth,
                    "y": serie.keyframe.imagelength,
                    "z": serie.keyframe.imagedepth,
                },
                "pixel_size_nm": pixel_size,
                "tile_extent": {
                    "x": serie.keyframe.tilewidth,
                    "y": serie.keyframe.tilelength,
                    "z": serie.keyframe.tiledepth,
                },
                "num_levels": len(levels),
                "levels": levels,
                "format": "tifffile-ome-tiff",
            }
            return slide_utils.get_success_response(content=slide_info)
        except ValueError as e1:
            return (slide_utils.get_error_response(status_code=500, detail=f"{e1}"),)
        except Exception as e2:
            return (slide_utils.get_error_response(status_code=500, detail=f"Failed to gather slide infos. [{e2}]"),)
