from cds_plugin_utils.backend_server import BackendServer


def main():
    server = BackendServer(
        plugin_module="cds_plugin_tifffile", plugin_class_name="OmeTifffileSlideInstance", tcp_port=5556
    )
    server.run()


if __name__ == "__main__":
    main()
