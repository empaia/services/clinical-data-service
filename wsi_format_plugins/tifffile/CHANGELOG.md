# Changelog

## 0.1.17

* renovate

## 0.1.16

* fix codestyle after python update

## 0.1.12 - 15

* fix tiffslide plugin version

## 0.1.9 - 0.1.11

* renovate

## 0.1.8

* removed timed_cache from utils

## 0.1.7

* renovate

## 0.1.6

* fix for tifffile read region

## 0.1.4 & 0.1.5

* small bugfix for tile region retrieval

## 0.1.3

* Rework slide utils import
* Refactored dockerfile

## 0.1.1 & 0.1.2

* updated plugin file handling and zmq port

## 0.1.0

* Init