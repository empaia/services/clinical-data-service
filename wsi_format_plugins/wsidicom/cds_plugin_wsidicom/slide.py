import glob
import os
from typing import List

from cds_plugin_utils import slide_utils
from cds_plugin_utils.base_slide_instance import BaseSlideInstance
from PIL import Image
from wsidicom import WsiDicom
from wsidicom.errors import WsiDicomNotFoundError


class WsiDicomSlideInstance(BaseSlideInstance):
    supported_vendor_formats = {
        "DICOM": [".dcm"],
    }

    def __init__(self, filepath: str, padding_color: tuple[int, int] = (255, 255, 255)):
        data_dir = os.getenv("PLUGIN_WSIDICOM_DATA_DIR") if os.getenv("PLUGIN_WSIDICOM_DATA_DIR") else "/data"
        self.slide = None
        self.slide_info = None

        self.abs_filepath = f"{data_dir.rstrip('/')}/{filepath.lstrip('/')}"
        self.padding_color = padding_color

        is_supported = self.__check_dicom_filepath(self.abs_filepath)
        if is_supported:
            try:
                self.slide = WsiDicom.open(self.abs_filepath)
                self.slide_info = self.__get_slide_info_dicom()
                if self.slide_info["rep"] == "error":
                    self.result = self.slide_info
                else:
                    self.result = slide_utils.get_success_response(
                        content={
                            "is_supported": True,
                            "format": "dicom",
                            "plugin": "wsidicom",
                            "storage_addresses": self.__get_storage_addresses(
                                abs_filepath=self.abs_filepath, filepath=filepath, data_dir=data_dir
                            ),
                        }
                    )
            except WsiDicomNotFoundError as e:
                self.result = slide_utils.get_error_response(status_code=500, detail=f"WsiDicomNotFoundError: {e}")
        else:
            self.result = slide_utils.get_success_response(
                content={
                    "is_supported": False,
                    "format": None,
                    "plugin": "wsidicom",
                    "storage_addresses": [],
                }
            )

    def close(self):
        if self.slide:
            self.slide.close()

    def get_info(self):
        if not self.slide_info:
            self.slide_info = self.__get_slide_info_dicom()
        return self.slide_info

    def get_region(
        self,
        level: int,
        start_x: int,
        start_y: int,
        size_x: int,
        size_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        if padding_color is None:
            padding_color = self.padding_color
        level_dicom = self.slide.levels[level].level
        level_location = (
            (int)(start_x),
            (int)(start_y),
        )
        try:
            slide_extent = self.slide_info["content"]["extent"]
            if not slide_utils.check_region_overlaps_image(
                start_x, start_y, size_x, start_y, slide_extent["x"], slide_extent["y"]
            ):
                img = self.slide.read_region(level_location, level_dicom, (size_x, size_y))
            else:
                crop_region_coord = slide_utils.get_crop_region_coordinates(
                    start_x, start_y, size_x, size_y, slide_extent["x"], slide_extent["y"]
                )
                paste_region_coord = slide_utils.get_paste_region_coordinates(start_x, start_y, size_x, size_y)

                img = Image.new("RGB", (size_x, size_y), padding_color)
                if crop_region_coord[2] > crop_region_coord[0] and crop_region_coord[3] > crop_region_coord[1]:
                    crop_region = self.slide.read_region(
                        (crop_region_coord[0], crop_region_coord[1]),
                        level_dicom,
                        (crop_region_coord[2], crop_region_coord[3]),
                    )
                    img.paste(crop_region, paste_region_coord)

            rgb_img = slide_utils.rgba_to_rgb_with_background_color(img, padding_color)

            if image_channels:
                rgb_img = slide_utils.process_image_channels(rgb_img, image_channels)

            return (
                slide_utils.get_success_response(content={"media_type": f"image/{image_format}"}),
                slide_utils.image_to_byte_array(rgb_img, image_format, image_quality),
            )
        except Exception as e:
            return (
                slide_utils.get_error_response(
                    status_code=500, detail=f"Failed to extract slide region from WSI. [{e}]"
                ),
                None,
            )

    def get_thumbnail(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        try:
            thumbnail_image_data = self.slide.read_thumbnail((max_x, max_y))

            return (
                slide_utils.get_success_response(content={"media_type": f"image/{image_format}"}),
                slide_utils.image_to_byte_array(thumbnail_image_data, image_format, image_quality),
            )
        except Exception as e:
            return (
                slide_utils.get_error_response(status_code=500, detail=f"Failed to extract thumbnail from WSI. [{e}]"),
                None,
            )

    def get_label(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        try:
            label_image_data = self.slide.read_label()
            label_image_data.thumbnail((max_x, max_y))

            return (
                slide_utils.get_success_response(content={"media_type": f"image/{image_format}"}),
                slide_utils.image_to_byte_array(label_image_data, image_format, image_quality),
            )
        except WsiDicomNotFoundError as e:
            return (slide_utils.get_error_response(status_code=404, detail="Label image does not exist"), None)

    def get_macro(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        try:
            macro_image_data = self.slide.read_overview()
            macro_image_data.thumbnail((max_x, max_y))

            return (
                slide_utils.get_success_response(content={"media_type": f"image/{image_format}"}),
                slide_utils.image_to_byte_array(macro_image_data, image_format, image_quality),
            )
        except WsiDicomNotFoundError as e:
            return (slide_utils.get_error_response(status_code=404, detail="Macro image does not exist"), None)

    def get_tile(
        self,
        level: int,
        tile_x: int,
        tile_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        level_dicom = self.slide.levels[level].level
        tile_size = self.slide.tile_size
        try:
            slide_extent = self.slide_info["content"]["extent"]
            tile_extent = self.slide_info["content"]["tile_extent"]
            if (
                tile_x < 0
                or tile_y < 0
                or tile_x * tile_extent["x"] > slide_extent["x"]
                or tile_y * tile_extent["y"] > slide_extent["y"]
            ):
                rgb_img = Image.new("RGB", (tile_size.width, tile_size.height), padding_color)
            else:
                tile = self.slide.read_tile(level_dicom, (tile_x, tile_y))
                if tile.width != tile_size.width or tile.height != tile_size.height:
                    rgb_img = Image.new("RGB", (tile_size.width, tile_size.height), padding_color)
                    rgb_img.paste(tile)
                else:
                    rgb_img = slide_utils.rgba_to_rgb_with_background_color(tile, padding_color)

            if image_channels:
                rgb_img = slide_utils.process_image_channels(rgb_img, image_channels)

            return (
                slide_utils.get_success_response(content={"media_type": f"image/{image_format}"}),
                slide_utils.image_to_byte_array(rgb_img, image_format, image_quality),
            )
        except Exception as e:
            return (
                slide_utils.get_error_response(status_code=500, detail=f"Failed to extract slide tile from WSI. [{e}]"),
                None,
            )

    # private

    def __check_dicom_filepath(self, filepath):
        if os.path.isdir(filepath):
            dcm_files = glob.glob(os.path.join(filepath, "*.dcm"))
            if len(dcm_files) > 0:
                return True

        return False

    def __get_storage_addresses(self, abs_filepath, filepath, data_dir):
        storage_addresses = []
        if os.path.isdir(abs_filepath):
            storage_addresses.append({"main_address": True, "address": filepath})
            dcm_files = glob.glob(os.path.join(abs_filepath, "*.dcm"))
            for file in dcm_files:
                storage_addresses.append(
                    {
                        "main_address": False,
                        "address": self.__get_relative_filepath(filepath=file, data_dir=data_dir),
                    }
                )
            return storage_addresses

    def __get_relative_filepath(self, filepath, data_dir):
        return f"{str(filepath).replace(data_dir, '').lstrip('/')}"

    def __get_levels_dicom(self):
        levels = self.slide.levels

        level_count = len(levels)
        level_dimensions = []
        level_downsamples = []

        for level in levels:
            level_dimensions.append(level.size.to_tuple())
            level_downsamples.append(2**level.level)

        original_levels = slide_utils.get_original_levels(
            level_count=level_count,
            level_dimensions=level_dimensions,
            level_downsamples=level_downsamples,
        )
        return original_levels

    def __get_pixel_size(self):
        mpp = self.slide.levels.base_level.mpp
        return {"x": 1000.0 * mpp.width, "y": 1000.0 * mpp.height}

    def __get_slide_info_dicom(self):
        try:
            levels = self.__get_levels_dicom()
            slide_info = {
                "id": "",
                "channels": slide_utils.get_rgb_channel_dict(),  # rgb channels
                "channel_depth": 8,  # 8bit each channel
                "extent": {
                    "x": self.slide.levels.base_level.size.width,
                    "y": self.slide.levels.base_level.size.height,
                    "z": 1,
                },
                "pixel_size_nm": self.__get_pixel_size(),
                "tile_extent": slide_utils.get_tile_extent(
                    tile_width=self.slide.levels.base_level.tile_size.width,
                    tile_height=self.slide.levels.base_level.tile_size.height,
                ),
                "num_levels": len(levels),
                "levels": levels,
                "format": "wsidicom-dicom",
            }
            return slide_utils.get_success_response(content=slide_info)
        except Exception as e:
            return slide_utils.get_error_response(status_code=404, detail=f"Failed to gather slide infos. [{e}]")
