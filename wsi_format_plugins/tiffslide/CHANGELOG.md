# Changelog

## 0.1.9 - 0.1.16

* renovate

## 0.1.8

* removed timed_cache from utils

## 0.1.7

* renovate

## 0.1.6

* quickfix and added todo for unexpected response type from tiffslide.read_region

## 0.1.5

* fix for generic tiff files without a value for mpp

## 0.1.4

* Rework slide utils import
* Refactored dockerfile

## 0.1.3

* updated zmq backend server logic

## 0.1.2

* bugfix for invalid associated images

## 0.1.1

* updated plugin file handling and zmq port

## 0.1.0

* Init