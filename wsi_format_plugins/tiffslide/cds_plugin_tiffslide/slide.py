import os
from typing import List

import numpy as np
import tiffslide
from cds_plugin_utils import slide_utils
from cds_plugin_utils.base_slide_instance import BaseSlideInstance
from PIL import Image


class TiffslideSlideInstance(BaseSlideInstance):
    supported_vendor_formats = {
        "aperio": [".svs"],
        "hamamatsu": [".ndpi"],
        "generic_tiff": [".tif", ".tiff"],
    }

    non_supported_format_extensions = ["ome.tif", "ome.tiff"]

    def __init__(self, filepath: str, padding_color: tuple[int, int] = (255, 255, 255)):
        data_dir = os.getenv("PLUGIN_TIFFSLIDE_DATA_DIR") if os.getenv("PLUGIN_TIFFSLIDE_DATA_DIR") else "/data"
        self.slide = None

        self.abs_filepath = f"{data_dir.rstrip('/')}/{filepath.lstrip('/')}"
        self.padding_color = padding_color

        supported_formats_ext = []
        for key in self.supported_vendor_formats.keys():
            supported_formats_ext.extend(self.supported_vendor_formats[key])
        is_supported = self.__check_file_extension_supported(
            self.abs_filepath, self.non_supported_format_extensions, set(supported_formats_ext)
        )
        if is_supported:
            try:
                self.slide = tiffslide.TiffSlide(self.abs_filepath)
                self.format = self.slide.detect_format(self.abs_filepath)
                if self.format not in self.supported_vendor_formats.keys():
                    self.result = slide_utils.get_error_response(
                        status_code=500, detail=f"Format {self.format} not enabled for plugin"
                    )

                self.slide_info = self.__get_slide_info()  # TODO: this can throw an error!
                if self.slide_info["rep"] == "error":
                    self.result = self.slide_info
                else:
                    self.is_jpeg_compression = self.__is_jpeg_compression()
                    self.color_transform = self.__get_color_transform()
                    self.result = slide_utils.get_success_response(
                        content={
                            "is_supported": True,
                            "format": self.format,
                            "plugin": "tiffslide",
                            "storage_addresses": [{"main_address": True, "address": filepath}],
                        }
                    )
            except tiffslide.TiffFileError as e:
                self.result = slide_utils.get_error_response(status_code=500, detail=f"TiffFileError: {e}")
        else:
            self.result = slide_utils.get_success_response(
                content={
                    "is_supported": False,
                    "format": None,
                    "plugin": "tiffslide",
                    "storage_addresses": [],
                }
            )

    def close(self):
        if self.slide:
            self.slide.close()

    def get_info(self):
        return self.slide_info

    def get_region(
        self,
        level: int,
        start_x: int,
        start_y: int,
        size_x: int,
        size_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        data, image = self.__get_region(level, start_x, start_y, size_x, size_y, padding_color, z=z)

        if image_channels:
            image = slide_utils.process_image_channels(image, image_channels)

        data["content"]["media_type"] = f"image/{image_format}"
        return data, slide_utils.image_to_byte_array(image, image_format, image_quality)

    def get_thumbnail(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        data, thumbnail = self.__get_associated_image("thumbnail")
        if data["rep"] == "error":
            data, thumbnail = self.__get_thumbnail_tiffslide(max_x, max_y)
            if data["rep"] == "error":
                return data, thumbnail

        thumbnail.thumbnail((max_x, max_y))
        data["content"]["media_type"] = f"image/{image_format}"
        return data, slide_utils.image_to_byte_array(thumbnail, image_format, image_quality)

    def get_label(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        data, label_image_data = self.__get_associated_image("label")
        if data["rep"] == "error":
            return data, label_image_data

        label_image_data.thumbnail((max_x, max_y))
        data["content"]["media_type"] = f"image/{image_format}"
        return data, slide_utils.image_to_byte_array(label_image_data, image_format, image_quality)

    def get_macro(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        data, macro_image_data = self.__get_associated_image("macro")
        if data["rep"] == "error":
            return data, macro_image_data

        macro_image_data.thumbnail((max_x, max_y))
        data["content"]["media_type"] = f"image/{image_format}"
        return data, slide_utils.image_to_byte_array(macro_image_data, image_format, image_quality)

    def get_tile(
        self,
        level: int,
        tile_x: int,
        tile_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        tile_extent_x = int(self.slide_info["content"]["tile_extent"]["x"])
        tile_extent_y = int(self.slide_info["content"]["tile_extent"]["y"])
        if (
            image_format == "jpeg"
            and image_channels is None
            and self.is_jpeg_compression
            and not self.__check_image_overlaps_region(level, tile_x, tile_y)
        ):
            tif_level = self.__get_tif_level_for_slide_level(level)
            page = tif_level.pages[0]
            tile_data = self.__read_raw_tile(page, tile_x, tile_y)
            self.__add_jpeg_headers(page, tile_data, self.color_transform)
            return (
                slide_utils.get_success_response(content={"media_type": f"image/{image_format}"}),
                bytes(tile_data),
            )
        else:
            data, image = self.__get_region(
                level=level,
                start_x=tile_x * tile_extent_x,
                start_y=tile_y * tile_extent_y,
                size_x=tile_extent_x,
                size_y=tile_extent_y,
                padding_color=padding_color,
                z=z,
            )

            if image_channels:
                image = slide_utils.process_image_channels(image, image_channels)

            data["content"]["media_type"] = f"image/{image_format}"
            return data, slide_utils.image_to_byte_array(image, image_format, image_quality)

    # private

    def __check_image_overlaps_region(self, level: int, tile_x: int, tile_y: int):
        downsample_factor = float(self.slide_info["content"]["levels"][level]["downsample_factor"])
        tile_extent_x = self.slide_info["content"]["tile_extent"]["x"] * downsample_factor
        tile_extent_y = self.slide_info["content"]["tile_extent"]["y"] * downsample_factor
        slide_extent_x = self.slide_info["content"]["extent"]["x"]
        slide_extent_y = self.slide_info["content"]["extent"]["y"]
        return slide_utils.check_region_overlaps_image(
            tile_x * tile_extent_x,
            tile_y * tile_extent_y,
            tile_extent_x,
            tile_extent_y,
            slide_extent_x,
            slide_extent_y,
        )

    def __check_file_extension_supported(self, filepath, unsupported_extensions, supported_extensions):
        if not filepath:
            return False

        unsupported_extensions = unsupported_extensions if unsupported_extensions else []
        supported_extensions = supported_extensions if supported_extensions else []

        for ext in unsupported_extensions:
            if str(filepath).endswith(ext):
                return False
        for ext in supported_extensions:
            if str(filepath).endswith(ext):
                return True

        return False

    def __get_tif_level_for_slide_level(self, level):
        slide_level = self.slide_info["content"]["levels"][level]
        for level in self.slide._tifffile.series[0].levels:
            if level.shape[1] == slide_level["extent"]["x"] and level.shape[0] == slide_level["extent"]["y"]:
                return level

    def __get_region(
        self, level: int, start_x: int, start_y: int, size_x: int, size_y: int, padding_color: tuple = None, z: int = 0
    ):
        if padding_color is None:
            padding_color = self.padding_color

        slide_extent_x = self.slide_info["content"]["levels"][level]["extent"]["x"]
        slide_extent_y = self.slide_info["content"]["levels"][level]["extent"]["y"]

        if not slide_utils.check_region_overlaps_image(
            start_x, start_y, size_x, size_y, slide_extent_x, slide_extent_y
        ):
            # simply crop image as requested region is a complete subset of the whole image region
            return self.__retrieve_image_region(level, start_x, start_y, size_x, size_y, padding_color, z=z)
        else:
            # calculate region to copy and paste
            crop_region_coord = slide_utils.get_crop_region_coordinates(
                start_x, start_y, size_x, size_y, slide_extent_x, slide_extent_y
            )
            paste_region_coord = slide_utils.get_paste_region_coordinates(start_x, start_y, size_x, size_y)

            result_region = Image.new("RGB", (size_x, size_y), padding_color)
            data = slide_utils.get_success_response(content={})
            if crop_region_coord[2] > crop_region_coord[0] and crop_region_coord[3] > crop_region_coord[1]:
                adapted_size_x = crop_region_coord[2] - crop_region_coord[0]
                adapted_size_y = crop_region_coord[3] - crop_region_coord[1]
                if adapted_size_x == 0 or adapted_size_y == 0:
                    return slide_utils.get_success_response_with_payload(content={}, raw_payload=result_region)

                data, image = self.__retrieve_image_region(
                    level,
                    crop_region_coord[0],
                    crop_region_coord[1],
                    adapted_size_x,
                    adapted_size_y,
                    padding_color,
                    z=z,
                )

                if data["rep"] == "error":
                    return data, image

                result_region.paste(image, paste_region_coord)

            return data, result_region

    def __retrieve_image_region(
        self, level: int, start_x: int, start_y: int, size_x: int, size_y: int, padding_color=None, z: int = 0
    ):
        downsample_factor = self.slide_info["content"]["levels"][level]["downsample_factor"]
        level_0_location = (
            (int)(start_x * downsample_factor),
            (int)(start_y * downsample_factor),
        )
        level_0_location = self.__adapt_level_0_location(level_0_location, downsample_factor, start_x, start_y)
        try:
            img = self.slide.read_region(level_0_location, level, (size_x, size_y))
            # TODO: investigate, why this can be a ndarray in some cases although as_array is not set
            if isinstance(img, np.ndarray):
                return (
                    {"rep": "error", "status_code": 500, "detail": f"Not an instance of pil.Image"},
                    None,
                )
        except tiffslide.TiffFileError as e:
            return (
                {"rep": "error", "status_code": 500, "detail": f"TiffFileError: {e}"},
                None,
            )
        rgb_img = slide_utils.rgba_to_rgb_with_background_color(img, padding_color)
        return (slide_utils.get_success_response(content={}), rgb_img)

    def __get_associated_image(self, associated_image_name):
        try:
            if associated_image_name not in self.slide.associated_images:
                return (
                    slide_utils.get_error_response(
                        status_code=404, detail=f"Associated image '{associated_image_name}' does not exist"
                    ),
                    None,
                )
            associated_image_rgba = self.slide.associated_images[associated_image_name]
            return (
                slide_utils.get_success_response(
                    content={
                        "img_data_width": associated_image_rgba.width,
                        "img_data_height": associated_image_rgba.height,
                        "type": "IMAGE",
                    }
                ),
                associated_image_rgba.convert("RGB"),
            )
        except Exception as e:
            return (
                slide_utils.get_error_response(
                    status_code=500, detail=f"Error retrieving associated image {associated_image_name}. ({e})"
                ),
                None,
            )

    def __get_levels(self):
        original_levels = slide_utils.get_original_levels(
            self.slide.level_count,
            self.slide.level_dimensions,
            self.slide.level_downsamples,
        )
        return original_levels

    def __get_pixel_size(self):
        pixel_size_nm_x = -1
        pixel_size_nm_y = -1

        vendor = self.slide.properties[tiffslide.PROPERTY_NAME_VENDOR]
        if vendor is None or vendor in self.supported_vendor_formats.keys():
            mpp_x = self.slide.properties[tiffslide.PROPERTY_NAME_MPP_X]
            mpp_y = self.slide.properties[tiffslide.PROPERTY_NAME_MPP_Y]

            if mpp_x and mpp_y:
                pixel_size_nm_x = 1000.0 * float(mpp_x)
                pixel_size_nm_y = 1000.0 * float(mpp_y)

        return {"x": pixel_size_nm_x, "y": pixel_size_nm_y}

    def __get_tile_extent(self):
        tile_height = 256
        tile_width = 256
        if (
            "tiffslide.level[0].tile-height" in self.slide.properties
            and "tiffslide.level[0].tile-width" in self.slide.properties
        ):
            # some tiles can have an unequal tile height and width that can cause problems in the slide viewer
            # since the tile route is used for viewing only, we provide the default tile width and height
            temp_height = self.slide.properties["tiffslide.level[0].tile-height"]
            temp_width = self.slide.properties["tiffslide.level[0].tile-width"]
            if temp_height == temp_width:
                tile_height = temp_height
                tile_width = temp_width

        return {"x": tile_width, "y": tile_height, "z": 1}

    def __get_slide_info(self):
        try:
            levels = self.__get_levels()
            slide_info = {
                "id": "",
                "channels": slide_utils.get_rgb_channel_dict(),
                "channel_depth": 8,  # 8bit each channel
                "extent": {
                    "x": self.slide.dimensions[0],
                    "y": self.slide.dimensions[1],
                    "z": 1,
                },
                "pixel_size_nm": self.__get_pixel_size(),
                "tile_extent": self.__get_tile_extent(),
                "num_levels": len(levels),
                "levels": levels,
                "format": f"tiffslide-{self.format}",
            }
            return slide_utils.get_success_response(content=slide_info)
        except Exception as e:
            return slide_utils.get_error_response(status_code=500, detail=f"Failed to gather slide infos. [{e}]")

    def __get_thumbnail_tiffslide(self, max_x, max_y):
        level = self.__get_best_level_for_thumbnail(max_x, max_y)
        level_extent = self.slide_info["content"]["levels"][level]["extent"]

        try:
            img = self.slide.read_region((0, 0), level, (level_extent["x"], level_extent["y"]))
            # TODO: investigate, why this can be a ndarray in some cases although as_array is not set
            if isinstance(img, np.ndarray):
                return (
                    {"rep": "error", "status_code": 500, "detail": f"Not an instance of pil.Image"},
                    None,
                )
        except tiffslide.TiffFileError as e:
            return (
                {"rep": "error", "status_code": 500, "detail": f"TiffFileError: {e}"},
                None,
            )
        rgb_img = slide_utils.rgba_to_rgb_with_background_color(img, self.padding_color)
        return (
            slide_utils.get_success_response(
                content={"img_data_width": rgb_img.width, "img_data_height": rgb_img.height, "type": "IMAGE"}
            ),
            rgb_img,
        )

    def __get_best_level_for_thumbnail(self, max_x, max_y):
        best_level = 0
        for level in self.slide_info["content"]["levels"]:
            if level["extent"]["x"] < max_x and level["extent"]["y"] < max_y:
                return best_level - 1
            best_level += 1
        return best_level - 1

    def __is_jpeg_compression(self):
        tif_level = self.__get_tif_level_for_slide_level(0)
        page = tif_level.pages[0]
        return page.jpegtables is not None

    def __get_color_transform(self):
        tif_level = self.__get_tif_level_for_slide_level(0)
        page = tif_level.pages[0]
        color_transform = "Unknown"
        if page.photometric == 6:
            color_transform = "YCbCr"
        return color_transform

    def __read_raw_tile(self, page, tile_x, tile_y):
        image_width = page.keyframe.imagewidth
        tile_width = page.keyframe.tilewidth
        tile_per_line = int(np.ceil(image_width / tile_width))
        index = int(tile_y * tile_per_line + tile_x)
        offset = page.dataoffsets[index]
        bytecount = page.databytecounts[index]
        self.slide._tifffile.filehandle.seek(offset)
        data = self.slide._tifffile.filehandle.read(bytecount)
        return bytearray(data)

    def __add_jpeg_headers(self, page, data, color_transform):
        # add jpeg tables
        pos = data.find(b"\xff\xda")
        data[pos:pos] = page.jpegtables[2:-2]
        # check missing huffman tables
        if data.find(b"\xff\xc4") < 0:
            data[pos:pos] = self.__get_default_huffman_tables()
        # add APP14 data
        #
        # Marker: ff ee
        # Length (14 bytes): 00 0e
        # Adobe (ASCI): 41 64 6f 62 65
        # Version (100): 00 64
        # Flags0: 00 00
        # Flags1: 00 00
        # Color transform:
        # 00 = Unknown (RGB or CMYK)
        color_transform_value = b"\x00"
        # 01 = YCbCr
        if color_transform == "YCbCr":
            color_transform_value = b"\x01"
        data[pos:pos] = b"\xff\xee\x00\x0e\x41\x64\x6f\x62\x65\x00\x64\x00\x00\x00\x00" + color_transform_value

    def __adapt_level_0_location(self, level_0_location, downsample_factor, start_x, start_y):
        # adapting level_0_location to maintain better compatibility with openslide
        # check issue: https://github.com/bayer-science-for-a-better-life/tiffslide/issues/63
        tiffslide_start_x = int(level_0_location[0] / downsample_factor)
        tiffslide_start_y = int(level_0_location[1] / downsample_factor)
        if tiffslide_start_x != start_x:
            level_0_location = (
                (int)(level_0_location[0] + abs(tiffslide_start_x - start_x) * downsample_factor),
                level_0_location[1],
            )
        if tiffslide_start_y != start_y:
            level_0_location = (
                level_0_location[0],
                (int)(level_0_location[1] + abs(tiffslide_start_y - start_y) * downsample_factor),
            )
        return level_0_location

    def __get_default_huffman_tables(
        self,
    ):
        return b"\xff\xc4\x00\x1f\x00\x00\x01\x05\x01\x01\x01\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\xff\xc4\x00\xb5\x10\x00\x02\x01\x03\x03\x02\x04\x03\x05\x05\x04\x04\x00\x00\x01\x7d\x01\x02\x03\x00\x04\x11\x05\x12\x21\x31\x41\x06\x13\x51\x61\x07\x22\x71\x14\x32\x81\x91\xa1\x08\x23\x42\xb1\xc1\x15\x52\xd1\xf0\x24\x33\x62\x72\x82\x09\x0a\x16\x17\x18\x19\x1a\x25\x26\x27\x28\x29\x2a\x34\x35\x36\x37\x38\x39\x3a\x43\x44\x45\x46\x47\x48\x49\x4a\x53\x54\x55\x56\x57\x58\x59\x5a\x63\x64\x65\x66\x67\x68\x69\x6a\x73\x74\x75\x76\x77\x78\x79\x7a\x83\x84\x85\x86\x87\x88\x89\x8a\x92\x93\x94\x95\x96\x97\x98\x99\x9a\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xb2\xb3\xb4\xb5\xb6\xb7\xb8\xb9\xba\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9\xfa"  # noqa
