import glob
import os
from typing import List

import openslide
from cds_plugin_utils import slide_utils
from cds_plugin_utils.base_slide_instance import BaseSlideInstance


class OpenslideSlideInstance(BaseSlideInstance):
    supported_vendor_formats = {
        "aperio": [".svs"],
        "mirax": [".mrxs"],
        "hamamatsu": [".ndpi"],
        "ventana": [".bif", ".tif"],
        "leica": [".scn"],
        "trestle": [".tif"],
        "philips": [".tif"],
        "vsf": [".vsf"],
        "generic_tiff": [".tif", ".tiff"],
    }

    non_supported_format_extensions = ["ome.tif", "ome.tiff"]

    def __init__(self, filepath: str, padding_color: tuple[int, int] = (255, 255, 255)):
        data_dir = os.getenv("PLUGIN_OPENSLIDE_DATA_DIR") if os.getenv("PLUGIN_OPENSLIDE_DATA_DIR") else "/data"
        self.slide = None
        self.slide_info = None

        self.abs_filepath = self.__check_and_adapt_filepath(f"{data_dir.rstrip('/')}/{filepath.lstrip('/')}")
        self.padding_color = padding_color

        supported_formats_ext = []
        for key in self.supported_vendor_formats.keys():
            supported_formats_ext.extend(self.supported_vendor_formats[key])
        is_supported = self.__check_file_extension_supported(
            self.abs_filepath, self.non_supported_format_extensions, set(supported_formats_ext)
        )

        if is_supported:
            try:
                self.slide = openslide.OpenSlide(self.abs_filepath)
                storage_addresses = self.__get_storage_addresses(filepath=self.abs_filepath, data_dir=data_dir)
                self.slide_info = self.__get_slide_info_openslide()
                if self.slide_info["rep"] == "error":
                    self.result = self.slide_info
                else:
                    self.result = slide_utils.get_success_response(
                        content={
                            "is_supported": True,
                            "format": self.slide.properties[openslide.PROPERTY_NAME_VENDOR],
                            "plugin": "openslide",
                            "storage_addresses": storage_addresses,
                        }
                    )
            except openslide.OpenSlideError as e1:
                self.result = slide_utils.get_error_response(status_code=500, detail=f"OpenSlideError: {e1}")
            except Exception as e2:
                self.result = slide_utils.get_error_response(status_code=500, detail=f"Critical Error: {e2}")
        else:
            self.result = slide_utils.get_success_response(
                content={
                    "is_supported": False,
                    "format": None,
                    "plugin": "openslide",
                    "storage_addresses": [],
                }
            )

    def close(self):
        if self.slide:
            self.slide.close()

    def get_info(self):
        return self.slide_info

    def get_region(
        self,
        level: int,
        start_x: int,
        start_y: int,
        size_x: int,
        size_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        data, image = self.__get_region(level, start_x, start_y, size_x, size_y, padding_color, z=z)
        data["content"]["media_type"] = f"image/{image_format}"

        if image_channels:
            image = slide_utils.process_image_channels(image, image_channels)

        return data, slide_utils.image_to_byte_array(image, image_format, image_quality)

    def get_thumbnail(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        data, thumbnail = self.__get_associated_image("thumbnail")
        if data["rep"] == "error":
            data, thumbnail = self.__get_thumbnail_openslide(max_x, max_y)
            if data["rep"] == "error":
                return data, thumbnail

        thumbnail.thumbnail((max_x, max_y))
        data["content"]["media_type"] = f"image/{image_format}"
        return data, slide_utils.image_to_byte_array(thumbnail, image_format, image_quality)

    def get_label(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        data, label_image_data = self.__get_associated_image("label")
        if data["rep"] == "error":
            return data, label_image_data

        label_image_data.thumbnail((max_x, max_y))
        data["content"]["media_type"] = f"image/{image_format}"
        return data, slide_utils.image_to_byte_array(label_image_data, image_format, image_quality)

    def get_macro(self, max_x: int, max_y: int, image_format: str, image_quality: int):
        data, macro_image_data = self.__get_associated_image("macro")
        if data["rep"] == "error":
            return data, macro_image_data

        macro_image_data.thumbnail((max_x, max_y))
        data["content"]["media_type"] = f"image/{image_format}"
        return data, slide_utils.image_to_byte_array(macro_image_data, image_format, image_quality)

    def get_tile(
        self,
        level: int,
        tile_x: int,
        tile_y: int,
        image_format: str,
        image_quality: int,
        image_channels: List[int] = None,
        padding_color: tuple = None,
        z: int = 0,
    ):
        tile_extent_x = int(self.slide_info["content"]["tile_extent"]["x"])
        tile_extent_y = int(self.slide_info["content"]["tile_extent"]["y"])
        data, image = self.__get_region(
            level,
            tile_x * tile_extent_x,
            tile_y * tile_extent_y,
            tile_extent_x,
            tile_extent_y,
            padding_color,
            z=z,
        )

        if data["rep"] == "error":
            return data, image

        data["content"]["media_type"] = f"image/{image_format}"

        if image_channels:
            image = slide_utils.process_image_channels(image, image_channels)

        return data, slide_utils.image_to_byte_array(image, image_format, image_quality)

    # private

    def __check_file_extension_supported(self, filepath, unsupported_extensions, supported_extensions):
        if not filepath:
            return False

        unsupported_extensions = unsupported_extensions if unsupported_extensions else []
        supported_extensions = supported_extensions if supported_extensions else []

        for ext in unsupported_extensions:
            if str(filepath).endswith(ext):
                return False
        for ext in supported_extensions:
            if str(filepath).endswith(ext):
                return True

        return False

    def __check_and_adapt_filepath(self, filepath):
        if os.path.isdir(filepath):
            vsf_files = glob.glob(os.path.join(filepath, "*.vsf"))
            if len(vsf_files) > 0:
                filepath = vsf_files[0]
        return filepath

    def __get_storage_addresses(self, filepath, data_dir):
        storage_addresses = []
        # VSF files
        if os.path.isdir(filepath):
            vsf_files = glob.glob(filepath)
            for vfile in vsf_files:
                storage_addresses.append(
                    {
                        "main_address": str(vfile).endswith(".vsf"),
                        "address": self.__get_relative_filepath(filepath=filepath, data_dir=data_dir),
                    }
                )
            return storage_addresses

        # MRXS files
        if str(filepath).endswith(".mrxs"):
            storage_addresses.append(
                {"main_address": True, "address": self.__get_relative_filepath(filepath=filepath, data_dir=data_dir)}
            )
            sub_dir = str(filepath).replace(".mrxs", "")
            for sub_file in glob.glob(os.path.join(sub_dir, "*")):
                storage_addresses.append(
                    {
                        "main_address": False,
                        "address": self.__get_relative_filepath(filepath=sub_file, data_dir=data_dir),
                    }
                )
            return storage_addresses

        return [
            {
                "main_address": True,
                "address": self.__get_relative_filepath(filepath=filepath, data_dir=data_dir),
            }
        ]

    def __get_relative_filepath(self, filepath, data_dir):
        return f"{str(filepath).replace(data_dir, '').lstrip('/')}"

    def __get_region(
        self, level: int, start_x: int, start_y: int, size_x: int, size_y: int, padding_color=None, z: int = 0
    ):
        if padding_color is None:
            padding_color = self.padding_color
        downsample_factor = self.slide_info["content"]["levels"][level]["downsample_factor"]
        level_0_location = (
            (int)(start_x * downsample_factor),
            (int)(start_y * downsample_factor),
        )
        try:
            img = self.slide.read_region(level_0_location, level, (size_x, size_y))
        except openslide.OpenSlideError as e:
            return (
                {"rep": "error", "status_code": 500, "detail": f"OpenSlideError: {e}"},
                None,
            )
        rgb_img = slide_utils.rgba_to_rgb_with_background_color(img, padding_color)
        return (slide_utils.get_success_response(content={}), rgb_img)

    def __get_associated_image(self, associated_image_name):
        try:
            if associated_image_name not in self.slide.associated_images:
                resp = slide_utils.get_error_response(
                    status_code=404, detail=f"Associated image '{associated_image_name}' does not exist"
                )
                return (resp, None)

            associated_image_rgba = self.slide.associated_images[associated_image_name]

            return (slide_utils.get_success_response(content={}), associated_image_rgba.convert("RGB"))
        except openslide.OpenSlideError as e:
            return (
                {"rep": "error", "status_code": 500, "detail": f"OpenSlideError: {e}"},
                None,
            )

    def __get_levels_openslide(self):
        original_levels = slide_utils.get_original_levels(
            self.slide.level_count,
            self.slide.level_dimensions,
            self.slide.level_downsamples,
        )
        return original_levels

    def __get_pixel_size(self):
        if self.slide.properties[openslide.PROPERTY_NAME_VENDOR] == "generic-tiff":
            if self.slide.properties["tiff.ResolutionUnit"] == "centimeter":
                if "tiff.XResolution" not in self.slide.properties or "tiff.YResolution" not in self.slide.properties:
                    raise ValueError("Generic tiff file is missing valid values for x and y resolution.")

                pixel_per_cm_x = float(self.slide.properties["tiff.XResolution"])
                pixel_per_cm_y = float(self.slide.properties["tiff.YResolution"])
                pixel_size_nm_x = 1e7 / pixel_per_cm_x
                pixel_size_nm_y = 1e7 / pixel_per_cm_y
            else:
                raise ValueError("Unable to extract pixel size from metadata.")

        elif self.slide.properties[openslide.PROPERTY_NAME_VENDOR] in self.supported_vendor_formats.keys():
            pixel_size_nm_x = 1000.0 * float(self.slide.properties[openslide.PROPERTY_NAME_MPP_X])
            pixel_size_nm_y = 1000.0 * float(self.slide.properties[openslide.PROPERTY_NAME_MPP_Y])
        else:
            raise ValueError("Unable to extract pixel size from metadata.")

        return {"x": pixel_size_nm_x, "y": pixel_size_nm_y}

    def __get_tile_extent(self):
        tile_height = 256
        tile_width = 256
        if (
            "openslide.level[0].tile-height" in self.slide.properties
            and "openslide.level[0].tile-width" in self.slide.properties
        ):
            temp_height = self.slide.properties["openslide.level[0].tile-height"]
            temp_width = self.slide.properties["openslide.level[0].tile-width"]
            if temp_height == temp_width:
                tile_height = temp_height
                tile_width = temp_width

        return {"x": tile_width, "y": tile_height, "z": 1}

    def __get_slide_info_openslide(self):
        try:
            levels = self.__get_levels_openslide()
            slide_info = {
                "id": "",
                "channels": slide_utils.get_rgb_channel_dict(),
                "channel_depth": 8,  # 8 bit each channel
                "extent": {
                    "x": self.slide.dimensions[0],
                    "y": self.slide.dimensions[1],
                    "z": 1,
                },
                "pixel_size_nm": self.__get_pixel_size(),
                "tile_extent": self.__get_tile_extent(),
                "num_levels": len(levels),
                "levels": levels,
                "format": f"openslide-{self.slide.properties[openslide.PROPERTY_NAME_VENDOR]}",
            }
            return slide_utils.get_success_response(content=slide_info)
        except ValueError as e1:
            return slide_utils.get_error_response(status_code=404, detail=f"ValueError: {e1}")
        except Exception as e2:
            return slide_utils.get_error_response(status_code=500, detail=f"Failed to gather slide infos. [{e2}]")

    def __get_thumbnail_openslide(self, max_x, max_y):
        level = self.__get_best_level_for_thumbnail(max_x, max_y)
        level_extent = self.slide_info["content"]["levels"][level]["extent"]

        try:
            img = self.slide.read_region((0, 0), level, (level_extent["x"], level_extent["y"]))
        except openslide.OpenSlideError as e:
            return (
                slide_utils.get_error_response(status_code=500, detail=f"Could not retrieve image data: {e}"),
                None,
            )
        return slide_utils.get_success_response_with_payload(
            content={},
            raw_payload=slide_utils.rgba_to_rgb_with_background_color(img, self.padding_color),
        )

    def __get_best_level_for_thumbnail(self, max_x, max_y):
        best_level = 0
        for level in self.slide_info["content"]["levels"]:
            if level["extent"]["x"] < max_x and level["extent"]["y"] < max_y:
                return best_level - 1
            best_level += 1
        return best_level - 1
