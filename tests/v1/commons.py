import time
import uuid

import requests

from clinical_data_service.models.v1.clinical import Case, CaseList, ClinicalSlide, PostCase
from tests import TIMEOUT

from ..singletons import cds_url


def find_case_id():
    r = requests.get(f"{cds_url}/v1/cases", timeout=TIMEOUT)
    assert r.status_code == 200
    cases = r.json()
    CaseList.model_validate(cases)
    return cases["items"][0]["id"]


def new_case(case_id=None, description: str = None):
    if case_id:
        return {"id": str(case_id), "description": description, "creator_id": str(uuid.uuid4()), "creator_type": "USER"}
    else:
        return PostCase(creator_id=str(uuid.uuid4()), description=description, creator_type="USER").model_dump()


def new_slide(case: Case, tissue=None, slide_id=None):
    if slide_id:
        return {"id": str(slide_id), "case_id": str(case.id), "tissue": tissue}
    else:
        return {"case_id": str(case.id), "tissue": tissue}


def create_sample_data(without_slides=False):
    cases = []
    for _ in range(5):
        case = new_case()
        r = requests.post(f"{cds_url}/private/v1/cases", json=case, timeout=TIMEOUT)
        print(r.status_code)
        print(r.text)
        assert r.status_code == 200
        data = r.json()
        assert case["creator_id"] == data["creator_id"]
        cases.append(Case(**data))

    if without_slides:
        return cases, None

    slides = []
    for case, tissue in zip(cases[:3], ["BREAST", "KIDNEY", None]):
        for _ in range(2):
            slide = new_slide(case=case, tissue=tissue)
            r = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
            print(r.status_code)
            print(r.content)
            assert r.status_code == 200
            data = r.json()
            slides.append(ClinicalSlide(**data))

    return cases, slides


def create_sample_cases():
    cases, _ = create_sample_data(without_slides=True)
    return cases


def create_sample_case():
    case = new_case()
    r = requests.post(f"{cds_url}/private/v1/cases", json=case, timeout=TIMEOUT)
    print(r.status_code)
    print(r.text)
    assert r.status_code == 200
    data = r.json()
    assert case["creator_id"] == data["creator_id"]
    return Case.model_validate(data)


def create_sample_slides():
    _, slides = create_sample_data()
    return slides


def check_time(timestamp):
    return abs(timestamp - time.time()) <= 5


def get_error(response):
    return response.json()["detail"]["cause"]


def get_error_api(response):
    return response.json()["detail"][0]["msg"]
