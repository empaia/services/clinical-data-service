import io
import os

import requests
import tifffile
from PIL import Image

from clinical_data_service.models.v1.clinical import Case, CaseList, ClinicalSlide
from clinical_data_service.models.v1.slide import SlideInfo
from clinical_data_service.models.v1.storage import SlideStorage
from tests import TIMEOUT
from tests.v1.commons import new_case, new_slide

from ...singletons import cds_url


def make_dir_if_not_exists(dirpath):
    if not os.path.exists(dirpath):
        os.mkdir(dirpath)


def get_image(response):
    return Image.open(io.BytesIO(response.raw.data))


def get_tiff_image(response):
    return tifffile.TiffFile(io.BytesIO(response.raw.data))


def tiff_pixels_equal(tiff_image, pixel_location, testpixel):
    narray = tiff_image.asarray()
    pixel = narray[pixel_location[0]][pixel_location[1]][pixel_location[2]]
    if pixel != testpixel[pixel_location[0]]:
        return False
    return True


def get_slide_id_or_create(slide_filepath) -> str:
    response = requests.get(f"{cds_url}/v1/cases?with_slides=True", timeout=TIMEOUT)
    assert response.status_code == 200
    cases = CaseList.model_validate(response.json())
    case = next((case for case in cases.items if case.description == "WSI_TEST_CASE"), None)
    if case:
        for slide in case.slides:
            response = requests.get(f"{cds_url}/private/v1/slides/{slide.id}/storage", timeout=TIMEOUT)
            storage = SlideStorage.model_validate(response.json())
            if storage.main_storage_address.path == slide_filepath:
                return storage.slide_id

        # create slide for case
        slide = new_slide(case=case)
        slide["main_path"] = slide_filepath
        response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
        print(response.content)
        assert response.status_code == 200
        return str(ClinicalSlide.model_validate(response.json()).id)
    else:
        # create new case
        case = new_case(description="WSI_TEST_CASE")
        response = requests.post(f"{cds_url}/private/v1/cases", json=case, timeout=TIMEOUT)
        assert response.status_code == 200
        case_with_id = Case.model_validate(response.json())
        # add new slide
        slide = new_slide(case=case_with_id)
        slide["main_path"] = slide_filepath
        response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
        assert response.status_code == 200
        return str(ClinicalSlide.model_validate(response.json()).id)


def check_get_slide_info_valid(
    slide_id, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y, plugin=""
):
    response = requests.get(f"{cds_url}/v1/slides/{slide_id}/info?plugin={plugin}", timeout=TIMEOUT)
    print(response.content)
    assert response.status_code == 200
    slide_info = SlideInfo.model_validate(response.json())
    assert slide_info.id == slide_id
    assert slide_info.num_levels == num_levels
    assert round(slide_info.pixel_size_nm.x) == pixel_size_nm
    assert round(slide_info.pixel_size_nm.y) == pixel_size_nm
    assert slide_info.extent.x == x
    assert slide_info.extent.y == y
    assert slide_info.tile_extent.x == tile_size[0]
    assert slide_info.tile_extent.y == tile_size[1]
    assert len(slide_info.channels) == channels
    assert slide_info.channel_depth == channel_depth


def check_get_slide_thumbnail_valid(
    image_format,
    image_quality,
    slide_id,
    return_value,
    pixel_location,
    testpixel_rgb,
    testpixel_multichannel,
    plugin="",
):
    max_size_x = 21
    max_size_y = 22
    response = requests.get(
        (
            f"{cds_url}/v1/slides/{slide_id}/thumbnail/max_size/{max_size_x}/{max_size_y}"
            f"?image_format={image_format}&image_quality={image_quality}&plugin={plugin}"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == return_value
    assert response.headers["content-type"] == f"image/{image_format}"
    if response.headers["content-type"] == "image/tiff":
        image = get_tiff_image(response)
        x, y = image.pages.keyframe.imagewidth, image.pages.keyframe.imagelength
        assert (x == max_size_x) or (y == max_size_y)
        narray = image.asarray()
        for i, pixel in enumerate(testpixel_multichannel):
            c = narray[i][pixel_location[0]][pixel_location[1]]
            print(f"Assertion: {c} == {pixel}")
            assert c == pixel
    else:
        image = get_image(response)
        x, y = image.size
        print(f"x: {x}, y: {y}")
        assert (x == max_size_x) or (y == max_size_y)
        if image_format == "png":
            print(f"Assertion: {image.getpixel((pixel_location[0], pixel_location[1]))} == {testpixel_rgb}")
            assert image.getpixel((pixel_location[0], pixel_location[1])) == testpixel_rgb


def check_get_slide_label_valid(image_format, image_quality, slide_id, has_label, pixel_location, testpixel, plugin=""):
    max_x, max_y = 200, 200
    response = requests.get(
        (
            f"{cds_url}/v1/slides/{slide_id}/label/max_size/{max_x}/{max_y}"
            f"?image_format={image_format}&image_quality={image_quality}&plugin={plugin}"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    if has_label:
        assert response.status_code == 200
        assert response.headers["content-type"] == f"image/{image_format}"
        if response.headers["content-type"] == "image/tiff":
            image = get_tiff_image(response)
            narray = image.asarray()
            for i, value in enumerate(testpixel):
                c = narray[i][pixel_location[0]][pixel_location[1]]
                print(f"Assertion: {c} == {testpixel}")
                assert c == value
        else:
            image = get_image(response)
            if image_format == "png":
                print(f"Assertion: {image.getpixel((pixel_location[0], pixel_location[1]))} == {testpixel}")
                assert image.getpixel((pixel_location[0], pixel_location[1])) == testpixel
    else:
        assert response.status_code == 404


def check_get_slide_macro_valid(
    image_format, image_quality, slide_id, return_value, pixel_location, testpixel, plugin=""
):
    max_x, max_y = 200, 200
    response = requests.get(
        (
            f"{cds_url}/v1/slides/{slide_id}/macro/max_size/{max_x}/{max_y}"
            f"?image_format={image_format}&image_quality={image_quality}&plugin={plugin}"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == return_value
    if return_value == 200:
        if response.headers["content-type"] == "image/tiff":
            image = get_tiff_image(response)
            narray = image.asarray()
            for i, value in enumerate(testpixel):
                c = narray[i][pixel_location[0]][pixel_location[1]]
                print(f"Assertion: {c} == {value}")
                assert c == value
        else:
            image = get_image(response)
            if image_format == "png":
                print(f"Assertion: {image.getpixel((pixel_location[0], pixel_location[1]))} == {testpixel}")
                assert image.getpixel((pixel_location[0], pixel_location[1])) == testpixel


def check_get_slide_region_valid_brightfield(
    image_format, image_quality, slide_id, pixel_location, testpixel, start_x, start_y, size, plugin=""
):
    level = 0
    size_x = size
    size_y = size + 198
    response = requests.get(
        (
            f"{cds_url}/v1/slides/{slide_id}/region/level/{level}/"
            f"start/{start_x}/{start_y}/size/{size_x}/{size_y}"
            f"?image_format={image_format}&image_quality={image_quality}&plugin={plugin}"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == f"image/{image_format}"
    if response.headers["content-type"] == "image/tiff":
        image = get_tiff_image(response)
        x, y = image.pages.keyframe.imagewidth, image.pages.keyframe.imagelength
        assert (x == size_x) or (y == size_y)
        narray = image.asarray()
        r = narray[0][pixel_location[0]][pixel_location[1]]
        g = narray[1][pixel_location[0]][pixel_location[1]]
        b = narray[2][pixel_location[0]][pixel_location[1]]
        print(f"Assertion: {(r, g, b)} == {testpixel}")
        assert (r, g, b) == testpixel
    else:
        image = get_image(response)
        x, y = image.size
        assert (x == size_x) or (y == size_y)
        if image_format == "png":
            print(f"Assertion: {image.getpixel((pixel_location[0], pixel_location[1]))} == {testpixel}")
            assert image.getpixel((pixel_location[0], pixel_location[1])) == testpixel


def check_get_slide_region_valid_fluorescence(
    slide_id,
    channels,
    start_point,
    size,
    pixel_location,
    testpixel_multichannel,
    testpixel_rgb,
    image_format,
    image_quality,
    plugin="",
    padding_color=None,
):
    level = 2

    padding_color_param = ""
    if padding_color:
        padding_color_param = f"&padding_color={padding_color}".replace("#", "%23")

    response = requests.get(
        (
            f"{cds_url}/v1/slides/{slide_id}/region/level/{level}/"
            f"start/{start_point[0]}/{start_point[1]}/size/{size[0]}/{size[1]}"
            f"?image_format={image_format}&image_quality={image_quality}&plugin={plugin}{padding_color_param}"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == f"image/{image_format}"
    if response.headers["content-type"] == "image/tiff":
        image = get_tiff_image(response)
        assert image.pages.keyframe.shape[0] == channels
        x, y = image.pages.keyframe.imagewidth, image.pages.keyframe.imagelength
        assert (x == size[0]) or (y == size[1])
        narray = image.asarray()
        for i, value in enumerate(testpixel_multichannel):
            c = narray[i][pixel_location[0]][pixel_location[1]]
            print(f"Assertion: {c} == {value}")
            assert c == value
    else:
        image = get_image(response)
        x, y = image.size
        assert (x == size[0]) or (y == size[1])
        if image_format in ["png", "bmp"]:
            print(f"Assertion: {image.getpixel((pixel_location[0], pixel_location[1]))} == {testpixel_rgb}")
            assert image.getpixel((pixel_location[0], pixel_location[1])) == testpixel_rgb


def check_get_slide_region_dedicated_channel(
    slide_id,
    level,
    channels,
    start_point,
    size,
    pixel_location,
    testpixel_multichannel,
    testpixel_rgb,
    image_format,
    image_quality,
    plugin="",
):
    str_channels = "&".join([f"image_channels={str(ch)}" for ch in channels])
    response = requests.get(
        (
            f"{cds_url}/v1/slides/{slide_id}/region/level/{level}/"
            f"start/{start_point[0]}/{start_point[1]}/size/{size[0]}/{size[1]}"
            f"?image_format={image_format}&image_quality={image_quality}&{str_channels}&plugin={plugin}"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == f"image/{image_format}"
    if response.headers["content-type"] == "image/tiff":
        image = get_tiff_image(response)
        x, y = image.pages.keyframe.imagewidth, image.pages.keyframe.imagelength
        assert (x == size[0]) or (y == size[1])
        narray = image.asarray()
        for i, value in enumerate(testpixel_multichannel):
            c = narray[i][pixel_location[0]][pixel_location[1]]
            print(f"Assertion: {c} == {value}")
            assert c == value
    else:
        image = get_image(response)
        x, y = image.size
        assert (x == size[0]) or (y == size[1])
        if image_format == "png":
            print(f"Assertion: {image.getpixel((pixel_location[0], pixel_location[1]))} == {testpixel_rgb}")
            assert image.getpixel((pixel_location[0], pixel_location[1])) == testpixel_rgb


def check_get_slide_region_invalid_channel(slide_id, channels, expected_response, plugin=""):
    str_channels = "&".join([f"image_channels={str(ch)}" for ch in channels])
    response = requests.get(
        f"{cds_url}/v1/slides/{slide_id}/region/level/2/start/0/0/size/64/64?{str_channels}&plugin={plugin}",
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == expected_response


def check_get_slide_region_invalid(slide_id, testpixel, start_x, start_y, size, status_code, plugin=""):
    level = 0
    size_x = size
    size_y = size + 198
    response = requests.get(
        (
            f"{cds_url}/v1/slides/{slide_id}/region/level/{level}/"
            f"start/{start_x}/{start_y}/size/{size_x}/{size_y}?plugin={plugin}"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == status_code


def check_get_slide_tile_valid(
    image_format,
    image_quality,
    slide_id,
    testpixel,
    tile_x,
    tile_y,
    tile_size,
    pixel_location=(0, 0),
    plugin="",
    padding_color=None,
):
    level = 0

    padding_color_param = ""
    if padding_color:
        padding_color_param = f"&padding_color={padding_color}".replace("#", "%23")

    response = requests.get(
        (
            f"{cds_url}/v1/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}"
            f"?image_format={image_format}&image_quality={image_quality}&plugin={plugin}{padding_color_param}"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == 200
    assert response.headers["content-type"] == f"image/{image_format}"
    if response.headers["content-type"] == "image/tiff":
        image = get_tiff_image(response)
        x, y = image.pages.keyframe.imagewidth, image.pages.keyframe.imagelength
        assert (x == tile_size[0]) or (y == tile_size[1])
        narray = image.asarray()
        for i, value in enumerate(testpixel):
            c = narray[i][pixel_location[0]][pixel_location[1]]
            print(f"Assertion: {c} == {value}")
            assert c == value
    else:
        image = get_image(response)
        x, y = image.size
        assert (x == tile_size[0]) or (y == tile_size[1])
        if image_format == "png":
            print(f"Assertion: {image.getpixel((pixel_location[0], pixel_location[1]))} == {testpixel}")
            assert image.getpixel((0, 0)) == testpixel
