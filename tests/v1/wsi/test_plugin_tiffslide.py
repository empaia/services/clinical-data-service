import pytest

from .validation_utils import (
    check_get_slide_info_valid,
    check_get_slide_label_valid,
    check_get_slide_macro_valid,
    check_get_slide_region_dedicated_channel,
    check_get_slide_region_invalid,
    check_get_slide_region_invalid_channel,
    check_get_slide_region_valid_brightfield,
    check_get_slide_thumbnail_valid,
    check_get_slide_tile_valid,
    get_slide_id_or_create,
)


@pytest.mark.parametrize(
    "slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y",
    [
        (
            # TIFF
            "/data/Generic TIFF/CMU-1.tiff",
            3,
            8,
            10,
            50,
            (128, 128),
            46000,
            32914,
        ),
        (
            # SVS
            "/data/Aperio/CMU-1.svs",
            3,
            8,
            3,
            499,
            (256, 256),
            46000,
            32914,
        ),
    ],
)
def test_get_slide_info_valid(slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_info_valid(
        slide_id,
        channels,
        channel_depth,
        num_levels,
        pixel_size_nm,
        tile_size,
        x,
        y,
        plugin="cds-plugin-tiffslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel_rgb, testpixel_multichannel",
    [
        (
            "/data/Generic TIFF/CMU-1.tiff",
            200,
            (5, 5),
            (245, 247, 246),
            (245, 247, 246),
        ),
        (
            "/data/Aperio/CMU-1.svs",
            200,
            (5, 5),
            (244, 246, 245),
            (244, 246, 245),
        ),
    ],
)
def test_get_slide_thumbnail_valid(
    image_format,
    image_quality,
    slide_filepath,
    return_value,
    pixel_location,
    testpixel_rgb,
    testpixel_multichannel,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_thumbnail_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel_rgb,
        testpixel_multichannel,
        plugin="cds-plugin-tiffslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, has_label, pixel_location, testpixel",
    [
        ("/data/Generic TIFF/CMU-1.tiff", False, (), ()),
        ("/data/Aperio/CMU-1.svs", True, (0, 0), (0, 0, 0)),
        ("/data/Aperio/CMU-1.svs", True, (50, 50), (202, 131, 51)),
    ],
)
def test_get_slide_label_valid(image_format, image_quality, slide_filepath, has_label, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_label_valid(
        image_format,
        image_quality,
        slide_id,
        has_label,
        pixel_location,
        testpixel,
        plugin="cds-plugin-tiffslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel",
    [
        ("/data/Generic TIFF/CMU-1.tiff", 404, (), ()),
        ("/data/Aperio/CMU-1.svs", 200, (0, 0), (0, 0, 0)),
        ("/data/Aperio/CMU-1.svs", 200, (50, 50), (238, 240, 240)),
    ],
)
def test_get_slide_macro_valid(image_format, image_quality, slide_filepath, return_value, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_macro_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel,
        plugin="cds-plugin-tiffslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 100), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath,  pixel_location, testpixel, start_x, start_y, size",
    [
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (0, 0),
            (255, 236, 253),
            15000,
            15000,
            345,
        ),
        (
            "/data/Aperio/CMU-1.svs",
            (0, 0),
            (255, 235, 255),
            15000,
            15000,
            345,
        ),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (0, 0),
            (255, 255, 255),
            -1_000_000,
            -1_000_000,
            1,
        ),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (0, 0),
            (255, 255, 255),
            1_000_000,
            1_000_000,
            1,
        ),
    ],
)
def test_get_slide_region_valid_brightfield(
    image_format,
    image_quality,
    slide_filepath,
    pixel_location,
    testpixel,
    start_x,
    start_y,
    size,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_valid_brightfield(
        image_format,
        image_quality,
        slide_id,
        pixel_location,
        testpixel,
        start_x,
        start_y,
        size,
        plugin="cds-plugin-tiffslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, testpixel, tile_x, tile_y, tile_size",
    [
        ("/data/Generic TIFF/CMU-1.tiff", (243, 243, 243), 21, 22, (128, 128)),
        ("/data/Aperio/CMU-1.svs", (246, 246, 243), 21, 22, (256, 256)),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (255, 255, 255),
            -1_000_000,
            -1_000_000,
            (128, 128),
        ),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (255, 255, 255),
            1_000_000,
            1_000_000,
            (128, 128),
        ),
    ],
)
def test_get_slide_tile_valid(image_format, image_quality, slide_filepath, testpixel, tile_x, tile_y, tile_size):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_tile_valid(
        image_format,
        image_quality,
        slide_id,
        testpixel,
        tile_x,
        tile_y,
        tile_size,
        plugin="cds-plugin-tiffslide",
    )


@pytest.mark.parametrize(
    "slide_filepath, testpixel, start_x, start_y, size, status_code",
    [("/data/Generic TIFF/CMU-1.tiff", (223, 217, 222), 15000, 15000, 30045, 422)],
)
def test_get_slide_region_invalid(slide_filepath, testpixel, start_x, start_y, size, status_code):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_invalid(
        slide_id, testpixel, start_x, start_y, size, status_code, plugin="cds-plugin-tiffslide"
    )


@pytest.mark.parametrize("image_format, image_quality", [("png", 100), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, level, channels, start_point, size, pixel_location, testpixel_multichannel, testpixel_rgb",
    [
        (
            "/data/Generic TIFF/CMU-1.tiff",
            7,
            [0],
            (0, 0),
            (256, 256),
            (128, 128),
            [246],
            (246, 0, 0),
        )
    ],
)
def test_get_slide_region_dedicated_channel(
    slide_filepath,
    level,
    channels,
    start_point,
    size,
    pixel_location,
    testpixel_multichannel,
    testpixel_rgb,
    image_format,
    image_quality,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_dedicated_channel(
        slide_id,
        level,
        channels,
        start_point,
        size,
        pixel_location,
        testpixel_multichannel,
        testpixel_rgb,
        image_format,
        image_quality,
        plugin="cds-plugin-tiffslide",
    )


@pytest.mark.parametrize(
    "slide_filepath, channels, expected_response",
    [
        ("/data/Generic TIFF/CMU-1.tiff", [2], 200),
        ("/data/Generic TIFF/CMU-1.tiff", [4], 400),
    ],
)
def test_get_slide_region_invalid_channel(slide_filepath, channels, expected_response):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_invalid_channel(slide_id, channels, expected_response, plugin="cds-plugin-tiffslide")
