import pytest

from .validation_utils import (
    check_get_slide_info_valid,
    check_get_slide_region_valid_brightfield,
    check_get_slide_thumbnail_valid,
    check_get_slide_tile_valid,
    get_slide_id_or_create,
)


@pytest.mark.parametrize(
    "slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y",
    [
        # JPG
        ("/data/PIL/aperio_small.jpeg", 3, 8, 1, -1, (256, 256), 500, 358),
        # PNG
        ("/data/PIL/aperio_small.png", 3, 8, 1, -1, (256, 256), 500, 358),
    ],
)
def test_get_slide_info_valid(slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_info_valid(
        slide_id,
        channels,
        channel_depth,
        num_levels,
        pixel_size_nm,
        tile_size,
        x,
        y,
        plugin="cds-plugin-pil",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90)])
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel_rgb",
    [
        (
            "/data/PIL/aperio_small.jpeg",
            200,
            (5, 5),
            (245, 246, 246),
        ),
        (
            "/data/PIL/aperio_small.png",
            200,
            (5, 5),
            (243, 245, 244),
        ),
    ],
)
def test_get_slide_thumbnail_valid(
    image_format,
    image_quality,
    slide_filepath,
    return_value,
    pixel_location,
    testpixel_rgb,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_thumbnail_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel_rgb,
        (None, None, None),
        plugin="cds-plugin-pil",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90)])
@pytest.mark.parametrize(
    "slide_filepath, testpixel, tile_x, tile_y, tile_size",
    [
        ("/data/PIL/aperio_small.jpeg", (243, 243, 243), 0, 0, (256, 256)),
        ("/data/PIL/aperio_small.png", (243, 243, 243), 0, 0, (256, 256)),
        (
            "/data/PIL/aperio_small.jpeg",
            (255, 255, 255),
            -1_000_000,
            -1_000_000,
            (256, 256),
        ),
        (
            "/data/PIL/aperio_small.jpeg",
            (255, 255, 255),
            1_000_000,
            1_000_000,
            (256, 256),
        ),
    ],
)
def test_get_slide_tile_valid(image_format, image_quality, slide_filepath, testpixel, tile_x, tile_y, tile_size):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_tile_valid(
        image_format,
        image_quality,
        slide_id,
        testpixel,
        tile_x,
        tile_y,
        tile_size,
        plugin="cds-plugin-pil",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90)])
@pytest.mark.parametrize(
    "slide_filepath,  pixel_location, testpixel, start_x, start_y, size",
    [
        (
            "/data/PIL/aperio_small.jpeg",
            (0, 0),
            (243, 243, 243),
            0,
            0,
            128,
        ),
        (
            "/data/PIL/aperio_small.jpeg",
            (0, 0),
            (255, 255, 255),
            -1_000_000,
            -1_000_000,
            128,
        ),
        (
            "/data/PIL/aperio_small.jpeg",
            (0, 0),
            (255, 255, 255),
            1_000_000,
            1_000_000,
            128,
        ),
    ],
)
def test_get_slide_region_valid_brightfield(
    image_format,
    image_quality,
    slide_filepath,
    pixel_location,
    testpixel,
    start_x,
    start_y,
    size,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_valid_brightfield(
        image_format,
        image_quality,
        slide_id,
        pixel_location,
        testpixel,
        start_x,
        start_y,
        size,
        plugin="cds-plugin-pil",
    )
