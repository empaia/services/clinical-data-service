import uuid

import requests

from tests.v1.commons import create_sample_cases, new_slide

from ... import TIMEOUT
from ...singletons import cds_url


def test_main():
    cases = create_sample_cases()
    slide = new_slide(case=cases[0])
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    slide_id = response.json()["id"]

    slide_storage_info = {
        "main_storage_address": {"storage_address_id": str(uuid.uuid4()), "path": "folder/slide.tiff"}
    }
    r = requests.put(f"{cds_url}/private/v1/slides/{slide_id}/storage", json=slide_storage_info, timeout=TIMEOUT)
    assert r.status_code == 404
    assert r.json()["detail"] == "File or directory 'folder/slide.tiff' does not exist"

    r = requests.get(cds_url + f"/private/v1/slides/{slide_id}/storage", timeout=TIMEOUT)
    assert r.status_code == 404
    assert r.json()["detail"] == f"No slide storage defined for slide {slide_id}"

    slide_storage_info = {"main_storage_address": {"storage_address_id": str(uuid.uuid4()), "path": "Aperio/CMU-1.svs"}}
    r = requests.put(f"{cds_url}/private/v1/slides/{slide_id}/storage", json=slide_storage_info, timeout=TIMEOUT)
    assert r.status_code == 200
    assert r.json()["main_storage_address"]["path"] == "Aperio/CMU-1.svs"

    r = requests.delete(cds_url + f"/private/v1/slides/{slide_id}/storage", timeout=TIMEOUT)
    assert r.status_code == 200

    r = requests.get(cds_url + f"/private/v1/slides/{slide_id}/storage", timeout=TIMEOUT)
    assert r.status_code == 404
    assert r.json()["detail"] == f"No slide storage defined for slide {slide_id}"

    r = requests.put(f"{cds_url}/private/v1/slides/{slide_id}/storage", json=slide_storage_info, timeout=TIMEOUT)
    assert r.status_code == 200

    r = requests.get(cds_url + f"/private/v1/slides/{slide_id}/storage", timeout=TIMEOUT)
    assert r.status_code == 200


def test_main_get_unknown_slide_id():
    r = requests.get(f"{cds_url}/private/v1/slides/unknown_id/storage", timeout=TIMEOUT)
    assert r.status_code == 400
    assert r.json()["detail"]["cause"] == "Slide not found: badly formed"

    slide_id = str(uuid.uuid4())
    r = requests.get(f"{cds_url}/private/v1/slides/{slide_id}/storage", timeout=TIMEOUT)
    assert r.status_code == 400
    assert r.json()["detail"]["cause"] == f"Slide not found: {slide_id} does not exist"


def test_update_slide_storage():
    cases = create_sample_cases()
    slide = new_slide(case=cases[0])
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    slide_id = response.json()["id"]

    slide_storage_info = {"main_storage_address": {"storage_address_id": str(uuid.uuid4()), "path": "Aperio/CMU-1.svs"}}
    r = requests.put(f"{cds_url}/private/v1/slides/{slide_id}/storage", json=slide_storage_info, timeout=TIMEOUT)
    assert r.status_code == 200

    slide_storage_info = {
        "main_storage_address": {"storage_address_id": str(uuid.uuid4()), "path": "MIRAX/Mirax2.2-1.mrxs"},
        "secondary_storage_addresses": [{"storage_address_id": str(uuid.uuid4()), "path": "not/eistent/file.ext"}],
    }
    r = requests.put(f"{cds_url}/private/v1/slides/{slide_id}/storage", json=slide_storage_info, timeout=TIMEOUT)
    assert r.status_code == 404

    slide_storage_info = {
        "main_storage_address": {"storage_address_id": str(uuid.uuid4()), "path": "MIRAX/Mirax2.2-1.mrxs"},
        "secondary_storage_addresses": [
            {"storage_address_id": str(uuid.uuid4()), "path": "MIRAX/Mirax2.2-1/Slidedat.ini"}
        ],
    }
    r = requests.put(f"{cds_url}/private/v1/slides/{slide_id}/storage", json=slide_storage_info, timeout=TIMEOUT)
    assert r.status_code == 200
    assert r.json()["main_storage_address"] == slide_storage_info["main_storage_address"]
    assert r.json()["secondary_storage_addresses"] == slide_storage_info["secondary_storage_addresses"]
