import pytest
import requests

from clinical_data_service.models.v1.clinical import ClinicalSlide
from clinical_data_service.models.v1.storage import SlideStorage

from ... import TIMEOUT
from ...singletons import cds_url
from ...v1.commons import create_sample_case, new_slide


@pytest.mark.parametrize("wsi_filepath", ["/data/Aperio/CMU-1.svs", "/data/Hamamatsu/OS-1.ndpi"])
def test_create_slide_with_automated_single_wsi_file_indexing(wsi_filepath: str):
    case = create_sample_case()
    slide = new_slide(case=case)
    slide["main_path"] = wsi_filepath

    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    print(response.text)
    assert response.status_code == 200
    cls = ClinicalSlide.model_validate(response.json())

    response = requests.get(f"{cds_url}/private/v1/slides/{cls.id}/storage", timeout=TIMEOUT)
    assert response.status_code == 200
    storage = SlideStorage.model_validate(response.json())
    assert wsi_filepath.endswith(storage.main_storage_address.path)
    assert storage.secondary_storage_addresses == []


@pytest.mark.parametrize(
    "wsi_filepath, related_files_count",
    [("/data/MIRAX/Mirax2.2-1.mrxs", 25), ("/data/DICOM_wsidicomizer/DICOM_Aperio", 5)],
)
def test_create_slide_with_automated_multi_wsi_file_indexing(wsi_filepath: str, related_files_count: int):
    case = create_sample_case()
    slide = new_slide(case=case)
    slide["main_path"] = wsi_filepath

    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 200
    cls = ClinicalSlide.model_validate(response.json())

    response = requests.get(f"{cds_url}/private/v1/slides/{cls.id}/storage", timeout=TIMEOUT)
    assert response.status_code == 200
    storage = SlideStorage.model_validate(response.json())
    assert len(storage.secondary_storage_addresses) == related_files_count
    assert wsi_filepath.endswith(storage.main_storage_address.path)
    for addr in storage.secondary_storage_addresses:
        parent_dir = wsi_filepath.split("/")[0]
        assert addr.path.startswith(parent_dir)


@pytest.mark.parametrize(
    "wsi_filepath",
    [
        "/data/_not/_existing.svs",
        "/data/_not/_existing.ndpi",
        "/data/_not/_existing.mrxs",
        "/data/_not/_existing",
        "/data/_not/_existing.tiff",
    ],
)
def test_create_slide_with_non_existant_wsi_file(wsi_filepath: str):
    case = create_sample_case()
    slide = new_slide(case=case)
    slide["main_path"] = wsi_filepath

    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 404
    fp = wsi_filepath.replace("/data/", "")
    assert response.json()["detail"] == f"File or directory '{fp}' does not exist"
