import uuid

import pytest
import requests

from clinical_data_service.models.v1.clinical import ClinicalSlide
from clinical_data_service.singletons import settings

from ... import TIMEOUT
from ...singletons import cds_url
from ..commons import (
    check_time,
    create_sample_cases,
    create_sample_data,
    create_sample_slides,
    get_error,
    get_error_api,
    new_slide,
)


def test_create_slide():
    """create a new Slide and just check (the id) is returned"""
    cases = create_sample_cases()
    slide = new_slide(case=cases[0])
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 200
    ClinicalSlide(**response.json())


@pytest.mark.skipif(settings.allow_external_ids, reason="CDS_ALLOW_EXTERNAL_IDS set to True in .env")
def test_create_slide_ignore_id():
    """
    ONLY EXECUTED IF CDS_ALLOW_EXTERNAL_IDS=False
    create a new slide and check that id was ignored
    """
    cases = create_sample_cases()
    slide = new_slide(case=cases[0], slide_id=uuid.uuid4())
    params = {"external_ids": True}
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, params=params, timeout=TIMEOUT)
    assert response.status_code == 200
    created_slide = response.json()
    ClinicalSlide(**created_slide)
    assert created_slide["id"] != slide["id"]


@pytest.mark.skipif(not settings.allow_external_ids, reason="CDS_ALLOW_EXTERNAL_IDS set to False in .env")
def test_create_slide_external_id():
    """
    ONLY EXECUTED IF CDS_ALLOW_EXTERNAL_IDS=True
    create a new slide and check that id was ignored
    """
    cases = create_sample_cases()
    slide = new_slide(case=cases[0], slide_id=uuid.uuid4())
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 200
    created_slide = response.json()
    ClinicalSlide(**created_slide)
    assert created_slide["id"] != slide["id"]

    # create a new slide and check that id was set"""
    params = {"external_ids": True}
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, params=params, timeout=TIMEOUT)
    assert response.status_code == 200
    created_slide = response.json()
    ClinicalSlide(**created_slide)
    assert created_slide["id"] == slide["id"]


def test_create_slide_missing_attributes():
    """a Slide must refer to a case"""
    response = requests.post(f"{cds_url}/private/v1/slides", json={}, timeout=TIMEOUT)
    assert response.status_code == 422
    assert "Field required" in get_error_api(response)


def test_create_slide_unknown_attributes():
    """creating a Slide with unknown attributes produces an error"""
    cases = create_sample_cases()
    slide = new_slide(case=cases[0])
    slide["unknown"] = "unknown"
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 422
    assert "Extra inputs" in get_error_api(response)


def test_create_slide_illegal_value():
    """try to create a Slide with an illegal value"""
    cases = create_sample_cases()
    slide = new_slide(case=cases[0])
    slide["tissue"] = "NO_TISSUE"
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 422
    assert "not a valid" in response.text


def test_create_slide_unknown_case_id():
    """try to create a Slide with a reference to a non-existing case"""
    cases = create_sample_cases()
    slide = new_slide(case=cases[0])
    slide["case_id"] = str(uuid.uuid4())
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 400
    assert "Case not found" in get_error(response)


def test_create_slide_check_id():
    """create a new Slide, get the Slide back, and compare the ID"""
    cases = create_sample_cases()
    slide = new_slide(case=cases[0])
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    slide = ClinicalSlide(**response.json())
    response = requests.get(f"{cds_url}/v1/slides/{slide.id}", timeout=TIMEOUT)
    slide2 = ClinicalSlide(**response.json())
    assert slide == slide2


def test_create_slide_check_attribute():
    """create a new Slide, get the Slide back, and compare an attribute"""
    cases = create_sample_cases()
    slide = new_slide(case=cases[0], tissue="BREAST")
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    slide = ClinicalSlide(**response.json())
    assert slide.tissue == "BREAST"


def test_create_slide_check_created_time():
    """create a new Slide, get the Slide back, and compare created_time"""
    cases = create_sample_cases()
    slide = new_slide(case=cases[0])
    response = requests.post(f"{cds_url}/private/v1/slides", json=slide, timeout=TIMEOUT)
    slide = ClinicalSlide(**response.json())
    assert check_time(slide.created_at)


def test_get_slide_get_unknown_id():
    """try to get a Slide with an unknown ID"""
    response = requests.get(f"{cds_url}/v1/slides/{str(uuid.uuid4())}", timeout=TIMEOUT)
    assert response.status_code == 400
    assert "Slide not found" in get_error(response)


def test_get_slides():
    create_sample_slides()
    r = requests.get(f"{cds_url}/v1/slides/", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    item_count = data.get("item_count")
    items = data.get("items")
    assert item_count == len(items)
    ids = [e["id"] for e in items]
    assert len(ids) == len(set(ids))


def test_get_slides_skip_limit():
    create_sample_slides()
    r = requests.get(f"{cds_url}/v1/slides/", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    item_count = data.get("item_count")
    items = data.get("items")
    assert item_count == len(items)

    r = requests.get(f"{cds_url}/v1/slides/?skip=1", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    skip_items = data.get("items")
    assert len(skip_items) == item_count - 1
    assert items[2]["id"] == skip_items[1]["id"]

    r = requests.get(f"{cds_url}/v1/slides/?limit=2", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    limit_items = data.get("items")
    assert len(limit_items) == 2
    assert items[0]["id"] == limit_items[0]["id"]
    assert items[1]["id"] == limit_items[1]["id"]

    r = requests.get(f"{cds_url}/v1/slides/?skip=2&limit=2", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    skip_limit_items = data.get("items")
    assert len(skip_limit_items) == 2
    assert items[2]["id"] == skip_limit_items[0]["id"]
    assert items[3]["id"] == skip_limit_items[1]["id"]


def test_query_slides():
    cases, _ = create_sample_data()
    r = requests.put(f"{cds_url}/v1/slides/query", json={"cases": [str(cases[0].id)]}, timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    item_count = data.get("item_count")
    items = data.get("items")
    assert item_count == 2
    assert len(items) == 2


def test_query_slides_invalid_case():
    r = requests.put(f"{cds_url}/v1/slides/query", json={"cases": ["case_id"]}, timeout=TIMEOUT)
    assert r.status_code == 422


def test_query_slides_no_match():
    r = requests.put(f"{cds_url}/v1/slides/query", json={"cases": [str(uuid.uuid4())]}, timeout=TIMEOUT)
    print(r.text)
    assert r.status_code == 200
    data = r.json()
    assert data["items"] == []
    assert data["item_count"] == 0


def test_query_slides_sorted():
    cases, _ = create_sample_data()
    case_ids = [str(case.id) for case in cases]

    r = requests.put(f"{cds_url}/v1/slides/query", json={"cases": case_ids[:-1]}, timeout=TIMEOUT)
    items = r.json()["items"]
    ordered = sorted(items, key=lambda x: x["created_at"], reverse=True)
    assert items == ordered

    r = requests.put(f"{cds_url}/v1/slides/query?skip=1&limit=2", json={"cases": case_ids[:-1]}, timeout=TIMEOUT)
    items = r.json()["items"]
    assert items == ordered[1:3]


def test_put_slides_query_skip_limit():
    create_sample_slides()

    r = requests.get(f"{cds_url}/v1/slides/", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    item_count = data.get("item_count")
    items = data.get("items")
    assert item_count == len(items)

    r = requests.put(f"{cds_url}/v1/slides/query?skip=1", json={}, timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    skip_items = data.get("items")
    assert len(skip_items) == item_count - 1
    assert items[2]["id"] == skip_items[1]["id"]

    r = requests.put(f"{cds_url}/v1/slides/query?limit=2", json={}, timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    limit_items = data.get("items")
    assert len(limit_items) == 2
    assert items[0]["id"] == limit_items[0]["id"]
    assert items[1]["id"] == limit_items[1]["id"]

    r = requests.put(f"{cds_url}/v1/slides/query?skip=2&limit=2", json={}, timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    skip_limit_items = data.get("items")
    assert len(skip_limit_items) == 2
    assert items[2]["id"] == skip_limit_items[0]["id"]
    assert items[3]["id"] == skip_limit_items[1]["id"]


def test_update_slide_check_attribute():
    """create Slide, update attribute, get Slide, check if the attribute is there"""
    slides = create_sample_slides()
    slide_id = slides[0].id
    response = requests.put(
        f"{cds_url}/private/v1/slides/{slide_id}", json={"tissue": "BREAST", "deleted": True}, timeout=TIMEOUT
    )
    assert response.status_code == 200
    slide = ClinicalSlide(**response.json())
    assert slide.tissue == "BREAST"
    assert slide.deleted
    response = requests.get(f"{cds_url}/v1/slides/{slide_id}", timeout=TIMEOUT)
    slide = ClinicalSlide(**response.json())
    assert slide.tissue == "BREAST"
    assert slide.deleted


def test_update_slide_check_updated_at():
    """create Slide, update some attribute, check updated-at attribute"""
    slides = create_sample_slides()
    slide_id = slides[0].id
    response = requests.put(f"{cds_url}/private/v1/slides/{slide_id}", json={"tissue": "BREAST"}, timeout=TIMEOUT)
    slide = ClinicalSlide(**response.json())
    assert check_time(slide.updated_at)


def test_update_slide_unknown_id():
    """try to update a Slide with an unknown ID"""
    response = requests.put(
        f"{cds_url}/private/v1/slides/{str(uuid.uuid4())}", json={"tissue": "Breast"}, timeout=TIMEOUT
    )
    assert response.status_code == 400
    assert "Slide not found" in get_error(response)


def test_update_slide_unknown_attribute():
    """trying to update a Slide with unknown attributes raises an error"""
    slides = create_sample_slides()
    slide_id = slides[0].id
    response = requests.put(f"{cds_url}/private/v1/slides/{slide_id}", json={"unknown": "unknown"}, timeout=TIMEOUT)
    assert response.status_code == 422


def test_update_slide_illegal_attribute():
    """try to update different attributes that are not meant to be updated, e.g. ID or created_at"""
    slides = create_sample_slides()
    slide_id = slides[0].id
    for attr in ["id", "created_at", "updated_at", "case_id"]:
        response = requests.put(
            f"{cds_url}/private/v1/slides/{slide_id}", json={attr: "should not be updated"}, timeout=TIMEOUT
        )
        assert response.status_code == 422


def test_update_slide_illegal_value():
    """try to update a valid attribute, but with an illegal value"""
    slides = create_sample_slides()
    slide_id = slides[0].id
    response = requests.put(f"{cds_url}/private/v1/slides/{slide_id}", json={"tissue": "NO_TISSUE"}, timeout=TIMEOUT)
    assert response.status_code == 422
    response = requests.put(f"{cds_url}/private/v1/slides/{slide_id}", json={"deleted": "text"}, timeout=TIMEOUT)
    assert response.status_code == 422


@pytest.mark.skip
def test_update_slide_unknown_case():
    """currently, the case_id attribute can not be updated; should that be changed, implement this test"""


def test_tags():
    """Test the "tags" endpoint"""
    response = requests.get(f"{cds_url}/v1/tags", timeout=TIMEOUT)
    tags = response.json()
    assert tags["TISSUE"]["SKIN"] == {"DE": "Haut", "EN": "Skin"}
