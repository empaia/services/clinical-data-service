import uuid

import pytest
import requests

from clinical_data_service.models.v1.clinical import Case
from clinical_data_service.singletons import settings

from ... import TIMEOUT
from ...singletons import cds_url
from ..commons import (
    check_time,
    create_sample_cases,
    create_sample_data,
    find_case_id,
    get_error,
    get_error_api,
    new_case,
)


def test_create_case():
    """create a new case and just check a string (the id) is returned"""
    response = requests.post(f"{cds_url}/private/v1/cases", json=new_case(), timeout=TIMEOUT)
    assert response.status_code == 200
    Case(**response.json())


@pytest.mark.skipif(settings.allow_external_ids, reason="CDS_ALLOW_EXTERNAL_IDS set to True in .env")
def test_create_case_ignore_id():
    """
    ONLY EXECUTED IF CDS_ALLOW_EXTERNAL_IDS=False
    create a new case and check that id was ignored
    """
    case = new_case(case_id=uuid.uuid4())
    params = {"external_ids": True}
    response = requests.post(f"{cds_url}/private/v1/cases", json=case, params=params, timeout=TIMEOUT)
    assert response.status_code == 200
    created_case = response.json()
    Case(**created_case)
    assert created_case["id"] != case["id"]


@pytest.mark.skipif(not settings.allow_external_ids, reason="CDS_ALLOW_EXTERNAL_IDS set to False in .env")
def test_create_case_external_id():
    """
    ONLY EXECUTED IF CDS_ALLOW_EXTERNAL_IDS=True
    create a new case and check that id was ignored
    """
    case = new_case(case_id=uuid.uuid4())
    response = requests.post(f"{cds_url}/private/v1/cases", json=case, timeout=TIMEOUT)
    assert response.status_code == 200
    created_case = response.json()
    Case(**created_case)
    assert created_case["id"] != case["id"]

    # create a new case and check that id was set"""
    params = {"external_ids": True}
    response = requests.post(f"{cds_url}/private/v1/cases", json=case, params=params, timeout=TIMEOUT)
    assert response.status_code == 200
    created_case = response.json()
    Case(**created_case)
    assert created_case["id"] == case["id"]


@pytest.mark.skip
def test_create_case_missing_attributes():
    """at the moment, all case attributes are optional"""


def test_create_case_unknown_attributes():
    """creating a Case with unknown attributes results in error"""
    response = requests.post(f"{cds_url}/private/v1/cases", json={**new_case(), "unknown": "unknown"}, timeout=TIMEOUT)
    assert response.status_code == 422
    assert "Extra inputs" in get_error_api(response)


def test_create_case_check_id():
    """create a new case, get the case back, and compare the ID"""
    response = requests.post(f"{cds_url}/private/v1/cases", json=new_case(), timeout=TIMEOUT)
    case = Case(**response.json())
    response = requests.get(f"{cds_url}/v1/cases/{case.id}", timeout=TIMEOUT)
    case2 = Case(**response.json())
    assert case == case2


def test_create_case_check_attribute():
    """create a new case, get the case back, and compare an attribute"""
    raw_case = new_case()
    creator_id = raw_case["creator_id"]
    response = requests.post(f"{cds_url}/private/v1/cases", json=raw_case, timeout=TIMEOUT)
    case = Case(**response.json())
    assert case.creator_id == creator_id


def test_create_case_check_created_time():
    """create a new case, get the case back, and compare created_time"""
    response = requests.post(f"{cds_url}/private/v1/cases", json=new_case(), timeout=TIMEOUT)
    case = Case(**response.json())
    assert check_time(case.created_at)


def test_get_case_invalid_id():
    """passing an invalid ID should be handled properly"""
    response = requests.get(f"{cds_url}/v1/cases/{'x' * 5}", timeout=TIMEOUT)
    assert response.status_code == 422


def test_get_case_get_unknown_id():
    """try to get a case with an unknown ID"""
    response = requests.get(f"{cds_url}/v1/cases/{str(uuid.uuid4())}", timeout=TIMEOUT)
    assert response.status_code == 400
    assert "Case not found" in get_error(response)


def test_get_cases():
    create_sample_cases()

    r = requests.get(f"{cds_url}/v1/cases/", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    item_count = data.get("item_count")
    items = data.get("items")
    assert item_count == len(items)
    assert item_count >= 5
    ids = [e["id"] for e in items]
    assert len(ids) == len(set(ids))


def test_get_cases_with_slides():
    cases, _ = create_sample_data()
    case_ids = [case.id for case in cases]

    r = requests.get(f"{cds_url}/v1/cases/", params={"with_slides": True}, timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    item_count = data.get("item_count")
    items = data.get("items")
    assert item_count == len(items)
    assert item_count >= 10
    ids = [e["id"] for e in items]
    assert len(ids) == len(set(ids))

    for case in items:
        slide_count = len(case["slides"])
        if case["id"] in case_ids and slide_count > 0:
            assert slide_count == 2


def test_get_cases_skip_limit():
    create_sample_cases()

    r = requests.get(f"{cds_url}/v1/cases/", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    item_count = data.get("item_count")
    items = data.get("items")
    assert item_count == len(items)
    assert item_count >= 5

    r = requests.get(f"{cds_url}/v1/cases/?skip=1", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    skip_items = data.get("items")
    assert len(skip_items) == item_count - 1
    assert items[2]["id"] == skip_items[1]["id"]

    r = requests.get(f"{cds_url}/v1/cases/?limit=2", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    limit_items = data.get("items")
    assert len(limit_items) == 2
    from pprint import pprint

    pprint(items[:2])
    pprint(limit_items[:2])
    assert items[0]["id"] == limit_items[0]["id"]
    assert items[1]["id"] == limit_items[1]["id"]

    r = requests.get(f"{cds_url}/v1/cases/?skip=2&limit=2", timeout=TIMEOUT)
    assert r.status_code == 200
    data = r.json()
    skip_limit_items = data.get("items")
    assert len(skip_limit_items) == 2
    assert items[2]["id"] == skip_limit_items[0]["id"]
    assert items[3]["id"] == skip_limit_items[1]["id"]


def test_get_cases_sorted():
    r = requests.get(f"{cds_url}/v1/cases/", timeout=TIMEOUT)
    items = r.json().get("items")
    ordered = sorted(items, key=lambda x: x["created_at"], reverse=True)
    assert items == ordered

    r = requests.get(f"{cds_url}/v1/cases/?skip=2&limit=2", timeout=TIMEOUT)
    items = r.json().get("items")
    assert items == ordered[2:4]


def test_update_case_check_attribute():
    """create case, update attribute, get case, check if the attribute is there"""
    cid = find_case_id()
    response = requests.put(
        f"{cds_url}/private/v1/cases/{cid}",
        json={"description": "updated description", "deleted": True},
        timeout=TIMEOUT,
    )
    assert response.status_code == 200
    case = Case(**response.json())
    assert case.description == "updated description"
    assert case.deleted
    response = requests.get(f"{cds_url}/v1/cases/{cid}", timeout=TIMEOUT)
    case = Case(**response.json())
    assert case.description == "updated description"
    assert case.deleted


def test_update_case_check_updated_at():
    """create case, update some attribute, check updated-at attribute"""
    cid = find_case_id()
    response = requests.put(
        f"{cds_url}/private/v1/cases/{cid}", json={"description": "updated description"}, timeout=TIMEOUT
    )
    case = Case(**response.json())
    assert check_time(case.updated_at)


def test_update_case_unknown_id():
    """try to update a case with an unknown ID"""
    response = requests.put(
        f"{cds_url}/private/v1/cases/{str(uuid.uuid4())}", json={"description": "updated description"}, timeout=TIMEOUT
    )
    assert response.status_code == 400
    assert "Case not found" in get_error(response)


def test_update_case_unknown_attribute():
    """trying to update a case with unknown attributes raises an error"""
    cid = find_case_id()
    response = requests.put(f"{cds_url}/private/v1/cases/{cid}", json={"unknown": "unknown"}, timeout=TIMEOUT)
    assert response.status_code == 422


def test_update_case_illegal_attribute():
    """try to update different attributes that are not meant to be updated, e.g. ID or created_at"""
    cid = find_case_id()
    for attr in ["id", "created_at", "updated_at"]:
        response = requests.put(
            f"{cds_url}/private/v1/cases/{cid}", json={attr: "should not be updated"}, timeout=TIMEOUT
        )
        assert response.status_code == 422


def test_update_case_illegal_attribute_value():
    """try to update a valid attribute, but with an illegal value (wrong type)"""
    cid = find_case_id()
    # Note: if str is expected, int is automatically converted to str
    response = requests.put(f"{cds_url}/private/v1/cases/{cid}", json={"creator_id": []}, timeout=TIMEOUT)
    assert response.status_code == 422
