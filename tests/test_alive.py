import requests

from clinical_data_service.models.commons import ServiceStatus, ServiceStatusEnum

from . import TIMEOUT
from .singletons import cds_url


def test_alive():
    response = requests.get(f"{cds_url}/alive", timeout=TIMEOUT)
    assert response.status_code == 200
    status = ServiceStatus.model_validate(response.json())
    assert status.status == ServiceStatusEnum.OK
