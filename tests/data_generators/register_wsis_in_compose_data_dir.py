import argparse
from pathlib import Path

import requests

from tests.singletons import compose_settings, pytest_settings

supported_file_extensions = (
    ".svs",
    ".scn",
    ".ndpi",
    ".vsf",
    ".mrxs",
    ".tif",
    ".tiff",
    ".isyntax",
    ".bif",
    ".jpeg",
    ".jpg",
    ".png",
)


def post_slide(api_version: str, case_id: str, tissue: str, stain: str, block: str, main_address):
    url = f"{pytest_settings.cds_url}/private/{api_version}/slides"
    data = {
        "case_id": case_id,
        "tissue": tissue,
        "stain": stain,
        "block": block,
        "main_path": main_address,
    }
    return requests.post(url, json=data, timeout=10)


def get_all_slides(api_version: str):
    url = f"{pytest_settings.cds_url}/{api_version}/slides"
    r = requests.get(url, timeout=10)
    r.raise_for_status()
    return r.json()


def get_slide_storage(api_version: str, slide_id: str):
    url = f"{pytest_settings.cds_url}/private/{api_version}/slides/{slide_id}/storage"
    r = requests.get(url, timeout=10)
    return r


def post_case(api_version: str, creator_id: str, description: str):
    url = f"{pytest_settings.cds_url}/private/{api_version}/cases"
    data = {
        "creator_id": creator_id,
        "creator_type": "USER",
        "description": description,
    }
    r = requests.post(url, json=data, timeout=10)
    r.raise_for_status()
    return r.json()


def get_supported_wsi_files_recursive(sub_path: Path, only_filetypes: list):
    internal_wsi_filepaths = []
    if sub_path.is_dir():
        temp_files = []
        for current_path in sub_path.iterdir():
            tf = get_supported_wsi_files_recursive(sub_path=current_path, only_filetypes=only_filetypes)
            temp_files.extend(tf)
        internal_wsi_filepaths.extend(temp_files)
    else:
        if include_file(sub_path, only_filetypes):
            if str(sub_path).endswith(".dcm"):
                internal_path = str(sub_path.parent).replace(compose_settings.data_dir.rstrip("/"), "")
                internal_wsi_filepaths.append(internal_path)
            if str(sub_path).endswith(supported_file_extensions):
                internal_file = str(sub_path).replace(compose_settings.data_dir.rstrip("/"), "")
                internal_wsi_filepaths.append(internal_file)
    return set(internal_wsi_filepaths)


def include_file(path, only_filetypes):
    if only_filetypes:
        for filetype in only_filetypes:
            if filetype in str(path):
                return True
        return False
    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--api-version", help="API version ['v1', 'v3']. Default is v3.")
    parser.add_argument(
        "--only-filetypes", help="Single filetype or list of filetypes (separated by ;) that should be registered."
    )
    args = parser.parse_args()

    url_api_version = "v3"
    if args.api_version:
        if args.api_version not in ["v1", "v2"]:
            raise ValueError(f"API version {args.api_version} not valid. Only 'v1' and 'v3' are valid parameters")
        else:
            url_api_version = args.api_version

    only_filetypes_to_register = None
    if args.only_filetypes:
        filetypes = str(args.only_filetypes).split(";")
        only_filetypes_to_register = []
        for ftype in filetypes:
            if ftype not in supported_file_extensions and ".dcm" not in ftype:
                raise ValueError(f"Filetype not supported. Valid types: {supported_file_extensions} and '.dcm'")
            else:
                only_filetypes_to_register.append(ftype)

    data_directory = compose_settings.data_dir

    existing_slides_main_addresses = []
    for slide in get_all_slides(url_api_version)["items"]:
        response = get_slide_storage(url_api_version, slide["id"])
        if response.status_code == 200:
            slide_storage = response.json()
            existing_slides_main_addresses.append(slide_storage["main_storage_address"]["path"])

    startup_case = post_case(url_api_version, "Dummy", "Example Case")

    wsi_files = get_supported_wsi_files_recursive(Path(data_directory), only_filetypes_to_register)

    for file in wsi_files:
        rel_file = str(file).lstrip("/")
        if rel_file in existing_slides_main_addresses:
            print(f"(-) WSI {file} already registered")
            continue

        response = post_slide(
            api_version=url_api_version,
            case_id=str(startup_case["id"]),
            tissue="BREAST",
            stain="HER2",
            block="dummy",
            main_address=rel_file,
        )
        if response.status_code == 200:
            slide = response.json()
            print(f"(*) Successfully registered WSI {rel_file} with slide ID: {slide['id']}")
        else:
            print(f"(X) Could not register file {rel_file}: \n\t> {response.content}")
