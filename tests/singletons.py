from .settings import ComposeSettings, PytestSettings

compose_settings = ComposeSettings()
pytest_settings = PytestSettings()
cds_url = pytest_settings.cds_url.rstrip("/")
