import pytest
import requests

from clinical_data_service.models.v3.clinical import ClinicalSlide
from clinical_data_service.models.v3.storage import SlideStorage

from ... import TIMEOUT
from ...singletons import cds_url
from ..commons import create_sample_case, new_slide


@pytest.mark.parametrize("wsi_filepath", ["Aperio/CMU-1.svs", "Hamamatsu/OS-1.ndpi"])
def test_create_slide_with_automated_single_wsi_file_indexing(wsi_filepath: str):
    case = create_sample_case()
    slide = new_slide(case=case)
    slide["main_path"] = wsi_filepath

    response = requests.post(f"{cds_url}/private/v3/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 200
    cls = ClinicalSlide.model_validate(response.json())

    response = requests.get(f"{cds_url}/private/v3/slides/{cls.id}/storage", timeout=TIMEOUT)
    assert response.status_code == 200
    storage = SlideStorage.model_validate(response.json())
    assert storage.main_storage_address.path == wsi_filepath
    assert storage.secondary_storage_addresses == []


@pytest.mark.parametrize(
    "wsi_filepath, related_files_count",
    [("MIRAX/Mirax2.2-1.mrxs", 25), ("DICOM_wsidicomizer/DICOM_Aperio", 5)],
)
def test_create_slide_with_automated_multi_wsi_file_indexing(wsi_filepath: str, related_files_count: int):
    case = create_sample_case()
    slide = new_slide(case=case)
    slide["main_path"] = wsi_filepath

    response = requests.post(f"{cds_url}/private/v3/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 200
    cls = ClinicalSlide.model_validate(response.json())

    response = requests.get(f"{cds_url}/private/v3/slides/{cls.id}/storage", timeout=TIMEOUT)
    assert response.status_code == 200
    storage = SlideStorage.model_validate(response.json())
    assert len(storage.secondary_storage_addresses) == related_files_count
    assert storage.main_storage_address.path == wsi_filepath
    for addr in storage.secondary_storage_addresses:
        parent_dir = wsi_filepath.split("/")[0]
        assert addr.path.startswith(parent_dir)


@pytest.mark.parametrize(
    "wsi_filepath",
    [
        "_not/_existing.svs",
        "_not/_existing.ndpi",
        "_not/_existing.mrxs",
        "_not/_existing",
        "_not/_existing.tiff",
    ],
)
def test_create_slide_with_non_existant_wsi_file(wsi_filepath: str):
    case = create_sample_case()
    slide = new_slide(case=case)
    slide["main_path"] = wsi_filepath

    response = requests.post(f"{cds_url}/private/v3/slides", json=slide, timeout=TIMEOUT)
    assert response.status_code == 404
    exp_resp = f"File or directory '{wsi_filepath}' does not exist"
    assert response.json()["detail"] == exp_resp
