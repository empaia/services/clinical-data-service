import pytest

from tests.v3.wsi.validation_utils import (
    check_get_slide_info_valid,
    check_get_slide_label_valid,
    check_get_slide_macro_valid,
    check_get_slide_region_dedicated_channel,
    check_get_slide_region_invalid,
    check_get_slide_region_invalid_channel,
    check_get_slide_region_valid_brightfield,
    check_get_slide_thumbnail_valid,
    check_get_slide_tile_valid,
    get_slide_id_or_create,
)


@pytest.mark.parametrize(
    "slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y",
    [
        (
            # TIFF
            "/data/Generic TIFF/CMU-1.tiff",
            3,
            8,
            10,
            50,
            (128, 128),
            46000,
            32914,
        ),
        (
            # SVS
            "/data/Aperio/CMU-1.svs",
            3,
            8,
            3,
            499,
            (256, 256),
            46000,
            32914,
        ),
        (
            # NDPI
            "/data/Hamamatsu/OS-1.ndpi",
            3,
            8,
            13,
            227,
            (256, 256),
            122880,
            110592,
        ),
        (
            # MRXS
            "/data/MIRAX/Mirax2.2-1.mrxs",
            3,
            8,
            10,
            234,
            (256, 256),
            101832,
            219976,
        ),
        (
            # BIF
            "/data/Ventana/OS-2.bif",
            3,
            8,
            10,
            232,
            (256, 256),
            114943,
            76349,
        ),
        (
            # SCN
            "/data/Leica/Leica-2.scn",
            3,
            8,
            6,
            250,
            (256, 256),
            106259,
            306939,
        ),
    ],
)
def test_get_slide_info_valid(slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_info_valid(
        slide_id,
        channels,
        channel_depth,
        num_levels,
        pixel_size_nm,
        tile_size,
        x,
        y,
        plugin="cds-plugin-openslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel_rgb, testpixel_multichannel",
    [
        (
            "/data/Generic TIFF/CMU-1.tiff",
            200,
            (5, 5),
            (245, 247, 246),
            (245, 247, 246),
        ),
        (
            "/data/Aperio/CMU-1.svs",
            200,
            (5, 5),
            (244, 246, 245),
            (244, 246, 245),
        ),
        (
            "/data/Hamamatsu/OS-1.ndpi",
            200,
            (5, 5),
            (211, 200, 221),
            (211, 200, 221),
        ),
        (
            "/data/MIRAX/Mirax2.2-1.mrxs",
            200,
            (5, 5),
            (223, 158, 215),
            (223, 158, 215),
        ),
        (
            "/data/Ventana/OS-2.bif",
            200,
            (5, 5),
            (239, 239, 240),
            (239, 239, 240),
        ),
        (
            "/data/Leica/Leica-2.scn",
            200,
            (5, 5),
            (219, 208, 217),
            (219, 208, 217),
        ),
    ],
)
def test_get_slide_thumbnail_valid(
    image_format,
    image_quality,
    slide_filepath,
    return_value,
    pixel_location,
    testpixel_rgb,
    testpixel_multichannel,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_thumbnail_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel_rgb,
        testpixel_multichannel,
        plugin="cds-plugin-openslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, has_label, pixel_location, testpixel",
    [
        ("/data/Generic TIFF/CMU-1.tiff", False, (), ()),
        ("/data/Aperio/CMU-1.svs", True, (0, 0), (0, 0, 0)),
        ("/data/Aperio/CMU-1.svs", True, (50, 50), (202, 131, 51)),
        ("/data/Hamamatsu/OS-1.ndpi", False, (), ()),
        ("/data/MIRAX/Mirax2.2-1.mrxs", True, (0, 0), (19, 19, 19)),
        ("/data/MIRAX/Mirax2.2-1.mrxs", True, (50, 50), (27, 27, 27)),
        ("/data/Ventana/OS-2.bif", False, (), ()),
        ("/data/Leica/Leica-2.scn", False, (), ()),
    ],
)
def test_get_slide_label_valid(image_format, image_quality, slide_filepath, has_label, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_label_valid(
        image_format,
        image_quality,
        slide_id,
        has_label,
        pixel_location,
        testpixel,
        plugin="cds-plugin-openslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel",
    [
        ("/data/Generic TIFF/CMU-1.tiff", 404, (), ()),
        ("/data/Aperio/CMU-1.svs", 200, (0, 0), (0, 0, 0)),
        ("/data/Aperio/CMU-1.svs", 200, (50, 50), (238, 240, 240)),
        ("/data/Hamamatsu/OS-1.ndpi", 200, (0, 0), (117, 117, 117)),
        ("/data/Hamamatsu/OS-1.ndpi", 200, (50, 50), (15, 15, 15)),
        ("/data/MIRAX/Mirax2.2-1.mrxs", 200, (0, 0), (221, 221, 221)),
        ("/data/MIRAX/Mirax2.2-1.mrxs", 200, (50, 50), (156, 156, 156)),
        ("/data/Ventana/OS-2.bif", 200, (0, 0), (94, 75, 51)),
        ("/data/Leica/Leica-2.scn", 200, (0, 0), (2, 2, 2)),
    ],
)
def test_get_slide_macro_valid(image_format, image_quality, slide_filepath, return_value, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_macro_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel,
        plugin="cds-plugin-openslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 100), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath,  pixel_location, testpixel, start_x, start_y, size",
    [
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (0, 0),
            (255, 236, 253),
            15000,
            15000,
            345,
        ),
        (
            "/data/Aperio/CMU-1.svs",
            (0, 0),
            (255, 235, 255),
            15000,
            15000,
            345,
        ),
        (
            "/data/Hamamatsu/OS-1.ndpi",
            (0, 0),
            (220, 219, 227),
            15000,
            15000,
            345,
        ),
        (
            "/data/MIRAX/Mirax2.2-1.mrxs",
            (0, 0),
            (231, 182, 212),
            50000,
            90000,
            345,
        ),
        (
            "/data/Ventana/OS-2.bif",
            (0, 0),
            (245, 241, 242),
            30000,
            30000,
            345,
        ),
        ("/data/Leica/Leica-2.scn", (0, 0), (131, 59, 122), 50000, 55000, 345),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (0, 0),
            (255, 255, 255),
            -1_000_000,
            -1_000_000,
            1,
        ),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (0, 0),
            (255, 255, 255),
            1_000_000,
            1_000_000,
            1,
        ),
    ],
)
def test_get_slide_region_valid_brightfield(
    image_format,
    image_quality,
    slide_filepath,
    pixel_location,
    testpixel,
    start_x,
    start_y,
    size,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_valid_brightfield(
        image_format,
        image_quality,
        slide_id,
        pixel_location,
        testpixel,
        start_x,
        start_y,
        size,
        plugin="cds-plugin-openslide",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, testpixel, tile_x, tile_y, tile_size",
    [
        ("/data/Generic TIFF/CMU-1.tiff", (243, 243, 243), 21, 22, (128, 128)),
        ("/data/Aperio/CMU-1.svs", (246, 246, 243), 21, 22, (256, 256)),
        ("/data/Hamamatsu/OS-1.ndpi", (222, 221, 229), 21, 22, (256, 256)),
        ("/data/MIRAX/Mirax2.2-1.mrxs", (255, 255, 255), 60, 60, (256, 256)),
        ("/data/Ventana/OS-2.bif", (238, 238, 236), 210, 210, (256, 256)),
        ("/data/Leica/Leica-2.scn", (137, 75, 138), 210, 210, (256, 256)),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (255, 255, 255),
            -1_000_000,
            -1_000_000,
            (128, 128),
        ),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            (255, 255, 255),
            1_000_000,
            1_000_000,
            (128, 128),
        ),
    ],
)
def test_get_slide_tile_valid(image_format, image_quality, slide_filepath, testpixel, tile_x, tile_y, tile_size):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_tile_valid(
        image_format,
        image_quality,
        slide_id,
        testpixel,
        tile_x,
        tile_y,
        tile_size,
        plugin="cds-plugin-openslide",
    )


@pytest.mark.parametrize(
    "slide_filepath, testpixel, start_x, start_y, size, status_code",
    [("/data/Generic TIFF/CMU-1.tiff", (223, 217, 222), 15000, 15000, 30045, 422)],
)
def test_get_slide_region_invalid(slide_filepath, testpixel, start_x, start_y, size, status_code):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_invalid(
        slide_id, testpixel, start_x, start_y, size, status_code, plugin="cds-plugin-openslide"
    )


@pytest.mark.parametrize("image_format, image_quality", [("png", 100), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, level, channels, start_point, size, pixel_location, testpixel_multichannel, testpixel_rgb",
    [
        (
            "/data/Generic TIFF/CMU-1.tiff",
            7,
            [0],
            (0, 0),
            (256, 256),
            (128, 128),
            [246],
            (246, 0, 0),
        )
    ],
)
def test_get_slide_region_dedicated_channel(
    slide_filepath,
    level,
    channels,
    start_point,
    size,
    pixel_location,
    testpixel_multichannel,
    testpixel_rgb,
    image_format,
    image_quality,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_dedicated_channel(
        slide_id,
        level,
        channels,
        start_point,
        size,
        pixel_location,
        testpixel_multichannel,
        testpixel_rgb,
        image_format,
        image_quality,
        plugin="cds-plugin-openslide",
    )


@pytest.mark.parametrize(
    "slide_filepath, channels, expected_response",
    [
        ("/data/Generic TIFF/CMU-1.tiff", [2], 200),
        ("/data/Generic TIFF/CMU-1.tiff", [4], 400),
    ],
)
def test_get_slide_region_invalid_channel(slide_filepath, channels, expected_response):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_invalid_channel(slide_id, channels, expected_response, plugin="cds-plugin-openslide")
