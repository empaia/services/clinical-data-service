import numpy as np
import pytest
import requests

from clinical_data_service.models.v3.slide import SlideInfo
from tests import TIMEOUT

from ...singletons import cds_url
from .validation_utils import get_image, get_slide_id_or_create


@pytest.mark.parametrize(
    "slide_filepath",
    [("/data/Aperio/CMU-1.svs"), ("/data/Generic TIFF/CMU-1.tiff")],
)
def test_compare_openslide_info(slide_filepath):
    slide_id = get_slide_id_or_create(slide_filepath)
    response = requests.get(f"{cds_url}/v3/slides/{slide_id}/info?plugin=cds-plugin-tiffslide", timeout=TIMEOUT)
    slide_info_tiffslide = SlideInfo.model_validate(response.json())
    response = requests.get(f"{cds_url}/v3/slides/{slide_id}/info?plugin=cds-plugin-openslide", timeout=TIMEOUT)
    slide_info_openslide = SlideInfo.model_validate(response.json())
    slide_info_tiffslide.format = ""
    slide_info_openslide.format = ""
    assert slide_info_tiffslide == slide_info_openslide


@pytest.mark.parametrize(
    "slide_filepath, tiles",
    [
        ("/data/Aperio/CMU-1.svs", [(0, 73, 80), (1, 19, 21), (2, 3, 6)]),
        ("/data/Generic TIFF/CMU-1.tiff", [(0, 150, 164), (2, 40, 42), (7, 0, 1)]),
    ],
)
def test_compare_openslide_tile(slide_filepath, tiles):
    slide_id = get_slide_id_or_create(slide_filepath)
    for level, x, y in tiles:
        response = requests.get(
            f"{cds_url}/v3/slides/{slide_id}/tile/level/{level}/tile/{x}/{y}?plugin=cds-plugin-tiffslide",
            stream=True,
            timeout=TIMEOUT,
        )
        tile_tiffslide_pil = get_image(response)
        # tile_tiffslide_pil.save(f"{slide_id}_{level}_{x}_{y}_tiffslide.png")
        tile_tiffslide = np.array(tile_tiffslide_pil).astype(np.float64)
        params = "plugin=cds-plugin-openslide&image_format=png"
        response = requests.get(
            f"{cds_url}/v3/slides/{slide_id}/tile/level/{level}/tile/{x}/{y}?{params}",
            stream=True,
            timeout=TIMEOUT,
        )
        tile_openslide_pil = get_image(response)
        # tile_openslide_pil.save(f"{slide_id}_{level}_{x}_{y}_openslide.png")
        tile_openslide = np.array(tile_openslide_pil).astype(np.float64)
        mean_relative_error = np.mean(
            np.abs(tile_tiffslide[tile_openslide > 0] - tile_openslide[tile_openslide > 0])
            / tile_openslide[tile_openslide > 0]
        )
        # print(slide_id, level, x, y, mean_relative_error)
        # expect mean relative error < 10 %
        assert mean_relative_error < 0.1


@pytest.mark.parametrize(
    "slide_filepath, regions",
    [
        ("/data/Aperio/CMU-1.svs", [(0, 16000, 24000), (1, 4000, 6000), (2, 1000, 1500), (2, -200, -100)]),
        (
            "/data/Generic TIFF/CMU-1.tiff",
            [
                (0, 16000, 24000),
                (1, 8000, 12000),
                (2, 4000, 6000),
                (2, -200, -100),
                (3, 2000, 3000),
                (4, 1000, 1500),
                (9, 0, 0),
            ],
        ),
    ],
)
def test_compare_openslide_region(slide_filepath, regions):
    slide_id = get_slide_id_or_create(slide_filepath)
    for level, x, y in regions:
        params = "plugin=cds-plugin-tiffslide&image_format=png"
        response = requests.get(
            f"{cds_url}/v3/slides/{slide_id}/region/level/{level}/start/{x}/{y}/size/512/512?{params}",
            stream=True,
            timeout=TIMEOUT,
        )
        tile_tiffslide_pil = get_image(response)
        # tile_tiffslide_pil.save(f"{slide_id}_{level}_{x}_{y}_tiffslide.png")
        tile_tiffslide = np.array(tile_tiffslide_pil).astype(np.float64)
        params = "plugin=cds-plugin-openslide&image_format=png"
        response = requests.get(
            f"{cds_url}/v3/slides/{slide_id}/region/level/{level}/start/{x}/{y}/size/512/512?{params}",
            stream=True,
            timeout=TIMEOUT,
        )
        tile_openslide_pil = get_image(response)
        # tile_openslide_pil.save(f"{slide_id}_{level}_{x}_{y}_openslide.png")
        tile_openslide = np.array(tile_openslide_pil).astype(np.float64)
        mean_relative_error = np.mean(
            np.abs(tile_tiffslide[tile_openslide > 0] - tile_openslide[tile_openslide > 0])
            / tile_openslide[tile_openslide > 0]
        )
        # print(slide_id, level, x, y, mean_relative_error)
        # expect mean relative error < 10 %
        assert mean_relative_error < 0.1
