import pytest

from tests.v3.wsi.validation_utils import (
    check_get_slide_info_valid,
    check_get_slide_label_valid,
    check_get_slide_macro_valid,
    check_get_slide_region_dedicated_channel,
    check_get_slide_region_invalid,
    check_get_slide_region_invalid_channel,
    check_get_slide_region_valid_brightfield,
    check_get_slide_thumbnail_valid,
    check_get_slide_tile_valid,
    get_slide_id_or_create,
)

from ...singletons import pytest_settings


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize(
    "slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y",
    [
        (
            "/data/MIRAX/Mirax2.2-1.mrxs",
            3,
            8,
            10,
            234,
            (338, 254),
            101832,
            219976,
        ),
    ],
)
def test_get_slide_info_valid(slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_info_valid(
        slide_id,
        channels,
        channel_depth,
        num_levels,
        pixel_size_nm,
        tile_size,
        x,
        y,
        plugin="cds-plugin-mirax",
    )


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 0)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel_rgb, testpixel_multichannel",
    [
        (
            "/data/MIRAX/Mirax2.2-1.mrxs",
            200,
            (5, 5),
            (217, 164, 218),
            (217, 164, 218),
        ),
    ],
)
def test_get_slide_thumbnail_valid(
    image_format,
    image_quality,
    slide_filepath,
    return_value,
    pixel_location,
    testpixel_rgb,
    testpixel_multichannel,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_thumbnail_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel_rgb,
        testpixel_multichannel,
        plugin="cds-plugin-mirax",
    )


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 0)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, has_label, pixel_location, testpixel",
    [
        ("/data/MIRAX/Mirax2.2-1.mrxs", True, (0, 0), (19, 19, 19)),
        ("/data/MIRAX/Mirax2.2-1.mrxs", True, (50, 50), (27, 27, 27)),
    ],
)
def test_get_slide_label_valid(image_format, image_quality, slide_filepath, has_label, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_label_valid(
        image_format,
        image_quality,
        slide_id,
        has_label,
        pixel_location,
        testpixel,
        plugin="cds-plugin-mirax",
    )


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 0)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel",
    [
        ("/data/MIRAX/Mirax2.2-1.mrxs", 200, (0, 0), (219, 219, 219)),
        ("/data/MIRAX/Mirax2.2-1.mrxs", 200, (50, 50), (154, 154, 154)),
    ],
)
def test_get_slide_macro_valid(image_format, image_quality, slide_filepath, return_value, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_macro_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel,
        plugin="cds-plugin-mirax",
    )


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 100)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath,  pixel_location, testpixel, start_x, start_y, size",
    [
        (
            "/data/MIRAX/Mirax2.2-1.mrxs",
            (0, 0),
            (231, 182, 212),
            50000,
            90000,
            345,
        ),
    ],
)
def test_get_slide_region_valid_brightfield(
    image_format,
    image_quality,
    slide_filepath,
    pixel_location,
    testpixel,
    start_x,
    start_y,
    size,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_valid_brightfield(
        image_format,
        image_quality,
        slide_id,
        pixel_location,
        testpixel,
        start_x,
        start_y,
        size,
        plugin="cds-plugin-mirax",
    )


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 0)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, testpixel, tile_x, tile_y, tile_size",
    [
        ("/data/MIRAX/Mirax2.2-1.mrxs", (255, 255, 255), 60, 60, (338, 254)),
    ],
)
def test_get_slide_tile_valid(image_format, image_quality, slide_filepath, testpixel, tile_x, tile_y, tile_size):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_tile_valid(
        image_format,
        image_quality,
        slide_id,
        testpixel,
        tile_x,
        tile_y,
        tile_size,
        plugin="cds-plugin-mirax",
    )


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize(
    "slide_filepath, testpixel, start_x, start_y, size, status_code",
    [("/data/MIRAX/Mirax2.2-1.mrxs", (223, 217, 222), 15000, 15000, 30045, 422)],
)
def test_get_slide_region_invalid(slide_filepath, testpixel, start_x, start_y, size, status_code):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_invalid(slide_id, testpixel, start_x, start_y, size, status_code, plugin="cds-plugin-mirax")


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("png", 100)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, level, channels, start_point, size, pixel_location, testpixel_multichannel, testpixel_rgb",
    [
        (
            "/data/MIRAX/Mirax2.2-1.mrxs",
            7,
            [0],
            (0, 0),
            (256, 256),
            (128, 128),
            [246],
            (255, 0, 0),
        )
    ],
)
def test_get_slide_region_dedicated_channel(
    slide_filepath,
    level,
    channels,
    start_point,
    size,
    pixel_location,
    testpixel_multichannel,
    testpixel_rgb,
    image_format,
    image_quality,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_dedicated_channel(
        slide_id,
        level,
        channels,
        start_point,
        size,
        pixel_location,
        testpixel_multichannel,
        testpixel_rgb,
        image_format,
        image_quality,
        plugin="cds-plugin-mirax",
    )


@pytest.mark.skipif(pytest_settings.skip_tests_mirax_plugin, reason="Plugin tests for Mirax disabled")
@pytest.mark.parametrize(
    "slide_filepath, channels, expected_response",
    [
        ("/data/MIRAX/Mirax2.2-1.mrxs", [2], 200),
        ("/data/MIRAX/Mirax2.2-1.mrxs", [4], 400),
    ],
)
def test_get_slide_region_invalid_channel(slide_filepath, channels, expected_response):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_invalid_channel(slide_id, channels, expected_response, plugin="cds-plugin-mirax")
