import pytest

from ...singletons import pytest_settings
from .validation_utils import (
    check_get_slide_info_valid,
    check_get_slide_label_valid,
    check_get_slide_macro_valid,
    check_get_slide_region_invalid,
    check_get_slide_region_valid_brightfield,
    check_get_slide_thumbnail_valid,
    check_get_slide_tile_valid,
    get_slide_id_or_create,
)


@pytest.mark.skipif(pytest_settings.skip_tests_isyntax_plugin, reason="Plugin tests for Isyntax disabled")
@pytest.mark.parametrize(
    "slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y",
    [
        # ISYNTAX
        ("/data/Philips iSyntax/4399.isyntax", 3, 8, 9, 250, (256, 256), 192518, 88070)
    ],
)
def test_get_slide_info_valid(slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_info_valid(slide_id, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y)


@pytest.mark.skipif(pytest_settings.skip_tests_isyntax_plugin, reason="Plugin tests for Isyntax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel_rgb, testpixel_multichannel",
    [
        (
            "/data/Philips iSyntax/4399.isyntax",
            200,
            (5, 5),
            (255, 255, 255),
            (255, 255, 255),
        )
    ],
)
def test_get_slide_thumbnail_valid(
    image_format,
    image_quality,
    slide_filepath,
    return_value,
    pixel_location,
    testpixel_rgb,
    testpixel_multichannel,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_thumbnail_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel_rgb,
        testpixel_multichannel,
    )


@pytest.mark.skipif(pytest_settings.skip_tests_isyntax_plugin, reason="Plugin tests for Isyntax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, has_label, pixel_location, testpixel",
    [("/data/Philips iSyntax/4399.isyntax", True, (50, 50), (255, 255, 255))],
)
def test_get_slide_label_valid(image_format, image_quality, slide_filepath, has_label, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_label_valid(image_format, image_quality, slide_id, has_label, pixel_location, testpixel)


@pytest.mark.skipif(pytest_settings.skip_tests_isyntax_plugin, reason="Plugin tests for Isyntax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel",
    [("/data/Philips iSyntax/4399.isyntax", 200, (0, 0), (180, 180, 180))],
)
def test_get_slide_macro_valid(image_format, image_quality, slide_filepath, return_value, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_macro_valid(image_format, image_quality, slide_id, return_value, pixel_location, testpixel)


@pytest.mark.skipif(pytest_settings.skip_tests_isyntax_plugin, reason="Plugin tests for Isyntax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 100), ("png", 100)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath,  pixel_location, testpixel, start_x, start_y, size",
    [("/data/Philips iSyntax/4399.isyntax", (0, 0), (234, 234, 247), 140000, 50000, 345)],
)
def test_get_slide_region_valid_brightfield(
    image_format,
    image_quality,
    slide_filepath,
    pixel_location,
    testpixel,
    start_x,
    start_y,
    size,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_valid_brightfield(
        image_format,
        image_quality,
        slide_id,
        pixel_location,
        testpixel,
        start_x,
        start_y,
        size,
    )


@pytest.mark.skipif(pytest_settings.skip_tests_isyntax_plugin, reason="Plugin tests for Isyntax disabled")
@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0)])  # ("tiff", 100)
@pytest.mark.parametrize(
    "slide_filepath, testpixel, tile_x, tile_y, tile_size",
    [("/data/Philips iSyntax/4399.isyntax", (21, 19, 20), 10, 11, (256, 256))],
)
def test_get_slide_tile_valid(image_format, image_quality, slide_filepath, testpixel, tile_x, tile_y, tile_size):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_tile_valid(image_format, image_quality, slide_id, testpixel, tile_x, tile_y, tile_size)


@pytest.mark.skipif(pytest_settings.skip_tests_isyntax_plugin, reason="Plugin tests for Isyntax disabled")
@pytest.mark.parametrize(
    "slide_filepath, testpixel, start_x, start_y, size, status_code",
    [("/data/Philips iSyntax/4399.isyntax", (0, 0, 0), 30000, 30000, 30045, 422)],
)
def test_get_slide_region_invalid(slide_filepath, testpixel, start_x, start_y, size, status_code):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_invalid(slide_id, testpixel, start_x, start_y, size, status_code)
