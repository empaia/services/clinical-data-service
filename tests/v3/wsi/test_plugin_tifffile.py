import pytest

from .validation_utils import (
    check_get_slide_info_valid,
    check_get_slide_label_valid,
    check_get_slide_macro_valid,
    check_get_slide_region_dedicated_channel,
    check_get_slide_region_invalid_channel,
    check_get_slide_region_valid_fluorescence,
    check_get_slide_thumbnail_valid,
    check_get_slide_tile_valid,
    get_slide_id_or_create,
)


@pytest.mark.parametrize(
    "slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y",
    [
        # OME-TIFF 3x16bit
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", 3, 16, 3, 325, (256, 256), 11260, 22300),
        # OME-TIFF 5x8bit
        ("/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff", 5, 8, 6, 498, (512, 512), 24960, 34560),
    ],
)
def test_get_slide_info_valid(slide_filepath, channels, channel_depth, num_levels, pixel_size_nm, tile_size, x, y):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_info_valid(
        slide_id,
        channels,
        channel_depth,
        num_levels,
        pixel_size_nm,
        tile_size,
        x,
        y,
        plugin="cds-plugin-tifffile",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel_rgb, testpixel_multichannel",
    [
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", 200, (0, 0), (0, 0, 0), (3, 2, 0)),
        (
            "/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif",
            200,
            (10, 10),
            (11, 7, 2),
            (761, 493, 149),
        ),
        ("/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff", 200, (0, 0), (0, 0, 0), (15, 0, 0)),
        (
            "/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff",
            200,
            (10, 10),
            (89, 56, 26),
            (5779, 3652, 1690),
        ),
    ],
)
def test_get_slide_thumbnail_valid(
    image_format,
    image_quality,
    slide_filepath,
    return_value,
    pixel_location,
    testpixel_rgb,
    testpixel_multichannel,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_thumbnail_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel_rgb,
        testpixel_multichannel,
        plugin="cds-plugin-tifffile",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, has_label, pixel_location, testpixel",
    [
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", False, (), ()),
        ("/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff", False, (), ()),
    ],
)
def test_get_slide_label_valid(image_format, image_quality, slide_filepath, has_label, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_label_valid(
        image_format,
        image_quality,
        slide_id,
        has_label,
        pixel_location,
        testpixel,
        plugin="cds-plugin-tifffile",
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, return_value, pixel_location, testpixel",
    [
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", 404, (), ()),
        ("/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff", 404, (), ()),
    ],
)
def test_get_slide_macro_valid(image_format, image_quality, slide_filepath, return_value, pixel_location, testpixel):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_macro_valid(
        image_format,
        image_quality,
        slide_id,
        return_value,
        pixel_location,
        testpixel,
        plugin="cds-plugin-tifffile",
    )


@pytest.mark.parametrize("image_format, image_quality", [("tiff", 100), ("jpeg", 90), ("png", 100)])
@pytest.mark.parametrize(
    "slide_filepath, channels, start_point, size, pixel_location, testpixel_multichannel, testpixel_rgb, padding_color",
    [
        (
            "/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif",
            3,
            (0, 0),
            (256, 256),
            (0, 0),
            (0, 0, 0),
            (0, 0, 0),
            "#000000",
        ),
        (
            "/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif",
            3,
            (512, 512),
            (256, 256),
            (100, 100),
            (3900, 2792, 3859),
            (60, 43, 60),
            None,
        ),
        (
            "/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff",
            5,
            (0, 0),
            (256, 256),
            (0, 0),
            (0, 0, 0),
            (0, 0, 0),
            "#000000",
        ),
        (
            "/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff",
            5,
            (512, 512),
            (256, 256),
            (200, 200),
            (4, 0, 0),
            (4, 0, 0),
            None,
        ),
        (
            "/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif",
            3,
            (-1_000_000, -1_000_000),
            (256, 256),
            (0, 0),
            (0, 0, 0),
            (0, 0, 0),
            "#000000",
        ),
        (
            "/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif",
            3,
            (1_000_000, 1_000_000),
            (256, 256),
            (0, 0),
            (0, 0, 0),
            (0, 0, 0),
            "#000000",
        ),
    ],
)
def test_get_slide_region_valid_fluorescence(
    slide_filepath,
    channels,
    start_point,
    size,
    pixel_location,
    testpixel_multichannel,
    testpixel_rgb,
    padding_color,
    image_format,
    image_quality,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_valid_fluorescence(
        slide_id,
        channels,
        start_point,
        size,
        pixel_location,
        testpixel_multichannel,
        testpixel_rgb,
        image_format,
        image_quality,
        plugin="cds-plugin-tifffile",
        padding_color=padding_color,
    )


@pytest.mark.parametrize("image_format, image_quality", [("jpeg", 90), ("png", 0), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, tile_x, tile_y, tile_size, padding_color, pixel_location, testpixel",
    [
        ("/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff", 21, 22, (512, 512), None, (0, 0), (30, 7, 6)),
        (
            "/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff",
            -1_000_000,
            -1_000_000,
            (512, 512),
            "#FFFFFF",
            (0, 0),
            (255, 255, 255),
        ),
        (
            "/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff",
            1_000_000,
            1_000_000,
            (512, 512),
            "#FFFFFF",
            (0, 0),
            (255, 255, 255),
        ),
    ],
)
def test_get_slide_tile_valid(
    image_format, image_quality, slide_filepath, tile_x, tile_y, tile_size, padding_color, pixel_location, testpixel
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_tile_valid(
        image_format,
        image_quality,
        slide_id,
        testpixel,
        tile_x,
        tile_y,
        tile_size,
        pixel_location=pixel_location,
        plugin="cds-plugin-tifffile",
        padding_color=padding_color,
    )


@pytest.mark.parametrize("image_format, image_quality", [("png", 100), ("tiff", 100)])
@pytest.mark.parametrize(
    "slide_filepath, level, channels, start_point, size, pixel_location, testpixel_multichannel, testpixel_rgb",
    [
        (
            "/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif",
            2,
            [0],
            (0, 0),
            (256, 256),
            (128, 128),
            [740],
            13,
        ),
        (
            "/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif",
            2,
            [0, 1],
            (0, 0),
            (256, 256),
            (128, 128),
            [740, 525],
            (13, 9, 0),
        ),
        (
            "/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff",
            5,
            [0],
            (0, 0),
            (256, 256),
            (128, 128),
            [36],
            36,
        ),
        (
            "/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff",
            5,
            [0, 1, 2, 3],
            (0, 0),
            (256, 256),
            (128, 128),
            [36, 36, 16, 43],
            (36, 36, 16),
        ),
    ],
)
def test_get_slide_region_dedicated_channel(
    slide_filepath,
    level,
    channels,
    start_point,
    size,
    pixel_location,
    testpixel_multichannel,
    testpixel_rgb,
    image_format,
    image_quality,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_dedicated_channel(
        slide_id,
        level,
        channels,
        start_point,
        size,
        pixel_location,
        testpixel_multichannel,
        testpixel_rgb,
        image_format,
        image_quality,
        plugin="cds-plugin-tifffile",
    )


@pytest.mark.parametrize(
    "slide_filepath, channels, expected_response",
    [
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", [0], 200),
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", [0, 1, 2], 200),
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", [0, 1, 6], 400),
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", [5], 400),
        ("/data/Fluorescence OME-Tif/2019_10_15__0014_GOOD.ome.tif", [], 200),
        ("/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff", [0, 1, 2, 3, 4], 200),
        ("/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff", [4], 200),
        ("/data/Fluorescence OME-Tif/LuCa-7color_Scan1.ome.tiff", [5], 400),
    ],
)
def test_get_slide_region_invalid_channel(slide_filepath, channels, expected_response):
    slide_id = get_slide_id_or_create(slide_filepath)
    check_get_slide_region_invalid_channel(slide_id, channels, expected_response, plugin="cds-plugin-tifffile")
