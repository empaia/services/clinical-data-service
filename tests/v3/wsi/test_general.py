import os
import shutil
import tempfile
from zipfile import ZipFile

import pytest
import requests
from PIL import Image
from PIL.ImageStat import Stat

from tests.v3.wsi.validation_utils import get_image, get_slide_id_or_create

from ... import TIMEOUT
from ...singletons import cds_url


@pytest.mark.parametrize("slide_filepath", ["/data/Generic TIFF/CMU-1.tiff"])
@pytest.mark.parametrize("tile_x, tile_y, level, expected_response, size", [(0, 0, 9, 200, (128, 128))])  # ok
def test_get_slide_tile_padding_color(slide_filepath, tile_x, tile_y, level, expected_response, size):
    slide_id = get_slide_id_or_create(slide_filepath)
    response = requests.get(
        (
            f"{cds_url}/v3/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}"
            "?image_format=png&padding_color=%23AABBCC"
        ),
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == expected_response
    assert response.headers["content-type"] == "image/png"

    image = get_image(response)
    x, y = image.size
    assert (x == size[0]) and (y == size[1])
    assert image.getpixel((size[0] - 1, size[1] - 1)) == (170, 187, 204)


@pytest.mark.parametrize("slide_filepath", ["/data/Generic TIFF/CMU-1.tiff"])
@pytest.mark.parametrize(
    "tile_x, level, expected_response",
    [
        (10, 1, 200),  # ok
        (10, 0, 200),  # ok
        (10, -1, 422),  # level -1 fails
        (10, 9, 200),  # level 10 ist coarsest level
        (10, 16, 422),  # level fails
    ],
)
def test_get_slide_tile_invalid(slide_filepath, tile_x, level, expected_response):
    slide_id = get_slide_id_or_create(slide_filepath)
    response = requests.get(
        f"{cds_url}/v3/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_x}", timeout=TIMEOUT
    )
    assert response.status_code == expected_response


@pytest.mark.parametrize("slide_filepath", ["/data/Generic TIFF/CMU-1.tiff"])
@pytest.mark.parametrize(
    "tile_x, tile_y, level, expected_response, expected_mean",
    [
        (-1, -1, 0, 200, 255),
        (1_000_000, 1_000_000, 0, 200, 255),
    ],
)
def test_get_slide_tile_out_of_image(slide_filepath, tile_x, tile_y, level, expected_response, expected_mean):
    slide_id = get_slide_id_or_create(slide_filepath)
    response = requests.get(
        f"{cds_url}/v3/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}", stream=True, timeout=TIMEOUT
    )
    assert response.status_code == expected_response
    image = get_image(response)
    assert sum(Stat(image).mean) / 3 == expected_mean


@pytest.mark.parametrize("slide_filepath", ["/data/Generic TIFF/CMU-1.tiff"])
@pytest.mark.parametrize(
    "start_x, start_y, size_x, size_y, level, expected_response, expected_mean",
    [
        (-1_000_000, -1_000_000, 100, 200, 0, 200, 255),
        (1_000_000, 1_000_000, 100, 200, 0, 200, 255),
    ],
)
def test_get_slide_region_out_of_image(
    slide_filepath, start_x, start_y, size_x, size_y, level, expected_response, expected_mean
):
    slide_id = get_slide_id_or_create(slide_filepath)
    response = requests.get(
        f"{cds_url}/v3/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == expected_response
    image = get_image(response)
    assert sum(Stat(image).mean) / 3 == expected_mean


@pytest.mark.parametrize("slide_filepath", ["/data/Generic TIFF/CMU-1.tiff"])
@pytest.mark.parametrize(
    "start_x, start_y, size_x, size_y, level, expected_response, expected_mean, white_patch_size, white_patch_position",
    [
        (-100, -200, 1024, 1024, 0, 200, 246, (924, 824), (100, 200)),
        (-100, -200, 1024, 1024, 9, 200, 254, (89, 64), (100, 200)),
        (10, 20, 1024, 1024, 9, 200, 254, (79, 44), (0, 0)),
    ],
)
def test_get_slide_region_partly_out_of_image(
    slide_filepath,
    start_x,
    start_y,
    size_x,
    size_y,
    level,
    expected_response,
    expected_mean,
    white_patch_size,
    white_patch_position,
):
    slide_id = get_slide_id_or_create(slide_filepath)
    query = "?image_format=png"
    response = requests.get(
        f"{cds_url}/v3/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}{query}",
        stream=True,
        timeout=TIMEOUT,
    )
    assert response.status_code == expected_response
    image = get_image(response)
    assert sum(Stat(image).mean) / 3 > expected_mean
    white_patch = Image.new("RGB", white_patch_size, (255, 255, 255))
    image.paste(
        white_patch,
        box=(
            white_patch_position[0],
            white_patch_position[1],
            white_patch_position[0] + white_patch_size[0],
            white_patch_position[1] + white_patch_size[1],
        ),
    )
    assert sum(Stat(image).mean) / 3 == 255


@pytest.mark.parametrize("region_size", [-1, 0, 1, 256, 512, 10000])
def test_get_region_maximum_extent(region_size):
    slide_id = get_slide_id_or_create("/data/MIRAX/Mirax2.2-1.mrxs")
    level = 5
    start_x = 13
    start_y = 23
    path_params = f"slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{region_size}/{region_size}"
    response = requests.get(
        f"{cds_url}/v3/{path_params}",
        timeout=TIMEOUT,
    )
    if region_size * region_size > 25000000:
        assert response.status_code == 422  # requested data too large
    elif region_size <= 0:
        assert response.status_code == 422
    else:
        assert response.status_code == 200


@pytest.mark.parametrize(
    "slide_filepath, file_count, file_size",
    [
        ("/data/Aperio/CMU-1.svs", 1, 177552579),  # SVS
        ("/data/DICOM_wsidicomizer/DICOM_Aperio", 5, 184520422),  # DICOM
        ("/data/MIRAX/Mirax2.2-1.mrxs", 26, 2915564670),  # MRXS
    ],
)
def test_download(slide_filepath, file_count, file_size):
    def download_file(url, download_folder):
        with requests.get(url, stream=True, timeout=TIMEOUT) as r:
            r.raise_for_status()
            filename = r.headers["content-disposition"].replace("attachment;filename=", "")
            file_path = os.path.join(download_folder, filename)
            with open(file_path, "wb") as f:
                for chunk in r.iter_content(chunk_size=1_000_000):
                    f.write(chunk)
        return file_path

    def get_file_count(path):
        file_count = 0
        with os.scandir(path) as it:
            for entry in it:
                if entry.is_file():
                    file_count += 1
                elif entry.is_dir():
                    file_count += get_file_count(entry.path)
        return file_count

    def get_dir_size(path):
        dir_size = 0
        with os.scandir(path) as it:
            for entry in it:
                if entry.is_file():
                    dir_size += entry.stat().st_size
                elif entry.is_dir():
                    dir_size += get_dir_size(entry.path)
        return dir_size

    slide_id = get_slide_id_or_create(slide_filepath)

    # create temp dir
    tmp_dir = tempfile.mkdtemp()
    # download
    file_path = download_file(f"{cds_url}/v3/slides/{slide_id}/download", tmp_dir)
    assert os.path.exists(file_path)
    assert slide_id in file_path
    # unzip
    zf = ZipFile(file_path)
    output_dir = os.path.join(tmp_dir, slide_id)
    zf.extractall(output_dir)
    os.makedirs(output_dir, exist_ok=True)
    assert get_file_count(output_dir) == file_count
    assert get_dir_size(output_dir) == file_size
    # remove temp dir
    shutil.rmtree(tmp_dir)
