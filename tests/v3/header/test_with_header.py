from uuid import uuid4

import requests

from tests.v3.wsi.validation_utils import get_slide_or_create

from ... import TIMEOUT
from ...singletons import cds_url


def _generate_urls(slide_id):
    level, tile_x, tile_y = 0, 0, 0
    start_x, start_y, size_x, size_y = 0, 0, 256, 256
    max_x, max_y = 256, 256

    urls = [
        f"{cds_url}/v3/slides/{slide_id}",
        f"{cds_url}/v3/slides/{slide_id}/info",
        f"{cds_url}/v3/slides/{slide_id}/download",
        f"{cds_url}/v3/slides/{slide_id}/thumbnail/max_size/{max_x}/{max_y}",
        f"{cds_url}/v3/slides/{slide_id}/label/max_size/{max_x}/{max_y}",
        f"{cds_url}/v3/slides/{slide_id}/macro/max_size/{max_x}/{max_y}",
        f"{cds_url}/v3/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        f"{cds_url}/v3/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}",
    ]

    return urls


def test_with_correct_header():
    slide_filepath = "/data/Generic TIFF/CMU-1.tiff"
    slide = get_slide_or_create(slide_filepath)
    slide_id = str(slide.id)
    case_id = str(slide.case_id)

    headers = {"case-id": case_id}
    urls = _generate_urls(slide_id=slide_id)

    for url in urls:
        r = requests.get(url, headers=headers, timeout=TIMEOUT)
        assert r.status_code in [200, 404]


def test_with_wrong_header():
    slide_filepath = "/data/Generic TIFF/CMU-1.tiff"
    slide = get_slide_or_create(slide_filepath)
    slide_id = str(slide.id)
    case_id = str(uuid4())

    headers = {"case-id": case_id}
    urls = _generate_urls(slide_id=slide_id)

    for url in urls:
        r = requests.get(url, headers=headers, timeout=TIMEOUT)
        assert r.status_code == 412
