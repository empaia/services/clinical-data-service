from pydantic_settings import BaseSettings, SettingsConfigDict


class ComposeSettings(BaseSettings):
    data_dir: str
    model_config = SettingsConfigDict(env_file=".env", env_prefix="COMPOSE_", extra="ignore")


class PytestSettings(BaseSettings):
    cds_url: str
    skip_tests_isyntax_plugin: bool
    skip_tests_mirax_plugin: bool
    model_config = SettingsConfigDict(env_file=".env", env_prefix="PYTEST_", extra="ignore")
